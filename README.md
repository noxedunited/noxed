# README #
####Noxed
Noxed is a world editor meant to be used with the [Nox Engine](https://bitbucket.org/suttungdigital/nox-engine).

To build and run the project, apply the [instructions from NOX Engine](https://bitbucket.org/suttungdigital/nox-engine/src/master/README.md) to this project.
The project is only tested on Windows and Linux. You can run doxygen on everything.doxy to generate documentation.

Noxed is released under the MIT license, see [LICENSE.md](https://bitbucket.org/noxedunited/noxed/src/LICENSE.md)

This editor started as a bachelor project in Game Programming and Computer Science at the Norwegian University of Science and Technology - Gjøvik.

Noxed is not affiliated with Suttung Digital.