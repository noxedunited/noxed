/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <nox/logic/actor/event/Event.h>

namespace nox
{
  namespace logic
  {
    namespace actor
    {
      class Actor;
    }
  }
}

namespace platformer { namespace events {

/**
 * An event telling an actor which one is selected.
 */
class Collision: public nox::logic::actor::Event
{
public:
	static const IdType ID;

  /**
   * Construct a tile placement event.
   *
   * @param actor Actor that is selected
   */
  Collision(nox::logic::actor::Actor* actor, nox::logic::actor::Actor* other, const std::string tag, const std::string otherTag);

  const std::string& getTag() const;
  const std::string& getContactTag() const;
  nox::logic::actor::Actor* getContact() const;

private:
  nox::logic::actor::Actor *other;
  std::string tag, otherTag;
};

}}
