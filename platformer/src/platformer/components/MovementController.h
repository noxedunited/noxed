/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <nox/logic/actor/Component.h>
#include <nox/logic/event/Event.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/app/log/Logger.h>
#include <glm/vec2.hpp>

namespace nox
{
	namespace logic
	{
		namespace actor
		{
			class Transform;
		}
		namespace physics
		{
			class ActorPhysics;
			class Simulation;
		}
	}
}

namespace platformer { namespace components
{

/**
 * Changes the actor's position based
 *
 * # JSON Description
 * ## Name
 * %Noxed::MovementController
 *
 * ## Properties
 * - __gridSize__:vec2 - Grid size for snapping. Set both components to 0 for no snapping
 * - __offset__:vec2 - Offset from mouse position. Use half the width and height to center it.
 */
class MovementController final: public nox::logic::actor::Component, public nox::logic::event::IListener
{
public:
	const static IdType NAME;

	MovementController();
	virtual ~MovementController();

	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;
	void onCreate() override;
	void onUpdate(const nox::Duration& deltaTime) override;
	const IdType& getName() const override;

	virtual void onComponentEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;
	virtual void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;

private:

	nox::logic::actor::Transform* actorTransform;
	nox::logic::physics::ActorPhysics* actorPhysics;
	nox::logic::physics::Simulation* simulation;
	nox::app::log::Logger log;
	nox::logic::event::ListenerManager listener;
	bool permitJump;
	glm::vec2 moveVector;

	glm::vec2 linearGravity;
	std::string moveAction;
	float moveForce;
	float moveMaxSpeed;
	std::string jumpAction;
	float jumpForce;
	bool enabled;
};

} }
