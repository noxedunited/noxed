/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "Collider.h"

#include <math.h>
#include <vector>

#include <glm/gtc/matrix_transform.hpp>

#include <nox/app/log/Logger.h>
#include <nox/logic/IContext.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/control/event/Action.h>
#include <nox/logic/control/actor/ActorControl.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/world/Manager.h>
#include <nox/util/json_utils.h>
#include <nox/util/chrono_utils.h>

#include <noxed/NoxedWindowView.h>
#include <noxed/events/MouseAction.h>

#include <platformer/events/Collision.h>

using nox::Duration;
using nox::logic::actor::Actor;
using nox::logic::actor::Component;
using nox::logic::actor::Transform;
using nox::logic::control::Action;
using nox::logic::event::Event;
using nox::logic::physics::ActorPhysics;
using nox::logic::physics::Simulation;
using nox::util::writeJsonVec;
using nox::util::parseJsonVec;
using nox::util::durationToSeconds;
using nox::logic::physics::CollisionData;
using noxed::events::MouseAction;
using platformer::events::Collision;

namespace platformer { namespace components
{

const Component::IdType Collider::NAME = "Platformer::Collider";

Collider::Collider():
	listener("Collider")
{
}

Collider::~Collider() = default;

bool Collider::initialize(const Json::Value& componentJsonObject)
{
	this->tag = componentJsonObject.get("tag", "NONE").asString();
	this->local = componentJsonObject.get("local", true).asBool();
	this->global = componentJsonObject.get("global", true).asBool();
	this->collidee = componentJsonObject.get("collidee", false).asBool();

	this->log = this->getLogicContext()->createLogger();

	return true;
}

void Collider::serialize(Json::Value& componentObject)
{
	componentObject["tag"] = this->tag;
	componentObject["local"] = this->local;
	componentObject["global"] = this->global;
	componentObject["collidee"] = this->collidee;
}

void Collider::onCreate()
{
	this->actorTransform = this->getOwner()->findComponent<Transform>();
	this->actorPhysics = this->getOwner()->findComponent<ActorPhysics>();
	this->simulation = this->getLogicContext()->getPhysics();
	this->world = this->getLogicContext()->getWorldManager();

	if(!collidee)
	{
		auto callback = std::bind(&Collider::onCollision, this, std::placeholders::_1);
		this->simulation->setActorCollisionCallback(this->getOwner()->getId(), callback);
	}
}

const Component::IdType& Collider::getName() const
{
	return NAME;
}

void Collider::onComponentEvent(const std::shared_ptr<Event>& event)
{
}

void Collider::onEvent(const std::shared_ptr<Event>& event)
{
}

void Collider::onCollision(const CollisionData &collisionData)
{
	if (collisionData.collidingActorId.isValid())
	{
		auto other = this->world->findActor(collisionData.collidingActorId);
		auto otherCollider = other->findComponent<Collider>();

		if(otherCollider != nullptr)
		{
			//this->log.info().format("Collision: %s (%i) -> %s (%i)", this->tag, this->getOwner()->getId(), otherCollider->tag, collisionData.collidingActorId);

			if(this->global || this->local)
			{
				auto event = std::make_shared<Collision>(this->getOwner(), other, this->tag, otherCollider->tag);

				if(this->global)
				{
					this->getLogicContext()->getEventBroadcaster()->queueEvent(event);
				}

				if(this->local)
				{
					this->getOwner()->broadCastComponentEvent(event);
				}
			}
		}
	}
	else
	{
		this->log.warning().format("Invalid contact with ID %i - ignored", collisionData.collidingActorId);
	}
}


} }
