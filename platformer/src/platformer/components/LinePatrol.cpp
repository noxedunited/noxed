/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "LinePatrol.h"

#include <math.h>
#include <vector>

#include <glm/gtc/matrix_transform.hpp>

#include <nox/app/log/Logger.h>
#include <nox/logic/IContext.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/control/event/Action.h>
#include <nox/logic/control/actor/ActorControl.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/world/Manager.h>
#include <nox/util/json_utils.h>
#include <nox/util/chrono_utils.h>

#include <noxed/NoxedWindowView.h>
#include <noxed/events/MouseAction.h>

#include <platformer/events/Collision.h>

using nox::Duration;
using nox::logic::actor::Actor;
using nox::logic::actor::Component;
using nox::logic::actor::Transform;
using nox::logic::control::Action;
using nox::logic::event::Event;
using nox::logic::physics::ActorPhysics;
using nox::util::writeJsonVec;
using nox::util::parseJsonVec;
using nox::util::durationToSeconds;
using noxed::events::MouseAction;
using platformer::events::Collision;

namespace platformer { namespace components
{

const Component::IdType LinePatrol::NAME = "Platformer::LinePatrol";

LinePatrol::LinePatrol()
{
}

LinePatrol::~LinePatrol() = default;

bool LinePatrol::initialize(const Json::Value& componentJsonObject)
{
	this->line = parseJsonVec(componentJsonObject["line"], glm::vec2(4.0f, 0.0f));
	this->speed = componentJsonObject.get("speed", 1).asFloat();
	this->threshold = componentJsonObject.get("threshold", 0.5).asFloat();

	this->log = this->getLogicContext()->createLogger();

	return true;
}

void LinePatrol::serialize(Json::Value& componentObject)
{
	componentObject["line"] = writeJsonVec(this->line);
	componentObject["speed"] = this->speed;
	componentObject["threshold"] = this->threshold;
}

void LinePatrol::onCreate()
{
	this->actorTransform = this->getOwner()->findComponent<Transform>();
	this->actorPhysics = this->getOwner()->findComponent<ActorPhysics>();

	if(this->actorTransform != nullptr && this->actorPhysics != nullptr)
	{
		this->start = this->actorTransform->getPosition();
		this->end = this->start + this->line;
		this->speedVector = glm::normalize(this->line) * this->speed;
		this->movingForward = true;
	}
	else
	{
		this->log.error().format("No Transform and/or Physics component found on actor %s (%i)", this->getOwner()->getName(), this->getOwner()->getId());
		this->getOwner()->deactivate();
	}
}

void LinePatrol::onUpdate(const Duration& deltaTime)
{
	auto position = this->actorTransform->getPosition();

	if(this->movingForward)
	{
		if(glm::distance(position, this->end) < this->threshold)
		{
			this->movingForward = false;
		}
		else
		{
			this->actorPhysics->setVelocity(this->speedVector);
		}
	}
	else
	{
		if(glm::distance(position, this->start) < this->threshold)
		{
			this->movingForward = true;
		}
		else
		{
			this->actorPhysics->setVelocity(-this->speedVector);
		}
	}
}

const Component::IdType& LinePatrol::getName() const
{
	return NAME;
}

} }
