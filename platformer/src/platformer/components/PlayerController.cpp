/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "PlayerController.h"

#include <math.h>
#include <vector>

#include <glm/gtc/matrix_transform.hpp>

#include <nox/app/log/Logger.h>
#include <nox/logic/IContext.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/control/event/Action.h>
#include <nox/logic/control/actor/ActorControl.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/world/Manager.h>
#include <nox/util/json_utils.h>
#include <nox/util/chrono_utils.h>

#include <noxed/NoxedWindowView.h>
#include <noxed/events/MouseAction.h>

#include <platformer/events/Collision.h>
#include <platformer/components/PowerUp.h>

using nox::Duration;
using nox::logic::actor::Actor;
using nox::logic::actor::Component;
using nox::logic::actor::Transform;
using nox::logic::control::Action;
using nox::logic::event::Event;
using nox::logic::physics::ActorPhysics;
using nox::util::writeJsonVec;
using nox::util::parseJsonVec;
using nox::util::durationToSeconds;
using noxed::events::MouseAction;
using platformer::events::Collision;

namespace platformer { namespace components
{

const Component::IdType PlayerController::NAME = "Platformer::PlayerController";

PlayerController::PlayerController():
	listener("PlayerController")
{
}

PlayerController::~PlayerController() = default;

bool PlayerController::initialize(const Json::Value& componentJsonObject)
{
	this->powerUpTag = componentJsonObject.get("powerUpTag", "powerup").asString();
	this->enemyTag = componentJsonObject.get("enemyTag", "enemy").asString();
	this->fallLimit = componentJsonObject.get("fallLimit", 0.0f).asFloat();

	if(componentJsonObject.isMember("spawnPoint"))
	{
		this->hasSpawnPoint = true;
		this->spawnPoint = parseJsonVec(componentJsonObject["spawnPoint"], glm::vec2(0.f, 0.f));
	}
	else
	{
		this->hasSpawnPoint = false;
	}

	this->log = this->getLogicContext()->createLogger();

	return true;
}

void PlayerController::serialize(Json::Value& componentObject)
{
	componentObject["powerUpTag"] = this->powerUpTag;
	componentObject["enemyTag"] = this->enemyTag;
	componentObject["fallLimit"] = this->fallLimit;
	componentObject["spawnPoint"] = writeJsonVec(this->spawnPoint);
}

void PlayerController::onCreate()
{
	this->actorTransform = this->getOwner()->findComponent<Transform>();
	this->actorPhysics = this->getOwner()->findComponent<ActorPhysics>();

	if(!this->hasSpawnPoint)
	{
		this->spawnPoint = this->actorTransform->getPosition();
	}
}

void PlayerController::onUpdate(const Duration& deltaTime)
{
	if(this->actorTransform != nullptr && this->actorTransform->getPosition().y < fallLimit)
	{
		this->kill();
	}
}

const Component::IdType& PlayerController::getName() const
{
	return NAME;
}

void PlayerController::onComponentEvent(const std::shared_ptr<Event>& event)
{
	if(event->isType(Collision::ID))
	{
		auto collision = static_cast<Collision*>(event.get());

		if(collision->getContactTag() == this->powerUpTag)
		{
			auto powerUp = collision->getContact()->findComponent<PowerUp>();

			if(powerUp != nullptr)
			{
				this->log.info().format("Picked up %s x%i", powerUp->getItemName(), powerUp->getItemAmount());
			}

			collision->getContact()->deactivate();
		}
		else if(collision->getContactTag() == this->enemyTag)
		{
			this->kill();
		}
	}
}

void PlayerController::onEvent(const std::shared_ptr<Event>& event)
{
}

void PlayerController::kill()
{
	this->log.info().raw("Player has been killed! D:");

	// Stop moving and get back to spawn.
	this->actorPhysics->setVelocity(glm::vec2(0.f, 0.f));
	this->actorPhysics->setPosition(this->spawnPoint);
}

} }
