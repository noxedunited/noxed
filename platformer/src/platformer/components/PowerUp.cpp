/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "PowerUp.h"

#include <math.h>
#include <vector>

#include <glm/gtc/matrix_transform.hpp>

#include <nox/app/log/Logger.h>
#include <nox/logic/IContext.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/control/event/Action.h>
#include <nox/logic/control/actor/ActorControl.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/world/Manager.h>
#include <nox/util/json_utils.h>
#include <nox/util/chrono_utils.h>

#include <noxed/NoxedWindowView.h>
#include <noxed/events/MouseAction.h>

#include <platformer/events/Collision.h>

using nox::Duration;
using nox::logic::actor::Actor;
using nox::logic::actor::Component;
using nox::logic::actor::Transform;
using nox::logic::control::Action;
using nox::logic::event::Event;
using nox::logic::physics::ActorPhysics;
using nox::util::writeJsonVec;
using nox::util::parseJsonVec;
using nox::util::durationToSeconds;
using noxed::events::MouseAction;
using platformer::events::Collision;

namespace platformer { namespace components
{

const Component::IdType PowerUp::NAME = "Platformer::PowerUp";

PowerUp::PowerUp()
{
}

PowerUp::~PowerUp() = default;

bool PowerUp::initialize(const Json::Value& componentJsonObject)
{
	this->itemName = componentJsonObject.get("itemName", "powerup").asString();
	this->itemAmount = componentJsonObject.get("itemAmount", 0).asInt();

	return true;
}

void PowerUp::serialize(Json::Value& componentObject)
{
	componentObject["itemName"] = this->itemName;
	componentObject["itemAmount"] = this->itemAmount;
}

void PowerUp::onCreate()
{
}

const Component::IdType& PowerUp::getName() const
{
	return NAME;
}

const std::string PowerUp::getItemName() const
{
	return this->itemName;
}

const int PowerUp::getItemAmount() const
{
	return this->itemAmount;
}

} }
