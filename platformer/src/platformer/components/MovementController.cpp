/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "MovementController.h"

#include <math.h>
#include <vector>

#include <glm/gtc/matrix_transform.hpp>

#include <nox/app/log/Logger.h>
#include <nox/logic/IContext.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/control/event/Action.h>
#include <nox/logic/control/actor/ActorControl.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/physics/Simulation.h>
#include <nox/logic/world/Manager.h>
#include <nox/util/json_utils.h>
#include <nox/util/chrono_utils.h>

#include <noxed/NoxedWindowView.h>
#include <noxed/events/MouseAction.h>

using nox::Duration;
using nox::logic::actor::Actor;
using nox::logic::actor::Component;
using nox::logic::actor::Transform;
using nox::logic::control::Action;
using nox::logic::event::Event;
using nox::logic::physics::ActorPhysics;
using nox::logic::physics::Simulation;
using nox::util::writeJsonVec;
using nox::util::parseJsonVec;
using nox::util::durationToSeconds;
using noxed::events::MouseAction;

namespace platformer { namespace components
{

const Component::IdType MovementController::NAME = "Platformer::MovementController";

MovementController::MovementController():
	listener("MovementController")
{
}

MovementController::~MovementController() = default;

bool MovementController::initialize(const Json::Value& componentJsonObject)
{
	this->linearGravity = parseJsonVec(componentJsonObject["linearGravity"], glm::vec2(0.0f, 9.81f));
	this->moveAction = componentJsonObject.get("moveAction", "").asString();
	this->moveForce = componentJsonObject.get("moveForce", 250.0f).asFloat();
	this->moveMaxSpeed = componentJsonObject.get("moveMaxSpeed", 1.0f).asFloat();
	this->jumpAction = componentJsonObject.get("jumpAction", "").asString();
	this->jumpForce = componentJsonObject.get("jumpForce", 500.0f).asFloat();
	this->enabled = componentJsonObject.get("enabled", true).asBool();

	this->permitJump = true;
	this->moveVector = glm::vec2(0, 0);

	if(this->moveAction != "")
	{
		this->listener.setup(this, this->getLogicContext()->getEventBroadcaster());
		this->listener.addEventTypeToListenFor(Action::ID);
		this->listener.startListening();
	}

	this->log = this->getLogicContext()->createLogger();


	return true;
}

void MovementController::serialize(Json::Value& componentObject)
{
	componentObject["linearGravity"] = writeJsonVec(this->linearGravity);
	componentObject["moveAction"] = this->moveAction;
	componentObject["moveForce"] = this->moveForce;
	componentObject["jumpAction"] = this->jumpAction;
	componentObject["jumpForce"] = this->jumpForce;
	componentObject["enabled"] = this->enabled;
}

void MovementController::onCreate()
{
	this->actorTransform = this->getOwner()->findComponent<Transform>();
	this->actorPhysics = this->getOwner()->findComponent<ActorPhysics>();
	this->simulation = this->getLogicContext()->getPhysics();

	if(this->actorTransform == nullptr || this->actorPhysics == nullptr)
	{
		this->log.error().format("%s component on actor %s cannot find Transform and/or Physics.", this->getName(), this->getOwner()->getName());
		this->enabled = false;
	}

	this->simulation->setUniversalGravity(this->linearGravity);
}

void MovementController::onUpdate(const Duration& deltaTime)
{
	if(enabled)
	{
		this->permitJump = true;

		if(abs(this->actorPhysics->getVelocity().x) < this->moveMaxSpeed)
		{
			this->actorPhysics->applyForce(this->moveVector * this->moveForce);
		}

		if(glm::length(this->moveVector) < 0.01)
		{
			auto v = this->actorPhysics->getVelocity();
			this->actorPhysics->applyForce(glm::vec2(-v.x * 25, 0));
		}
	}
}

const Component::IdType& MovementController::getName() const
{
	return NAME;
}

void MovementController::onComponentEvent(const std::shared_ptr<Event>& event)
{
}

void MovementController::onEvent(const std::shared_ptr<Event>& event)
{
	if (event->isType(Action::ID))
	{
		if(this->enabled && this->permitJump)
		{
			auto action = static_cast<Action*>(event.get());
			glm::vec2 vector = action->getControlVector();

			if(action->getControlName() == this->moveAction)
			{
				this->moveVector = glm::vec2(vector.x, 0.0f);
			}
			else if(action->getControlName() == this->jumpAction)
			{
				if(this->permitJump)
				{
					auto contacts = this->simulation->getActorContacts(this->getOwner()->getId());

					for(const auto& contact : contacts)
					{
						const glm::vec2 direction = contact.contactDirection;

						if(abs(direction.x) < 0.5 && direction.y < -0.1)
						{
							this->actorPhysics->applyForce(vector * this->jumpForce);
							this->permitJump = false;

							break;
						}
					}
				}
			}
		}
	}
}

} }
