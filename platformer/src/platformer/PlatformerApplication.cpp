/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "PlatformerApplication.h"

#include <nox/logic/world/Manager.h>

#include <noxed/NoxedWindowView.h>

#include <platformer/components/MovementController.h>
#include <platformer/components/Collider.h>
#include <platformer/components/PlayerController.h>
#include <platformer/components/PowerUp.h>
#include <platformer/components/LinePatrol.h>

namespace platformer
{

PlatformerApplication::PlatformerApplication(const char *projectName) :
  NoxedApplication(projectName)
{
}

bool PlatformerApplication::onInit()
{
  this->worldLoadCallbacks.push_back([=](nox::logic::world::Manager* world)
  {
  	world->registerActorComponent<platformer::components::MovementController>();
    world->registerActorComponent<platformer::components::Collider>();
    world->registerActorComponent<platformer::components::PlayerController>();
    world->registerActorComponent<platformer::components::PowerUp>();
    world->registerActorComponent<platformer::components::LinePatrol>();
  });

  if(!NoxedApplication::onInit())
  {
    return false;
  }
}

}
