get_filename_component(SAMPLE_NAME "${CMAKE_CURRENT_LIST_DIR}" NAME)
project(${SAMPLE_NAME})
source_group(" ")

set(NOXED_PATH ../Noxed/)
set(THIRD_PATH ../third-party/)

include(${CMAKE_CURRENT_SOURCE_DIR}/../Noxed/CMakeLists.txt)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/../Noxed/)

include(${CMAKE_CURRENT_SOURCE_DIR}/../third-party/CMakeLists.txt)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/../third-party/)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src/)

set(SOURCES
	${SOURCES}
	src/topdown/main.cpp
	src/topdown/TopDownApplication.h
	src/topdown/TopDownApplication.cpp
	src/topdown/components/PlayerController.h
	src/topdown/components/PlayerController.cpp
	src/topdown/components/PowerUp.h
	src/topdown/components/PowerUp.cpp
	src/topdown/components/Enemy.h
	src/topdown/components/Enemy.cpp
	src/topdown/events/SpeedChange.h
	src/topdown/events/SpeedChange.cpp
	src/topdown/events/KillActor.h
	src/topdown/events/KillActor.cpp	
)

add_executable(${SAMPLE_NAME} ${SOURCES})
target_link_libraries(${SAMPLE_NAME} ${NOXSAMPLE_NOX_LIBRARY})
