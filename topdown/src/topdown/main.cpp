/*
* NOX Engine
*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* This file was copied from https://bitbucket.org/suttungdigital/nox-engine-sample and modified
* by Tor Str�mme and Gisle Aune.
*/

#include "TopDownApplication.h"

int main(int argc, char* argv[])
{
  /*
  * Create our very own Example application the inherits from SdlApplciation. The application
  * and organization name is passed from the ExampleApplciation constructor.
  */
  auto application = topdown::TopDownApplication("topdown");

  /*
  * With our custom ExampleApplication this should in addition output "I've successfully initialized my very own application!"
  * to the console.
  */
  if (application.init(argc, argv) == false)
  {
    return 1;
  }

  const auto result = application.execute();

  application.shutdown();
  return 0;
}
