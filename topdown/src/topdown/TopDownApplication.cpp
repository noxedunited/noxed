#include "TopDownApplication.h"
#include <noxed/NoxedWindowView.h>

#include <nox/logic/world/Manager.h>

#include <topdown/components/PlayerController.h>
#include <topdown/components/PowerUp.h>
#include <topdown/components/Enemy.h>

namespace topdown {
  TopDownApplication::TopDownApplication(const char *projectName) :
    NoxedApplication(projectName)
  {
  }

  bool TopDownApplication::onInit()
  {
    this->worldLoadCallbacks.push_back([=](nox::logic::world::Manager* world)
    {
      world->registerActorComponent<topdown::components::PlayerController>();
      world->registerActorComponent<topdown::components::PowerUp>();
      world->registerActorComponent<topdown::components::Enemy>();
    });
    if (NoxedApplication::onInit())
    {
      return true;
    }
    return false;
  }


}