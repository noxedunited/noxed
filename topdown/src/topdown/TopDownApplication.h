

#pragma once

#include <noxed/NoxedApplication.h>

namespace topdown
{
  //!  The Topdown application
  /*!
  Extends the NoxedAppplication Class. Contains everything that is topdown exclusive.
  */
  class TopDownApplication final : public NoxedApplication
  {
  public:
    TopDownApplication(const char *projectName);
    virtual bool onInit() override;

  private:

  };
}