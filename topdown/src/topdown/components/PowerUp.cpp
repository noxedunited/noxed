/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "PowerUp.h"

#include <topdown/events/SpeedChange.h>
#include <nox/logic/IContext.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/View.h>


#include <iostream>
namespace topdown {
  namespace components
  {
    const nox::logic::actor::Component::IdType PowerUp::NAME = "Topdown::PowerUp";
    PowerUp::PowerUp():
      damping(0.0f)
    {
    }


    PowerUp::~PowerUp() = default;


    void PowerUp::powerUpActor(nox::logic::actor::Actor *actor)
    {
      auto speedEvent = std::make_shared<topdown::events::SpeedChange>(this->damping, this->getOwner());
      this->broadcaster->queueEvent(speedEvent);
      this->getOwner()->deactivate();
    }


    void PowerUp::onCreate()
    {
     
    }


    bool PowerUp::initialize(const Json::Value & componentJsonObject)
    {
      this->damping = componentJsonObject.get("damping", 1).asFloat();
      this->log = this->getLogicContext()->createLogger();
      return true;
    }

    void PowerUp::onActivate()
    {
      this->broadcaster = this->getLogicContext()->getEventBroadcaster();
      this->setCollisionCallback();
    }


    const nox::logic::actor::Component::IdType & PowerUp::getName() const
    {
      return NAME;
    }


    void PowerUp::collisionFunction(const nox::logic::physics::CollisionData &collisionData)
    {
      if (collisionData.collidingActorId.isValid())
      {
        auto collidingActor = this->getLogicContext()->getWorldManager()->findActor(collisionData.collidingActorId);
        if (collidingActor->getName() == "Player") {
          powerUpActor(collidingActor);
        }

      }
    }

    void PowerUp::setCollisionCallback()
    {
      auto physics = this->getLogicContext()->getPhysics();

      auto actor_id = this->getOwner()->getId();
  
      if (actor_id.isValid()) {
        this->log.info().format("Actor ID is valid: %.2f", actor_id);
      }
      else
      {
        this->log.info().raw("No Player ID found!");
      }

      auto callback = [=](const nox::logic::physics::CollisionData &collisionData)
      {
        this->collisionFunction(collisionData);
      };
      physics->setActorCollisionCallback(actor_id, callback);
    }

    void PowerUp::serialize(Json::Value & componentObject)
    {
    }
  }
}