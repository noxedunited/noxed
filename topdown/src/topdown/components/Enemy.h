
/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <nox/logic/actor/Component.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/app/log/Logger.h>
#include <nox/logic/physics/Simulation.h>


namespace nox {
  namespace logic {
    namespace actor {
      class Actor;
    }
    namespace physics {
      class ActorPhysics;
    }
  }
}

namespace topdown {
  namespace components
  {
    //!  Topdown Enemey Class
    /*!
    This class controls the enemy actors behaviour.
    */
    class Enemy final : public nox::logic::actor::Component
    {
    public:
      const static IdType NAME;

      Enemy();
      ~Enemy();

      void killActor(nox::logic::actor::Actor *actor);

      void onCreate() override;
      const IdType& getName() const override;
      void collisionFunction(const nox::logic::physics::CollisionData & collisionData);
      void setCollisionCallback();
      bool initialize(const Json::Value& componentJsonObject) override;
      void onActivate() override;
      void serialize(Json::Value& componentObject) override;
    private:
      nox::logic::physics::ActorPhysics* actorPhysics;
      void startMoving();
      nox::app::log::Logger log;
      nox::logic::event::IBroadcaster *broadcaster;
    };

  }
}
