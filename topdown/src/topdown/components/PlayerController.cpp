/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "PlayerController.h"

#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/physics/physics_utils.h>

#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/IContext.h>
#include <nox/util/chrono_utils.h>

#include <noxed/events/MouseAction.h>
#include <topdown/events/SpeedChange.h>
#include <topdown/events/KillActor.h>

#include <math.h>



using nox::logic::physics::ActorPhysics;
using nox::logic::actor::Transform;
using noxed::events::MouseAction;

namespace topdown {
  namespace components
  {
    const nox::logic::actor::Component::IdType PlayerController::NAME = "Topdown::PlayerController";
    PlayerController::PlayerController():
      actorPhysics(nullptr),
      rotation(0.0f),
      damping(0.0f),
      listener("PlayerController")
    {
    }

    PlayerController::~PlayerController() = default;


    void PlayerController::onCreate()
    {
      this->actorPhysics = this->getOwner()->findComponent<ActorPhysics>();
      this->actorTransform = this->getOwner()->findComponent<Transform>();
      this->spawnPoint = this->actorTransform->getPosition();
    }


    bool PlayerController::initialize(const Json::Value & componentJsonObject)
    {
      this->damping = componentJsonObject.get("damping", 0.0f).asFloat();
      this->listener.setup(this, this->getLogicContext()->getEventBroadcaster());
      this->listener.addEventTypeToListenFor(MouseAction::MOVE);
      this->listener.addEventTypeToListenFor(topdown::events::SpeedChange::ID);
      this->listener.addEventTypeToListenFor(topdown::events::KillActor::ID);
      this->listener.startListening();
      return true;
    }


    const nox::logic::actor::Component::IdType & PlayerController::getName() const
    {
      return NAME;
    }


    void PlayerController::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
    {
      if (event->isType(MouseAction::MOVE))
      {
        this->mouseTurn(event);
      }
      if (event->isType("topdown.speedchange"))
      {
        this->speedChange(event);
      }
      if (event->isType("topdown.killactor"))
      {
        this->kill(event);
      }
    }


    void PlayerController::serialize(Json::Value & componentObject)
    {
    }


    void PlayerController::mouseTurn(const std::shared_ptr<nox::logic::event::Event>& event)
    {
      auto action = std::static_pointer_cast<MouseAction>(event);
      auto mouseVector = action->getWorldPosition();
      auto playerVector = this->actorTransform->getPosition();

      rotation = atan2(playerVector.y - mouseVector.y, playerVector.x - mouseVector.x);
      this->actorPhysics->rotateTo(rotation);
    }


    void PlayerController::speedChange(const std::shared_ptr<nox::logic::event::Event>& event)
    {
      auto action = std::static_pointer_cast<topdown::events::SpeedChange>(event);
      this->damping -= action->getDamping();

      if (this->damping >= 3.0)
      {
        this->actorPhysics->setLinearDamping(damping);
      }
    }


    void PlayerController::kill(const std::shared_ptr<nox::logic::event::Event>& event)
    {
      auto action = std::static_pointer_cast<topdown::events::KillActor>(event);
      if (action->getActor() == this->getOwner())
      {
        this->actorPhysics->setVelocity(glm::vec2(0.f, 0.f));
        this->actorPhysics->setPosition(this->spawnPoint);
      }
    }
  }
}