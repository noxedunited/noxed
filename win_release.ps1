Copy-Item nox-engine\assets ..\release\nox-engine\assets -recurse
Copy-Item topdown\assets ..\release\topdown\assets -recurse
Copy-Item platformer\assets ..\release\platformer\assets -recurse
Copy-Item ..\build\bin\Release\* ..\release
md ..\release\noxed-editor
Move-Item ..\release\noxed-editor.exe ..\release\noxed-editor

'start "" "%~dp0\noxed-editor\noxed-editor.exe" topdown' | Out-File topdown-editor.bat -enc ascii
'start "" "%~dp0\noxed-editor\noxed-editor.exe" platformer' | Out-File platformer-editor.bat -enc ascii
Move-Item topdown-editor.bat ..\release
Move-Item platformer-editor.bat ..\release

Copy-Item ..\windows-libraries\msvc\vc140\bin\x86\* ..\release
Read-Host -Prompt "Press Any Key to Exit"