#!/bin/sh
echo "*** (Re)building the release version..."
rm -r build-release
mkdir build-release
cd build-release
cmake ../source/ -DCMAKE_CXX_COMPILER=g++-5 -DCMAKE_BUILD_TYPE=Release -DNOX_AUDIO_OPENAL=OFF && make -j2
cd ..

# Clean up earlier release
if [ -d "release" ]; then
  echo "*** Removing old release..."
  rm -r release
fi

echo "*** Making directory structure for release..."
mkdir -p release/platformer/nox-engine/
mkdir -p release/platformer/platformer/
mkdir -p release/platformer/lib/
mkdir -p release/platformer/bin/
mkdir -p release/topdown/nox-engine/
mkdir -p release/topdown/topdown/
mkdir -p release/topdown/lib/
mkdir -p release/topdown/bin/

echo "*** Copying executables..."
cp build-release/bin/noxed-editor release/platformer/bin/editor
cp build-release/bin/platformer release/platformer/bin/game
cp build-release/bin/noxed-editor release/topdown/bin/editor
cp build-release/bin/topdown release/topdown/bin/game

echo "*** Copying assets..."
cp -r source/nox-engine/assets release/platformer/nox-engine
cp -r source/platformer/assets release/platformer/platformer
cp -r source/nox-engine/assets release/topdown/nox-engine
cp -r source/topdown/assets release/topdown/topdown

echo "*** Copying launch scripts..."
echo "#!/bin/sh\nLD_LIBRARY_PATH=\"$(pwd)/lib:$LD_LIBRARY_PATH\" ./bin/editor platformer $@" >release/platformer/run-editor.sh
echo "#!/bin/sh\nLD_LIBRARY_PATH=\"$(pwd)/lib:$LD_LIBRARY_PATH\" ./bin/game $@" >release/platformer/run-game.sh
echo "#!/bin/sh\nLD_LIBRARY_PATH=\"$(pwd)/lib:$LD_LIBRARY_PATH\" ./bin/editor topdown $@" >release/topdown/run-editor.sh
echo "#!/bin/sh\nLD_LIBRARY_PATH=\"$(pwd)/lib:$LD_LIBRARY_PATH\" ./bin/game $@" >release/topdown/run-game.sh
chmod +x release/platformer/run-editor.sh
chmod +x release/platformer/run-game.sh
chmod +x release/topdown/run-editor.sh
chmod +x release/topdown/run-game.sh

echo "*** Copying system libraries..."
mkdir temp-libs
cp $(ldd release/platformer/bin/editor | awk '{print $3}' | grep \.so) temp-libs/
cp temp-libs/libboost_* release/platformer/lib/
cp temp-libs/libstdc++* release/platformer/lib/
cp temp-libs/libboost_* release/topdown/lib/
cp temp-libs/libstdc++* release/topdown/lib/
rm -r temp-libs

# If there are any libraries built
if [ -d "build-release/lib/"
	cp build-release/lib/* release/topdown/lib/
fi

# If openal is there
if [ -f "build-release/nox-engine/third_party/openal-soft/libopenal.so" ]; then
	cp build-release/nox-engine/third_party/openal-soft/libopenal.so* release/platformer/lib/
	cp build-release/nox-engine/third_party/openal-soft/libopenal.so* release/topdown/lib/
fi

echo "*** Copying license..."
cp source/LICENSE.md release/platformer/
cp source/LICENSE.md release/topdown/
