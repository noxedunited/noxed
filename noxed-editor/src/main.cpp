/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Strømme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "NoxedEditorApplication.h"
#include <iostream>

int main(int argc, char* argv[])
{
  /*
  * If an argument is provided, use that as project name. This is for reusing
  * the same editor for multiple projects without requiring symlink nonsense
  * or copying stuff out.
  */
  std::string projectName;
  if(argc == 2 && argv[1][0] != '-')
  {
    projectName = argv[1];
  }
  else
  {
    projectName = "noxed-editor";
  }

  /*
  * Create our very own Example application the inherits from SdlApplciation. The application
  * and organization name is passed from the ExampleApplciation constructor.
  */
  auto application = NoxedEditorApplication(projectName.c_str());

  /*
  * With our custom ExampleApplication this should in addition output "I've successfully initialized my very own application!"
  * to the console.
  */
  if (application.init(argc, argv) == false)
  {
    system("pause");
    return 1;
  }

  const auto result = application.execute();

  application.shutdown();
  return 0;
}
