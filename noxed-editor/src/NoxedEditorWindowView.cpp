/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "NoxedEditorWindowView.h"
#include <iostream>
#include <nox/logic/IContext.h>
#include <nox/app/graphics/2d/IRenderer.h>
#include <nox/window/SdlWindowView.h>

NoxedEditorWindowView::NoxedEditorWindowView(nox::app::IContext* appContext, const std::string& windowTitle) :
  NoxedWindowView(appContext, windowTitle),
  guiRenderer(nullptr)
{

}

void NoxedEditorWindowView::onSdlEvent(const SDL_Event & event)
{
  this->suppressEvent = false;
  this->dearImguiProcessEvent(&event);

  if(!this->suppressEvent)
  {
    this->NoxedWindowView::onSdlEvent(event);
  }
}


void NoxedEditorWindowView::onRendererCreated(nox::app::graphics::IRenderer* renderer)
{
  NoxedWindowView::onRendererCreated(renderer);
  renderer->addSubRenderer(std::move(guiRenderer));
}

bool NoxedEditorWindowView::onWindowCreated(SDL_Window * window)
{
  this->guiRenderer = std::unique_ptr<nox::app::graphics::SubRenderer>{ std::make_unique<noxed::gui::DearImguiSubRenderer>(window, this->getLogicContext()) };
  if (RenderSdlWindowView::onWindowCreated(window))
  {
    return true;
  }

  return false;
}

bool NoxedEditorWindowView::dearImguiProcessEvent(const SDL_Event * event)
{
  static bool         mousePressed[3] = { false, false, false };
  static float        mouseWheel = 0.0f;

  ImGuiIO& io = ImGui::GetIO();
  switch (event->type)
  {
  case SDL_MOUSEWHEEL:
  {
    if (event->wheel.y > 0)
      mouseWheel = 1;
    if (event->wheel.y < 0)
      mouseWheel = -1;
    return true;
  }
  case SDL_MOUSEBUTTONDOWN:
  {
    if (event->button.button == SDL_BUTTON_LEFT) mousePressed[0] = true;
    if (event->button.button == SDL_BUTTON_RIGHT) mousePressed[1] = true;
    if (event->button.button == SDL_BUTTON_MIDDLE) mousePressed[2] = true;
    return true;
  }
  case SDL_TEXTINPUT:
  {
    ImGuiIO& io = ImGui::GetIO();
    io.AddInputCharactersUTF8(event->text.text);

    return true;
  }
  case SDL_KEYDOWN:
  case SDL_KEYUP:
  {
    int key = event->key.keysym.sym & ~SDLK_SCANCODE_MASK;
    io.KeysDown[key] = (event->type == SDL_KEYDOWN);
    io.KeyShift = ((SDL_GetModState() & KMOD_SHIFT) != 0);
    io.KeyCtrl = ((SDL_GetModState() & KMOD_CTRL) != 0);
    io.KeyAlt = ((SDL_GetModState() & KMOD_ALT) != 0);

    if(io.WantTextInput)
    {
      this->suppressEvent = true;
    }

    return true;
  }
  }
  return false;

}
