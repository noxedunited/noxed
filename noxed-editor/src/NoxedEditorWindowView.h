//!  Extends the NoxedWindowView Class to setup gui-renderer.
/*!
  This class is only used in the editor.

*/
/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <noxed/NoxedWindowView.h>
#include "noxed/gui/DearImguiSubRenderer.h"


class Icontext;

class NoxedEditorWindowView final : public NoxedWindowView
{
public:
  NoxedEditorWindowView(nox::app::IContext* applicationContext, const std::string& windowTitle);

private:
  /*!
  *Sends the SDL event to the GUI before sending it to the WindowView function.
  *@param[event] The SDL event that is managed.
  !*/
  void onSdlEvent(const SDL_Event& event) override;


  /*!
  *Adds the GUI-subrenderer that is created in onWindowCreated(...)
  *@param[renderer] The active graphics renderer.
  !*/
  void onRendererCreated(nox::app::graphics::IRenderer* renderer) override;
  std::unique_ptr<nox::app::graphics::SubRenderer> guiRenderer;

  /*!
  *Creates the subrenderer if the window was succesfully created.
  *@param[window] The active SDL window.
  !*/
  virtual bool onWindowCreated(SDL_Window* window) override;

  /*!
  *Handles input for the GUI
  *This function is copied from the example found at:
  *https://github.com/ocornut/imgui/tree/master/examples/sdl_opengl3_example
  *@param[event] Input event to be handled.
  !*/
  bool dearImguiProcessEvent(const SDL_Event* event);

  bool suppressEvent;
};
