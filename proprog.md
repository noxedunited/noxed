# proprog.md

This is the written assignment for the professional programming course. All file references will be local to `Noxed/noxed/` unless otherwise specified.

## The good
### TileGrid (tileset/TileGrid)
I think that data structure has clean, easy to understand code. It's mostly used by other components.

### ExposedComponent / ExposedArgument (expose/\*)
This is a rather simple class that powers a lot of the editable values of actors placed in the editor.

### WorldServer (components/WorldSaver)
For doing the most important task, it's very concise and clean. And all the to-be-saved objects need to do is submit JSON data to it, and tell it whether that is the exported level's "controlled actor".

### GridCursor (components/GridCursor)
This used to be a royal mess, doing more than simply making the object a grid-snapping cursor. However, after gutting out the non-cursor parts, it turned out to be very clean and readable. It means that `TileSpawner` and `EntitySpawner` can do their thing without bothering with making the object move.

### GuiHandler (gui)
We think that this class has a program flow that is easy to follow. It used to have a messy data structure in the header
file. But that was also fixed before delivery.

## The bad
### CMaking (various CMakeLists.txt files)
The entire CMake is the biggest offender, as it is modified just enough to barely hold together. You will even see `NOXSAMPLE` here and there. The platformer and topdown projects should be their own repositories.

### Camera management (components/PushCamera, components/FollowCamera)
The camera is accessed by getting the view controlling the actor and taking its camera with a public function. That goes against the components independence, and it should instead have been a request/response ordeal.

### BrushPatternManager is a singleton (brush/BrushPatternManager)
To not bother with managing the lifespan, I made this a singleton accessible from everywhere. While it works, and there's no issues with it, I think it is rather ugly. Not to mention that everything other than its lifespan is done in the NoxedWindowView class.

### Magical crash-preventing logging (components/EditorBlueprint.cpp:204)
I don't know how, and I definitely don't know why, but that log line is crucial to the value being settable without crashing. Might be a bug with jsoncpp. This usually hidden debug line does the trick, though, so I focused on more important matters than debugging it further. And no, just the `it->second->getValue()` is not enough.
```
this->log.debug().format("EntityBlueprint set(%s = %s)", key, it->second->getValue());
it->second->setValue(valueChange->getValue());
```

### DearImguiSubrenderer (gui)
This class is not especially well implemented in a proffesional sense. It works and is efficient, but the code
is mostly hacked together from the library examples. We should have a deeper understanding of how it works.

## Reviews
**Gisle**: I only had one review of my code, as reviews weren't part of most lectures I went to. The pertinent code is now on `components/GridCursor.cpp:164` and until the end of the function. What I did was using the implicit property of int-conversion for rounding rather than explicit rounding. 

**Tor**: The only reviews I had were from Gisle and they were helpful. I have no particular example, but mainly it was related to keeping naming consistent and useful. I feel that I am much better at it now.

## Hindsight
As is mentioned in the report, we should have had the UI be one of the first things we nailed down. That way, we wouldn't have as many stand-ins and hotkeys without a GUI counterpart. I think an interface would also be a better way of doing it rather than having the same function names used in multiple components.

We started out knowing nothing about the engine. We should have had the setup bit and learned it during the first few weeks of the semester. Then, we might have had a better understanding of the component model and time to clean up the mess before it spread.
