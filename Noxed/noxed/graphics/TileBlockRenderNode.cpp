/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "TileBlockRenderNode.h"

#include <nox/app/graphics/2d/SpriteRenderNode.h>
#include <nox/app/graphics/2d/OpenGlRenderer.h>

#include <noxed/tileset/TileSet.h>
#include <iostream>

using nox::app::graphics::TextureRenderer;
using nox::app::graphics::IRenderer;
using nox::app::graphics::TextureManager;
using nox::app::graphics::TextureQuad;
using noxed::tileset::TileSet;
using noxed::tileset::TileGrid;

namespace noxed { namespace graphics {

TileBlockRenderNode::TileBlockRenderNode(TileSet *set, TileGrid::Block *block):
  set(set),
  block(block),
  needUpdate(true),
  previousMatrix(1.0f),
  renderedTileSize(1.f, 1.f),
  color(1.0f, 1.0f, 1.0f, 1.0f),
  lightMultiplier(1.0f),
  emissiveLight(0.0f),
  levelID(0),
  prevDataSize(0)
{
}

void TileBlockRenderNode::markAsEdited()
{
  this->needUpdate = true;
}

void TileBlockRenderNode::setColor(const glm::vec4& color)
{
  this->color = color;
	this->needUpdate = true;
}

void TileBlockRenderNode::setEmissiveLight(float emissive)
{
  this->emissiveLight = emissive;
	this->needUpdate = true;
}

void TileBlockRenderNode::setLightMultiplier(float multiplier)
{
  this->lightMultiplier = multiplier;
	this->needUpdate = true;
}

void TileBlockRenderNode::setRenderLevel(const unsigned int level)
{
  this->levelID = level;

	if (this->dataHandle.isValid())
	{
		this->getCurrentRenderer()->getTextureRenderer().deleteData(this->dataHandle);
		this->dataHandle = this->getCurrentRenderer()->getTextureRenderer().requestDataSpace(this->block->blockWidth * this->block->blockWidth, this->levelID);
    this->needUpdate = true;
  }
}


void TileBlockRenderNode::onNodeEnter(TextureRenderer& renderData, glm::mat4x4& modelMatrix)
{
  if(block == nullptr)
  {
    this->needUpdate = false;
    return;
  }

  if (modelMatrix != this->previousMatrix || this->needUpdate == true)
	{
    std::vector<TextureQuad::RenderQuad> quads;

    std::vector<int>& tiles = this->block->tiles;
    for(int i = 0; i < tiles.size(); ++i)
    {
      if(tiles[i] >= 0 && tiles[i] <= this->tileQuads->size())
      {
        const int xIndex = i % this->block->blockWidth;
        const int yIndex = i / this->block->blockWidth;
        const float xOffset = ((this->block->x * this->block->blockWidth) + xIndex) * this->renderedTileSize.x;
        const float yOffset = ((this->block->y * this->block->blockWidth) + yIndex) * this->renderedTileSize.y;

        glm::mat4 translatedMatrix = glm::translate(modelMatrix, glm::vec3(xOffset, yOffset, 0));
        auto transformedQuad = transform(this->tileQuads->at(tiles[i]).getRenderQuad(), translatedMatrix);

        transformedQuad.setColor(this->color);
        transformedQuad.setLightMultiplier(this->lightMultiplier);
        transformedQuad.setEmissiveLight(this->emissiveLight);

        quads.push_back(transformedQuad);
      }
    }

    // Pad it out with empty quads to prevent "ghosts" of tiles of yore
    while(quads.size() < this->block->blockWidth * this->block->blockWidth)
    {
      quads.push_back(TextureQuad::RenderQuad());
    }

    renderData.setData(this->dataHandle, quads);

		this->previousMatrix = modelMatrix;
    this->prevDataSize = quads.size();
    this->needUpdate = false;
	}
}

void TileBlockRenderNode::onAttachToRenderer(IRenderer* renderer)
{
  const TextureManager& spriteManager = renderer->getTextureManager();

  this->dataHandle = renderer->getTextureRenderer().requestDataSpace(this->block->blockWidth * this->block->blockWidth, levelID);
  this->tileQuads = this->set->getQuads(spriteManager);
}

void TileBlockRenderNode::onDetachedFromRenderer()
{
  this->getCurrentRenderer()->getTextureRenderer().deleteData(this->dataHandle);
}


}}
