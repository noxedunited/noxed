/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "BrushSpriteRenderNode.h"

#include <nox/app/graphics/2d/SpriteRenderNode.h>
#include <nox/app/graphics/2d/OpenGlRenderer.h>

#include <noxed/brush/BrushPattern.h>

#include <iostream>

using nox::app::graphics::TextureRenderer;
using nox::app::graphics::IRenderer;
using nox::app::graphics::TextureManager;
using nox::app::graphics::TextureQuad;
using noxed::brush::BrushPattern;

namespace noxed { namespace graphics {

BrushSpriteRenderNode::BrushSpriteRenderNode(const std::string spriteName, const unsigned int brushSize, const unsigned int maxSize):
  spriteName(spriteName),
  brushSize(brushSize),
  maxSize(maxSize),
  needUpdate(true),
  previousMatrix(1.0f),
  renderedSquareSize(1.f, 1.f),
  color(1.0f, 1.0f, 1.0f, 1.0f),
  lightMultiplier(1.0f),
  emissiveLight(0.0f),
  levelID(0),
  pattern(nullptr),
  scale(1.0)
{
}

void BrushSpriteRenderNode::setColor(const glm::vec4& color)
{
  this->color = color;
	this->needUpdate = true;
}

void BrushSpriteRenderNode::setEmissiveLight(float emissive)
{
  this->emissiveLight = emissive;
	this->needUpdate = true;
}

void BrushSpriteRenderNode::setLightMultiplier(float multiplier)
{
  this->lightMultiplier = multiplier;
	this->needUpdate = true;
}

void BrushSpriteRenderNode::setRenderLevel(const unsigned int level)
{
  this->levelID = level;

	if (this->dataHandle.isValid())
	{
		this->getCurrentRenderer()->getTextureRenderer().deleteData(this->dataHandle);
		this->dataHandle = this->getCurrentRenderer()->getTextureRenderer().requestDataSpace(this->block->blockWidth * this->block->blockWidth, this->levelID);
    this->needUpdate = true;
  }
}

void BrushSpriteRenderNode::setBrushSize(const unsigned int size)
{
  this->brushSize = size;
  this->needUpdate = true;
}

void BrushSpriteRenderNode::setBrushPattern(noxed::brush::BrushPattern *pattern)
{
  this->pattern = pattern;
  this->needUpdate = true;
}

void BrushSpriteRenderNode::setScale(const float scale)
{
  this->scale = scale;
  this->needUpdate = true;
}

void BrushSpriteRenderNode::onNodeEnter(TextureRenderer& renderData, glm::mat4x4& modelMatrix)
{
  if(pattern == nullptr)
  {
    this->needUpdate = false;
    return;
  }

  if (modelMatrix != this->previousMatrix || this->needUpdate == true)
	{
    std::vector<TextureQuad::RenderQuad> quads;

    std::vector<glm::ivec2> core = this->pattern->getCore(this->brushSize);
    std::vector<glm::ivec2> edge = this->pattern->getEdge(this->brushSize);

    std::vector<glm::ivec2> offsets;
    offsets.reserve(core.size() + edge.size());
    offsets.insert(offsets.end(), core.begin(), core.end());
    offsets.insert(offsets.end(), edge.begin(), edge.end());

    for(int i = 0; i < offsets.size(); ++i)
    {
      const float xOffset = this->renderedSquareSize.x * offsets[i].x;
      const float yOffset = this->renderedSquareSize.y * offsets[i].y;

      glm::mat4 translatedMatrix = glm::translate(modelMatrix, glm::vec3(xOffset, yOffset, 0));
      translatedMatrix = glm::scale(translatedMatrix, glm::vec3(this->scale, this->scale, 1.0));
      auto transformedQuad = transform(this->spriteQuad.getRenderQuad(), translatedMatrix);

      transformedQuad.setColor(this->color);
      transformedQuad.setLightMultiplier(this->lightMultiplier);
      transformedQuad.setEmissiveLight(this->emissiveLight);

      quads.push_back(transformedQuad);
    }

    // Pad it out with empty quads to prevent "ghosts" of previous brush grid cells
    // size is radius, so a full brush would be 4 times the square of max size
    while(quads.size() < this->maxSize * this->maxSize * 4)
    {
      quads.push_back(TextureQuad::RenderQuad());
    }

    renderData.setData(this->dataHandle, quads);

		this->previousMatrix = modelMatrix;
    this->needUpdate = false;
	}
}

void BrushSpriteRenderNode::onAttachToRenderer(IRenderer* renderer)
{
  const TextureManager& spriteManager = renderer->getTextureManager();

  this->dataHandle = renderer->getTextureRenderer().requestDataSpace(this->maxSize * this->maxSize * 4, levelID);
  this->spriteQuad = spriteManager.getTexture(this->spriteName);
}

void BrushSpriteRenderNode::onDetachedFromRenderer()
{
  this->getCurrentRenderer()->getTextureRenderer().deleteData(this->dataHandle);
}


}}
