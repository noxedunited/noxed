/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <nox/app/graphics/2d/SceneGraphNode.h>
#include <nox/app/graphics/TextureQuad.h>
#include <nox/app/graphics/TextureRenderer.h>

#include <noxed/tileset/TileGrid.h>

namespace noxed
{
  namespace tileset
  {
    class TileSet;
  }
}

namespace noxed { namespace graphics {

/**
 * Renders a block of tiles using data from a provided tileset.
 */
class TileBlockRenderNode: public nox::app::graphics::SceneGraphNode
{
public:
  /**
   * Construct the render node
   *
   * @param set The tile set to get the tiles from
   * @param block The block this render node is supposed to render
   */
  TileBlockRenderNode(noxed::tileset::TileSet *set, noxed::tileset::TileGrid::Block *block);

  /**
   * Signal that the node needs updating, which is processed when the scene
   * graph is traversed to look for nodes needing update. This operation just
   * sets a bool, so it can be called multiple times during a frame without
   * ill effect; for example, when you edit a group of tiles within the same
   * block.
   */
  void markAsEdited();

  /**
  *  Set the color of the tile set.
  *  @param color to be set.
  */
  void setColor(const glm::vec4& color);

  /**
  *  Set the emmisive lighting of the tile set.
  *  @param emmisive to be set.
  */
  void setEmissiveLight(float emissive);

  /**
  *  Set the multiplier of the tile set lighting.
  *  @param multiplier to be set.
  */
  void setLightMultiplier(float multiplier);

  /**
   * Set and, if necessary, updates the renderlevel of the spriteRenderNode.
   * @param level the new level to set for the sprite.
   */
  void setRenderLevel(const unsigned int level);

private:
  void onNodeEnter(nox::app::graphics::TextureRenderer& renderData, glm::mat4x4& modelMatrix) override;
  void onAttachToRenderer(nox::app::graphics::IRenderer* renderer) override;
  void onDetachedFromRenderer() override;

  nox::app::graphics::TextureRenderer::DataHandle dataHandle;
  std::vector<nox::app::graphics::TextureQuad>* tileQuads;

  glm::vec4 color;
  float lightMultiplier;
  float emissiveLight;
	unsigned int levelID;

  glm::mat4 previousMatrix;
  bool needUpdate;
  noxed::tileset::TileSet *set;
  glm::vec2 renderedTileSize;
  int prevDataSize;

  noxed::tileset::TileGrid::Block *block;
};

}}
