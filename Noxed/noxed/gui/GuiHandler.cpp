/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "GuiHandler.h"
#include <iostream>
#include <cmath>
#include <cstring>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/IContext.h>
#include <noxed/gui/ElementType.h>
#include <noxed/events/GuiLayoutNotification.h>
#include <noxed/events/GuiValueChange.h>


namespace noxed {
  namespace gui {

    GuiHandler::GuiHandler(nox::logic::IContext *logic) :
      listener("DearImguiHandler"),
      showGui(false),
      broadcaster(nullptr)
    {
      this->broadcaster = logic->getEventBroadcaster();
      this->listener.addEventTypeToListenFor(noxed::events::GuiLayoutNotification::ID);
      this->listener.setup(this, this->broadcaster, nox::logic::event::ListenerManager::StartListening_t());
    }

    void GuiHandler::updateDearImgui()
    {
      if (showGui)
      {
        char buffer[128];
        std::vector<const char *> tempListItems;

        ImGui::Begin("Entities");

        for (auto& mapit : this->currentElements)
        {
          // Skip empty elements!
          if(mapit.second->layouts.size() == 0)
          {
            continue;
          }

          bool foundHeader = false;
          for (auto it : mapit.second->layouts)
          {
            if(it->type == noxed::gui::ElementType::LAYOUT_HEADER)
            {
              foundHeader = true;
              ImGui::Text(it->label.c_str());
            }
          }

          if(!foundHeader)
          {
            ImGui::Text(mapit.second->elementName.c_str());
          }

          for (auto it : mapit.second->layouts)
          {
            if(!it->visible)
            {
              continue;
            }

            switch (it->type)
            {

            case noxed::gui::ElementType::NUMBER_FLOAT:
            {
              float tempFloat = it->value.asFloat();
              if (ImGui::SliderFloat(it->label.c_str(), &tempFloat, it->min.asFloat(), it->max.asFloat()))
              {
                it->value = tempFloat;
                informActor(mapit.second->actor, mapit.second->elementID, it->key, it->value);
              }
              break;
            }

            case noxed::gui::ElementType::NUMBER_INT:
            {
              int tempInt = it->value.asInt();
              if (ImGui::SliderInt(it->label.c_str(), &tempInt, it->min.asInt(), it->max.asInt()))
              {
                it->value = tempInt;
                informActor(mapit.second->actor, mapit.second->elementID, it->key, it->value);
              }
              break;
            }

            case noxed::gui::ElementType::OUTPUT_STRING:
            {
              std::string str = it->value.asString();

              strcpy(buffer, str.c_str());

              ImGui::InputText(it->label.c_str(), buffer, str.size(), ImGuiInputTextFlags_ReadOnly);

              break;
            }

            case noxed::gui::ElementType::STRING:
            {
              std::string str = it->value.asString();
              strcpy(buffer, str.c_str());

              if (ImGui::InputText(it->label.c_str(), buffer, 128))
              {
                it->value = buffer;
                informActor(mapit.second->actor, mapit.second->elementID, it->key, it->value);
              }

              break;
            }

            case noxed::gui::ElementType::BOOLEAN:
            {
              bool tempBool = it->value.asBool();
              if (ImGui::Checkbox(it->label.c_str(), &tempBool))
              {
                it->value = tempBool;
                informActor(mapit.second->actor, mapit.second->elementID, it->key, it->value);
              }
              break;
            }

            case noxed::gui::ElementType::LIST_STRING:
            {
              int tempIndex = it->value.asInt();

              tempListItems.clear();
              for(const auto& str : it->listItems)
              {
                tempListItems.push_back(str.c_str());
              }

              if(tempListItems.size() == 0)
              {
                tempListItems.push_back("(none)");
              }

              if (ImGui::ListBox(it->label.c_str(), &tempIndex, &(*tempListItems.begin()), tempListItems.size(), std::max(4, static_cast<int>(it->listItems.size()))))
              {
                if(it->listItems.size() != 0)
                {
                  it->value = tempIndex;
                  informActor(mapit.second->actor, mapit.second->elementID, it->key, it->value);
                }
                else
                {
                  it->value = -1;
                }
              }
              break;
            }

            case noxed::gui::ElementType::BUTTON:
            {
              if (ImGui::Button(it->label.c_str()))
              {
                informActor(mapit.second->actor, mapit.second->elementID, it->key, it->value);
              }
              break;
            }

            case noxed::gui::ElementType::VEC2_FLOAT:
            {
              float tempValues[4];
              tempValues[0] = it->value.get("x", 0.0f).asFloat();
              tempValues[1] = it->value.get("y", 0.0f).asFloat();

              if (ImGui::InputFloat2(it->label.c_str(), tempValues))
              {
                it->value["x"] = tempValues[0];
                it->value["y"] = tempValues[1];

                informActor(mapit.second->actor, mapit.second->elementID, it->key, it->value);
              }
              break;
            }

            }

          }

          ImGui::Separator();
        }
        ImGui::End();
      }
    }


    void GuiHandler::informActor(nox::logic::actor::Actor *actor, const std::string elementid, const std::string key, Json::Value value)
    {
      auto guiEvent  = std::make_shared<noxed::events::GuiValueChange>(actor, elementid, key, value);
      this->broadcaster->queueEvent(guiEvent);
    }

    void GuiHandler::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
    {
      if (event->isType("noxed.gui.layout"))
      {
        auto action = std::static_pointer_cast<noxed::events::GuiLayoutNotification>(event);

        auto info = std::make_unique<ElementGroup>();

        info->actor = action->getActor();
        if (info->actor != nullptr) {
          info->elementName = info->actor->getName();
        }
        else
        {
          info->elementName = "NO ACTOR POINTER FOUND";
        }
        info->elementID = action->getElementId();
        info->layouts = action->getLayouts();
        this->currentElements[action->getElementId()] = std::move(info);

        this->showGui = true;
      }
    }
  }
}
