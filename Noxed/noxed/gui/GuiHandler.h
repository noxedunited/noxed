/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/event/IBroadcaster.h>

#include <json/value.h>
#include <imgui/imgui.h>

#include <noxed/gui/ElementLayout.h>

#include <vector>



namespace noxed 
{
  namespace gui 
  {
    //!  The GUI Handler
    /*!
    This class controls how the GUI appears.
    */
    class GuiHandler final : public nox::logic::event::IListener
    {
    public:
      /*!
      * Constructor that sets up the listener and broadcaster
      * for nox-events. Also sets up the color scheme for the GUI.
      * @param logic The programs logic context.
      */
      GuiHandler(nox::logic::IContext *logic);

      /*!
      *Iterates through the saved elements that are stored 
      *in currentElements.
      */
      void updateDearImgui();

      /*!
      * Stores elements sent in currentElements. 
      * @param event The event that is received on the listener.
      */
      virtual void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;


    private:

      /**
      * A structure describing the layout of GUI element(s).
      */
      struct ElementGroup {
          nox::logic::actor::Actor *actor;  /*! Actor pointer if the element belongs to an actor. */
          std::string elementName;          /*! Name in case the actor is a nullptr. */
          std::string elementID;            /*! Unique ID for each element*/
          std::vector <std::shared_ptr<gui::ElementLayout>> layouts;  /*! all of the layouts for the element*/
      };

      std::map<std::string, std::unique_ptr<ElementGroup>> currentElements;   /*! all of the entities that are currently saved for displaying, we allow one of each type at a time.*/
      nox::logic::event::ListenerManager listener;    /*! The listener that listens for events*/
      nox::logic::event::IBroadcaster *broadcaster;
      bool showGui;   /*! If this is true the GUI will be shown.*/

      /*!
      * Informs an actor that its values has been changed via the GUI
      * @param actor actor to be informed.
      * @param elementid unique identifier for each element
      * @param key name of the variable that is changed.
      * @param key the new value of the variable
      */
      void informActor(nox::logic::actor::Actor *actor, const std::string elementid, const std::string key, Json::Value value);
    };
  }
}