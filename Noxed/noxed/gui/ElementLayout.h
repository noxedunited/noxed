/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <json/value.h>

#include <noxed/gui/ElementType.h>

#include <string>

namespace noxed { namespace gui {

/**
 * A structure describing the layout of a single GUI element.
 */
struct ElementLayout
{
  const std::string key;
  const std::string label;
  const ElementType type;
  Json::Value min;
  Json::Value max;
  Json::Value value;
  std::vector<std::string> listItems;
  bool visible;

  /**
   * Construct an input layout with key, label and type. This overload is only
   * for text, separators and other value-less bits of GUI layout.
   *
   * @param key Key to associate with it. Used for GuiValueChange events
   * @param label Text-label to use for this GUI element
   * @param type Type of this bit of GUI layout
   */
  ElementLayout(const std::string& key, const std::string& label, const ElementType& type);

  /**
   * Construct an input layout with key, label and type.
   *
   * @param key Key to associate with it. Used for GuiValueChange events
   * @param label Text-label to use for this GUI element
   * @param type Type of this bit of GUI layout
   * @param value Current value of the GUI element
   */
  ElementLayout(const std::string& key, const std::string& label, const ElementType& type, const Json::Value& value);

  /**
   * Construct an input layout with key, label and type.
   *
   * @param key Key to associate with it. Used for GuiValueChange events
   * @param label Text-label to use for this GUI element
   * @param type Type of this bit of GUI layout
   * @param value Current bound of the GUI element value
   * @param min Lower bound of the GUI element value
   */
  ElementLayout(const std::string& key, const std::string& label, const ElementType& type, const Json::Value& value, const Json::Value& min, const Json::Value& max);
};

}}
