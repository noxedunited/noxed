/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "DearImguiSubRenderer.h"
#include <SDL_syswm.h>

namespace noxed {
  namespace gui {
    //Initialize the DearImgui static data.
    double       DearImguiSubRenderer::time = 0.0f;
    bool         DearImguiSubRenderer::mousePressed[3] = { false, false, false };
    float        DearImguiSubRenderer::mouseWheel = 0.0f;
    GLuint       DearImguiSubRenderer::fontTexture = 0;
    int          DearImguiSubRenderer::shaderHandle = 0;
    int          DearImguiSubRenderer::vertHandle = 0;
    int          DearImguiSubRenderer::fragHandle = 0;
    int          DearImguiSubRenderer::attribLocationTex = 0;
    int          DearImguiSubRenderer::attribLocationProjMtx = 0;
    int          DearImguiSubRenderer::attribLocationPosition = 0;
    int          DearImguiSubRenderer::attribLocationUV = 0;
    int          DearImguiSubRenderer::attribLocationColor = 0;
    unsigned int DearImguiSubRenderer::vboHandle = 0;
    unsigned int DearImguiSubRenderer::vaoHandle = 0;
    unsigned int DearImguiSubRenderer::elementsHandle = 0;

    DearImguiSubRenderer::DearImguiSubRenderer(SDL_Window * editorwindow, nox::logic::IContext *logic) :
      handler(nullptr),
      logicContext(logic)
    {
      this->window = editorwindow;
    }

    bool DearImguiSubRenderer::init(nox::app::IContext* context, nox::app::graphics::RenderData& renderData, const glm::uvec2& screenSize)
    {
      this->handler = std::make_unique<GuiHandler>(logicContext);
      this->startDearImgui();

      return true;
    }

    void DearImguiSubRenderer::prepareRendering(nox::app::graphics::RenderData& renderData)
    {
    }

    void DearImguiSubRenderer::render(nox::app::graphics::RenderData& renderData)
    {
      this->DearImguiNewFrame();
      this->handler->updateDearImgui();
      glViewport(0, 0, (int)ImGui::GetIO().DisplaySize.x, (int)ImGui::GetIO().DisplaySize.y);
      ImGui::Render();
    }
    void DearImguiSubRenderer::onScreenSizeChange(const glm::uvec2 & screenSize)
    {
    }

    void DearImguiSubRenderer::startDearImgui()
    {
      ImGuiIO& io = ImGui::GetIO();
      io.KeyMap[ImGuiKey_Tab] = SDLK_TAB;                     // Keyboard mapping. ImGui will use those indices to peek into the io.KeyDown[] array.
      io.KeyMap[ImGuiKey_LeftArrow] = SDL_SCANCODE_LEFT;
      io.KeyMap[ImGuiKey_RightArrow] = SDL_SCANCODE_RIGHT;
      io.KeyMap[ImGuiKey_UpArrow] = SDL_SCANCODE_UP;
      io.KeyMap[ImGuiKey_DownArrow] = SDL_SCANCODE_DOWN;
      io.KeyMap[ImGuiKey_PageUp] = SDL_SCANCODE_PAGEUP;
      io.KeyMap[ImGuiKey_PageDown] = SDL_SCANCODE_PAGEDOWN;
      io.KeyMap[ImGuiKey_Home] = SDL_SCANCODE_HOME;
      io.KeyMap[ImGuiKey_End] = SDL_SCANCODE_END;
      io.KeyMap[ImGuiKey_Delete] = SDLK_DELETE;
      io.KeyMap[ImGuiKey_Backspace] = SDLK_BACKSPACE;
      io.KeyMap[ImGuiKey_Enter] = SDLK_RETURN;
      io.KeyMap[ImGuiKey_Escape] = SDLK_ESCAPE;
      io.KeyMap[ImGuiKey_A] = SDLK_a;
      io.KeyMap[ImGuiKey_C] = SDLK_c;
      io.KeyMap[ImGuiKey_V] = SDLK_v;
      io.KeyMap[ImGuiKey_X] = SDLK_x;
      io.KeyMap[ImGuiKey_Y] = SDLK_y;
      io.KeyMap[ImGuiKey_Z] = SDLK_z;

      io.RenderDrawListsFn = dearImguiRenderDrawLists;
      io.SetClipboardTextFn = dearImguiSetClipboardText;
      io.GetClipboardTextFn = dearImguiGetClipboardText;

#ifdef _WIN32

      SDL_SysWMinfo wmInfo;
      SDL_VERSION(&wmInfo.version);
      SDL_GetWindowWMInfo(this->window, &wmInfo);
      io.ImeWindowHandle = wmInfo.info.win.window;
#endif

    }


    void DearImguiSubRenderer::DearImguiCreateFontsTexture()
    {
      // Build texture atlas
      ImGuiIO& io = ImGui::GetIO();
      unsigned char* pixels;
      int width, height;
      io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);   // Load as RGBA 32-bits for OpenGL3 demo because it is more likely to be compatible with user's existing shader.

                                                                // Upload texture to graphics system
      GLint last_texture;
      glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
      glGenTextures(1, &fontTexture);
      glBindTexture(GL_TEXTURE_2D, fontTexture);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

      // Store our identifier
      io.Fonts->TexID = (void *)(intptr_t)fontTexture;

      // Restore state
      glBindTexture(GL_TEXTURE_2D, last_texture);
    }

    bool DearImguiSubRenderer::DearImguiCreateDeviceObjects()
    {
      // Backup GL state
      GLint last_texture, last_array_buffer, last_vertex_array;
      glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
      glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &last_array_buffer);
      glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &last_vertex_array);

      const GLchar *vertex_shader =
        "#version 330\n"
        "uniform mat4 ProjMtx;\n"
        "in vec2 Position;\n"
        "in vec2 UV;\n"
        "in vec4 Color;\n"
        "out vec2 Frag_UV;\n"
        "out vec4 Frag_Color;\n"
        "void main()\n"
        "{\n"
        "	Frag_UV = UV;\n"
        "	Frag_Color = Color;\n"
        "	gl_Position = ProjMtx * vec4(Position.xy,0,1);\n"
        "}\n";

      const GLchar* fragment_shader =
        "#version 330\n"
        "uniform sampler2D Texture;\n"
        "in vec2 Frag_UV;\n"
        "in vec4 Frag_Color;\n"
        "out vec4 Out_Color;\n"
        "void main()\n"
        "{\n"
        "	Out_Color = Frag_Color * texture( Texture, Frag_UV.st);\n"
        "}\n";

      shaderHandle = glCreateProgram();
      vertHandle = glCreateShader(GL_VERTEX_SHADER);
      fragHandle = glCreateShader(GL_FRAGMENT_SHADER);
      glShaderSource(vertHandle, 1, &vertex_shader, 0);
      glShaderSource(fragHandle, 1, &fragment_shader, 0);
      glCompileShader(vertHandle);
      glCompileShader(fragHandle);
      glAttachShader(shaderHandle, vertHandle);
      glAttachShader(shaderHandle, fragHandle);
      glLinkProgram(shaderHandle);

      attribLocationTex = glGetUniformLocation(shaderHandle, "Texture");
      attribLocationProjMtx = glGetUniformLocation(shaderHandle, "ProjMtx");
      attribLocationPosition = glGetAttribLocation(shaderHandle, "Position");
      attribLocationUV = glGetAttribLocation(shaderHandle, "UV");
      attribLocationColor = glGetAttribLocation(shaderHandle, "Color");

      glGenBuffers(1, &vboHandle);
      glGenBuffers(1, &elementsHandle);

      glGenVertexArrays(1, &vaoHandle);
      glBindVertexArray(vaoHandle);
      glBindBuffer(GL_ARRAY_BUFFER, vboHandle);
      glEnableVertexAttribArray(attribLocationPosition);
      glEnableVertexAttribArray(attribLocationUV);
      glEnableVertexAttribArray(attribLocationColor);

#define OFFSETOF(TYPE, ELEMENT) ((size_t)&(((TYPE *)0)->ELEMENT))
      glVertexAttribPointer(attribLocationPosition, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, pos));
      glVertexAttribPointer(attribLocationUV, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, uv));
      glVertexAttribPointer(attribLocationColor, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, col));
#undef OFFSETOF

      DearImguiCreateFontsTexture();
      // Restore modified GL state
      glBindTexture(GL_TEXTURE_2D, last_texture);
      glBindBuffer(GL_ARRAY_BUFFER, last_array_buffer);
      glBindVertexArray(last_vertex_array);

      return true;
    }

    void DearImguiSubRenderer::DearImguiInvalidateDeviceObjects()
    {
      if (vaoHandle) glDeleteVertexArrays(1, &vaoHandle);
      if (vboHandle) glDeleteBuffers(1, &vboHandle);
      if (elementsHandle) glDeleteBuffers(1, &elementsHandle);
      vaoHandle = vboHandle = elementsHandle = 0;

      glDetachShader(shaderHandle, vertHandle);
      glDeleteShader(vertHandle);
      vertHandle = 0;

      glDetachShader(shaderHandle, fragHandle);
      glDeleteShader(fragHandle);
      fragHandle = 0;

      glDeleteProgram(shaderHandle);
      shaderHandle = 0;

      if (fontTexture)
      {
        glDeleteTextures(1, &fontTexture);
        ImGui::GetIO().Fonts->TexID = 0;
        fontTexture = 0;
      }

    }

    void DearImguiSubRenderer::DearImguiShutdown()
    {
      DearImguiInvalidateDeviceObjects();
      ImGui::Shutdown();
    }

    void DearImguiSubRenderer::DearImguiNewFrame()
    {
      if (!fontTexture)
        DearImguiCreateDeviceObjects();

      ImGuiIO& io = ImGui::GetIO();

      // Setup display size (every frame to accommodate for window resizing)
      int w, h;
      SDL_GetWindowSize(this->window, &w, &h);
      io.DisplaySize = ImVec2((float)w, (float)h);
      io.DisplayFramebufferScale = ImVec2(1.0f, 1.0f);

      // Setup time step
      Uint32	ticks = SDL_GetTicks();
      double current_time = ticks / 1000.0;
      io.DeltaTime = time > 0.0 ? (float)(current_time - time) : (float)(1.0f / 60.0f);
      time = current_time;

      // Setup inputs
      // (we already got mouse wheel, keyboard keys & characters from SDL_PollEvent())
      int mx, my;
      Uint32 mouseMask = SDL_GetMouseState(&mx, &my);
      if (SDL_GetWindowFlags(this->window) & SDL_WINDOW_MOUSE_FOCUS)
        io.MousePos = ImVec2((float)mx, (float)my);   // Mouse position, in pixels (set to -1,-1 if no mouse / on another screen, etc.)
      else
        io.MousePos = ImVec2(-1, -1);

      io.MouseDown[0] = mousePressed[0] || (mouseMask & SDL_BUTTON(SDL_BUTTON_LEFT)) != 0;		// If a mouse press event came, always pass it as "mouse held this frame", so we don't miss click-release events that are shorter than 1 frame.
      io.MouseDown[1] = mousePressed[1] || (mouseMask & SDL_BUTTON(SDL_BUTTON_RIGHT)) != 0;
      io.MouseDown[2] = mousePressed[2] || (mouseMask & SDL_BUTTON(SDL_BUTTON_MIDDLE)) != 0;
      mousePressed[0] = mousePressed[1] = mousePressed[2] = false;

      io.MouseWheel = mouseWheel;
      mouseWheel = 0.0f;

      // Hide OS mouse cursor if ImGui is drawing it
      SDL_ShowCursor(io.MouseDrawCursor ? 0 : 1);

      // Start the frame
      ImGui::NewFrame();
    }

    void DearImguiSubRenderer::dearImguiSetClipboardText(const char* text)
    {
      SDL_SetClipboardText(text);
    }

    const char* DearImguiSubRenderer::dearImguiGetClipboardText()
    {
      return SDL_GetClipboardText();
    }

    void DearImguiSubRenderer::dearImguiRenderDrawLists(ImDrawData* draw_data)
    {
      // Backup GL state
      GLint last_program; glGetIntegerv(GL_CURRENT_PROGRAM, &last_program);
      GLint last_texture; glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
      GLint last_array_buffer; glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &last_array_buffer);
      GLint last_element_array_buffer; glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &last_element_array_buffer);
      GLint last_vertex_array; glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &last_vertex_array);
      GLint last_blend_src; glGetIntegerv(GL_BLEND_SRC, &last_blend_src);
      GLint last_blend_dst; glGetIntegerv(GL_BLEND_DST, &last_blend_dst);
      GLint last_blend_equation_rgb; glGetIntegerv(GL_BLEND_EQUATION_RGB, &last_blend_equation_rgb);
      GLint last_blend_equation_alpha; glGetIntegerv(GL_BLEND_EQUATION_ALPHA, &last_blend_equation_alpha);
      GLint last_viewport[4]; glGetIntegerv(GL_VIEWPORT, last_viewport);
      GLboolean last_enable_blend = glIsEnabled(GL_BLEND);
      GLboolean last_enable_cull_face = glIsEnabled(GL_CULL_FACE);
      GLboolean last_enable_depth_test = glIsEnabled(GL_DEPTH_TEST);
      GLboolean last_enable_scissor_test = glIsEnabled(GL_SCISSOR_TEST);

      // Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled
      glEnable(GL_BLEND);
      glBlendEquation(GL_FUNC_ADD);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      glDisable(GL_CULL_FACE);
      glDisable(GL_DEPTH_TEST);
      glEnable(GL_SCISSOR_TEST);
      glActiveTexture(GL_TEXTURE0);

      // Handle cases of screen coordinates != from framebuffer coordinates (e.g. retina displays)
      ImGuiIO& io = ImGui::GetIO();
      int fb_width = (int)(io.DisplaySize.x * io.DisplayFramebufferScale.x);
      int fb_height = (int)(io.DisplaySize.y * io.DisplayFramebufferScale.y);
      if (fb_width == 0 || fb_height == 0)
        return;
      draw_data->ScaleClipRects(io.DisplayFramebufferScale);

      // Setup orthographic projection matrix
      glViewport(0, 0, (GLsizei)fb_width, (GLsizei)fb_height);
      const float ortho_projection[4][4] =
      {
        { 2.0f / io.DisplaySize.x, 0.0f,                   0.0f, 0.0f },
        { 0.0f,                  2.0f / -io.DisplaySize.y, 0.0f, 0.0f },
        { 0.0f,                  0.0f,                  -1.0f, 0.0f },
        { -1.0f,                  1.0f,                   0.0f, 1.0f },
      };
      glUseProgram(shaderHandle);
      glUniform1i(attribLocationTex, 0);
      glUniformMatrix4fv(attribLocationProjMtx, 1, GL_FALSE, &ortho_projection[0][0]);
      glBindVertexArray(vaoHandle);

      for (int n = 0; n < draw_data->CmdListsCount; n++)
      {
        const ImDrawList* cmd_list = draw_data->CmdLists[n];
        const ImDrawIdx* idx_buffer_offset = 0;

        glBindBuffer(GL_ARRAY_BUFFER, vboHandle);
        glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)cmd_list->VtxBuffer.size() * sizeof(ImDrawVert), (GLvoid*)&cmd_list->VtxBuffer.front(), GL_STREAM_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementsHandle);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizeiptr)cmd_list->IdxBuffer.size() * sizeof(ImDrawIdx), (GLvoid*)&cmd_list->IdxBuffer.front(), GL_STREAM_DRAW);

        for (const ImDrawCmd* pcmd = cmd_list->CmdBuffer.begin(); pcmd != cmd_list->CmdBuffer.end(); pcmd++)
        {
          if (pcmd->UserCallback)
          {
            pcmd->UserCallback(cmd_list, pcmd);
          }
          else
          {
            glBindTexture(GL_TEXTURE_2D, (GLuint)(intptr_t)pcmd->TextureId);
            glScissor((int)pcmd->ClipRect.x, (int)(fb_height - pcmd->ClipRect.w), (int)(pcmd->ClipRect.z - pcmd->ClipRect.x), (int)(pcmd->ClipRect.w - pcmd->ClipRect.y));
            glDrawElements(GL_TRIANGLES, (GLsizei)pcmd->ElemCount, sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT, idx_buffer_offset);
          }
          idx_buffer_offset += pcmd->ElemCount;
        }
      }

      // Restore modified GL state
      glUseProgram(last_program);
      glBindTexture(GL_TEXTURE_2D, last_texture);
      glBindVertexArray(last_vertex_array);
      glBindBuffer(GL_ARRAY_BUFFER, last_array_buffer);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, last_element_array_buffer);
      glBlendEquationSeparate(last_blend_equation_rgb, last_blend_equation_alpha);
      glBlendFunc(last_blend_src, last_blend_dst);
      if (last_enable_blend) glEnable(GL_BLEND); else glDisable(GL_BLEND);
      if (last_enable_cull_face) glEnable(GL_CULL_FACE); else glDisable(GL_CULL_FACE);
      if (last_enable_depth_test) glEnable(GL_DEPTH_TEST); else glDisable(GL_DEPTH_TEST);
      if (last_enable_scissor_test) glEnable(GL_SCISSOR_TEST); else glDisable(GL_SCISSOR_TEST);
      glViewport(last_viewport[0], last_viewport[1], (GLsizei)last_viewport[2], (GLsizei)last_viewport[3]);
    }
  }
}
