/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <nox/app/graphics/SubRenderer.h>
#include <nox/logic/IContext.h>

#include <noxed/gui/GuiHandler.h>

#include <SDL.h>
#include <imgui/imgui.h>
#include <glew/glew.h>


  class RenderData;


namespace noxed {
  namespace gui {
    //!  Subrenderer for the GUI. 
    /*!
    This class handles all of the graphics of the User Interface.
    This functionality is placed in the subrenderer to get the ImGui::Render(); call
    in the correct place.
    */
    class DearImguiSubRenderer final : public nox::app::graphics::SubRenderer
    {
    public:
      /*!
      * Constructor that saves the logicContext for the GuIHandler
      * and the SDL_Window for the GUI-rendering.
      * @param editorwindow The SDL_Window for the program.
      * @param logic  The logic context of the program
      */
      DearImguiSubRenderer(SDL_Window* editorwindow, nox::logic::IContext *logic);

      /*!
      *Sets up the GuiHandler and starts the DearImgui library for the GUI
      *The parameters are not currently in use.
      */
      bool init(nox::app::IContext* context, nox::app::graphics::RenderData& renderData, const glm::uvec2& screenSize) override;

      /*!
      *Overridden function from the nox Subrenderer class. Not currently in use
      */
      void prepareRendering(nox::app::graphics::RenderData& renderData) override;

      /*!
      *Starts a new GUI-frame and tells the Handler to update it.
      *The parameter is not currently in use.
      */
      void render(nox::app::graphics::RenderData& renderData) override;

      /*!
      *Overridden function from the nox Subrenderer class. Not currently in use
      */
      void onScreenSizeChange(const glm::uvec2& screenSize) override;

    private:
      
      std::unique_ptr<GuiHandler> handler;  /*! The GUI Handler */

      nox::logic::IContext *logicContext;   /*! Logic context used by the GUI handler to send and receive Ilistener events */

      SDL_Window *window;                   /*! Window pointer so the DearImgui library can render to the screen. */
      
      //remember to enable DISTRIBUTE_GROUP_DOC in the doxygen config.
      /** @name DearImgui Variables and functions.
      *   Functions and variables copied from
      *   https://github.com/ocornut/imgui/tree/master/examples/sdl_opengl3_example
      *   that handles rendering of the GUI to the graphics pipeline.
      */
      ///@{
      static double time;
      static bool   mousePressed[3];
      static float  mouseWheel;
      static GLuint fontTexture;
      static int    shaderHandle;
      static int    vertHandle;
      static int    fragHandle;
      static int    attribLocationTex;
      static int    attribLocationProjMtx;
      static int    attribLocationPosition;
      static int    attribLocationUV;
      static int    attribLocationColor;
      static unsigned int vboHandle;
      static unsigned int vaoHandle;
      static unsigned int elementsHandle;
      
      void startDearImgui();

      void DearImguiCreateFontsTexture();
      bool DearImguiCreateDeviceObjects();
      void DearImguiInvalidateDeviceObjects();
      void DearImguiShutdown();
      void DearImguiNewFrame();
      static void dearImguiRenderDrawLists(ImDrawData* draw_data);
      static void dearImguiSetClipboardText(const char* text);
      static const char* dearImguiGetClipboardText();
      ///@}
    };
  }
}