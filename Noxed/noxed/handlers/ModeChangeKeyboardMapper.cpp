/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "ModeChangeKeyboardMapper.h"

using nox::logic::actor::Actor;
using nox::app::resource::Handle;
using nox::app::resource::JsonExtraData;
using noxed::events::ModeChange;

namespace noxed { namespace handlers {

const int ModeChangeKeyboardMapper::MODE_SET = 0;
const int ModeChangeKeyboardMapper::MODE_TOGGLE = 1;
const int ModeChangeKeyboardMapper::MODE_ADD = 2;
const int ModeChangeKeyboardMapper::MODE_SUBTRACT = 3;

ModeChangeKeyboardMapper::ModeInfo::ModeInfo(const std::string mode, const int value, const int operation, const int limit):
  mode(mode),
  value(value),
  operation(operation),
  limit(limit)
{
}

ModeChangeKeyboardMapper::ModeChangeKeyboardMapper(const std::shared_ptr<Handle>& handle):
  controlledActor(nullptr),
  activeModifier(SDL_SCANCODE_UNKNOWN)
{
  // Get the json objects
  const Json::Value& root = handle->getExtraData<JsonExtraData>()->getRootValue();
  const Json::Value& buttons = root["modeControls"]["buttons"];

  // Go over the buttons
  auto buttonNames = buttons.getMemberNames();
  for(const auto& buttonName : buttonNames)
  {
    SDL_Scancode scanCode = SDL_GetScancodeFromName(buttonName.c_str());
    SDL_Scancode modifier = SDL_SCANCODE_UNKNOWN;
    const auto plusPos = buttonName.find("+");

    if(plusPos != std::string::npos)
    {
      const std::string modifierKey = buttonName.substr(0, plusPos);
      const std::string pressedKey = buttonName.substr(plusPos + 1, buttonName.size() - (plusPos + 1));

      scanCode = SDL_GetScancodeFromName(pressedKey.c_str());
      modifier = SDL_GetScancodeFromName(modifierKey.c_str());
    }
    else
    {
      scanCode = SDL_GetScancodeFromName(buttonName.c_str());
      modifier = SDL_SCANCODE_UNKNOWN;
    }

    const Json::Value& button = buttons[buttonName];

    const std::string mode = button.get("mode", "").asString();
    const int value = button.get("value", 0).asInt();
    const auto operationName = button.get("operation", "").asString();
    const int limit = button.get("limit", 0).asInt();
    int defaultValue = button.get("default", 0).asInt();

    int operation = MODE_SET;
    if(operationName == "toggle" || operationName == "!")
    {
      operation = MODE_TOGGLE;
    }
    else if(operationName == "add" || operationName == "+")
    {
      operation = MODE_ADD;

      if(defaultValue > limit)
      {
        defaultValue = limit;
      }
    }
    else if(operationName == "subtract" || operationName == "-")
    {
      operation = MODE_SUBTRACT;

      if(defaultValue < limit)
      {
        defaultValue = limit;
      }
    }
    // if nonoe, assume "=" or "set"

    // Track the state if it's not a set
    if(operation != MODE_SET && stateMap.find(mode) == stateMap.end())
    {
      stateMap.insert(std::make_pair(mode, defaultValue));
    }

    auto modeInfo = std::make_unique<ModeInfo>(mode, value, operation, limit);

    if(this->keymap.find(modifier) == this->keymap.end())
    {
      this->keymap.insert(std::make_pair(modifier, std::map<SDL_Scancode, std::unique_ptr<ModeInfo>>()));
    }

    this->keymap[modifier].insert(std::make_pair(scanCode, std::move(modeInfo)));
  }
}

std::shared_ptr<ModeChange> ModeChangeKeyboardMapper::getEvent(const SDL_Scancode& scanCode)
{
  if(this->keymap.find(scanCode) != this->keymap.end())
  {
    this->activeModifier = scanCode;
    return std::shared_ptr<ModeChange>(nullptr);
  }

  auto modIt = this->keymap.find(this->activeModifier);
  if(modIt == this->keymap.end())
  {
    return std::shared_ptr<ModeChange>(nullptr);
  }

  auto it = modIt->second.find(scanCode);
  if(it != modIt->second.end())
  {
    ModeInfo* info = it->second.get();

    // Find value
    auto it2 = this->stateMap.find(info->mode);
    int value = 0;
    if(it2 != this->stateMap.end())
    {
      value = it2->second;
    }

    switch (info->operation)
    {
    case MODE_SET:
      if(this->stateMap.find(info->mode) != this->stateMap.end())
      {
        this->stateMap[info->mode] = info->value;
      }

      return std::make_shared<ModeChange>(this->controlledActor, info->mode, info->value);

    case MODE_TOGGLE:
      value = !value;

      this->stateMap[info->mode] = value;

      return std::make_shared<ModeChange>(this->controlledActor, info->mode, value);

    case MODE_ADD:
      if(value + info->value <= info->limit)
      {
        value += info->value;
      }

      this->stateMap[info->mode] = value;

      return std::make_shared<ModeChange>(this->controlledActor, info->mode, value);

    case MODE_SUBTRACT:
      if(value - info->value >= info->limit)
      {
        value -= info->value;
      }

      this->stateMap[info->mode] = value;

      return std::make_shared<ModeChange>(this->controlledActor, info->mode, value);

    default:
      return std::shared_ptr<ModeChange>(nullptr);
    }

  }
  else
  {
    return std::shared_ptr<ModeChange>(nullptr);
  }
}

void ModeChangeKeyboardMapper::handleRelease(const SDL_Scancode& keysym)
{
  if(this->activeModifier == keysym)
  {
    this->activeModifier = SDL_SCANCODE_UNKNOWN;
  }
}

void ModeChangeKeyboardMapper::setControlledActor(nox::logic::actor::Actor *actor)
{
  this->controlledActor = actor;
}

}}
