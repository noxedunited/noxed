/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include <memory>
#include <map>
#include <string>

#include <noxed/events/ModeChange.h>

#include <nox/app/resource/Handle.h>
#include <nox/app/resource/data/JsonExtraData.h>
#include <nox/logic/actor/Actor.h>

#include <SDL.h>

namespace noxed { namespace handlers {

class ModeChangeKeyboardMapper
{
  struct ModeInfo {
    std::string mode;
    int value;
    int operation;
    int limit;

    ModeInfo(const std::string mode, const int value, const int operation, const int limit);
  };

public:
  static const int MODE_SET;
  static const int MODE_TOGGLE;
  static const int MODE_ADD;
  static const int MODE_SUBTRACT;

  /**
   * Construct the object. It needs a handle to a json file that defines
   * modeControls.buttons
   *
   * @param handle The resource handle to get the JSON data from
   */
  ModeChangeKeyboardMapper(const std::shared_ptr<nox::app::resource::Handle>& handle);

  /**
   * Look for the keybinding, and make an event object if it matches one in the
   * keybindings defined in the JSON definition.
   *
   * @param Keysym The key symbol.
   */
  std::shared_ptr<noxed::events::ModeChange> getEvent(const SDL_Scancode& keysym);

  void handleRelease(const SDL_Scancode& keysym);

  /**
   * Set the controlled actor, which will be used when constructing the event
   * object.
   *
   * @param actor New controlled actor.
   */
  void setControlledActor(nox::logic::actor::Actor *actor);

private:
  std::map<SDL_Scancode, std::map<SDL_Scancode, std::unique_ptr<ModeInfo>>> keymap;
  //std::map<SDL_Scancode, std::unique_ptr<ModeInfo>> keymap;
  std::map<std::string, int> stateMap;
  nox::logic::actor::Actor *controlledActor;
  SDL_Scancode activeModifier;
};

}}
