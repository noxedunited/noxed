/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "TileSet.h"

#include <noxed/tileset/TileSetManager.h>
#include <iostream>

using nox::app::graphics::TextureQuad;
using nox::app::graphics::TextureManager;

namespace noxed { namespace tileset {

TileSet::TileSet(const Json::Value& jsonDefinition, TileSetManager *manager)
{
  this->name = jsonDefinition.get("name", "").asString();
  this->imageName = jsonDefinition.get("image", "").asString();
  this->tileSize = parseVector(jsonDefinition["tileSize"], glm::ivec2(64, 64));
  this->tileSep = parseVector(jsonDefinition["tileSep"], glm::ivec2(0, 0));
  this->setSize = parseVector(jsonDefinition["setSize"], glm::ivec2(1, 1));

  this->setLength = this->setSize.x * this->setSize.y;

  this->tileQuads = std::make_unique<std::vector<TextureQuad>>();

  for(const auto& smartTile : jsonDefinition["smartTiles"])
  {
    auto instance = manager->createSmartTile(smartTile);

    this->smartTileNames.push_back(smartTile.get("name", "UNNAMED").asString());

    if(instance != nullptr)
    {
      this->smartTiles.push_back(std::move(instance));
    }
  }
}

const std::string TileSet::getName() const
{
  return this->name;
}

const std::string TileSet::getImageName() const
{
  return this->imageName;
}

const glm::ivec2 TileSet::getTileSize() const
{
  return this->tileSize;
}

const glm::ivec2 TileSet::getTileSep() const
{
  return this->tileSep;
}

const glm::ivec2 TileSet::getSetSize() const
{
  return this->setSize;
}

const glm::vec2 TileSet::getTileFraction() const
{
  auto fsize = static_cast<glm::vec2>(this->tileSize);
  auto fsep = static_cast<glm::vec2>(this->tileSep);
  auto fssize = static_cast<glm::vec2>(this->setSize);

  return fsize / (((fsize + fsep) * fssize) - fsep);
}

const glm::vec2 TileSet::getStrideFraction() const
{
  auto fsize = static_cast<glm::vec2>(this->tileSize);
  auto fsep = static_cast<glm::vec2>(this->tileSep);
  auto fssize = static_cast<glm::vec2>(this->setSize);

  return (fsize + fsep) / (((fsize + fsep) * fssize) - fsep);
}


const int TileSet::getFlatLength() const
{
  return this->setLength;
}

std::vector<TextureQuad>* TileSet::getQuads(const TextureManager& textureManager)
{
  if(this->tileQuads->size() > 0)
  {
    return this->tileQuads.get();
  }

  TextureQuad spriteQuad = textureManager.getTexture(this->imageName);
  const TextureQuad::RenderQuad& renderQuad = spriteQuad.getRenderQuad();
  const int setWidth = this->setSize.x;
  const auto size = static_cast<glm::vec2>(this->tileSize);
  const auto stride = this->tileSize + this->tileSep;

  // Get full tile set texture rectangle (stx1, sty1, stx2, sty2)
  const auto texTL = renderQuad.topLeft.getTextureCoordinate();
  const auto texBR = renderQuad.bottomRight.getTextureCoordinate();
  const float stx1 = texTL.x;
  const float sty1 = texTL.y;
  const float stx2 = texBR.x;
  const float sty2 = texBR.y;

  const glm::vec2 tileFraction = this->getTileFraction();
  const glm::vec2 strideFraction = this->getStrideFraction();

  const float tileWidth = (stx2 - stx1) * tileFraction.x;
  const float tileHeight = (sty2 - sty1) * tileFraction.y;
  const float strideWidth = (stx2 - stx1) * strideFraction.x;
  const float strideHeight = (sty2 - sty1) * strideFraction.y;

  for(int i = 0; i < this->setLength; ++i)
  {
    // Convert flat index to two-dimensional index
    const int xIndex = i % setWidth;
    const int yIndex = i / setWidth;

    // Make a quad, and copy the details from the original full sprite
    TextureQuad newQuad(spriteQuad.getAtlasId());
    TextureQuad::RenderQuad newRenderQuad(renderQuad);

    // Find texture coordinates.
    const float tx1 = stx1 + (xIndex * strideWidth);
    const float ty1 = sty1 + (yIndex * strideHeight);
    const float tx2 = tx1 + tileWidth;
    const float ty2 = ty1 + tileHeight;

    // Set texture coordinates on new quad
    newRenderQuad.topLeft.setTextureCoordinate(glm::vec2(tx1, ty1));
    newRenderQuad.topRight.setTextureCoordinate(glm::vec2(tx2, ty1));
    newRenderQuad.bottomLeft.setTextureCoordinate(glm::vec2(tx1, ty2));
    newRenderQuad.bottomRight.setTextureCoordinate(glm::vec2(tx2, ty2));

    // Set coordinates on new quad. This flips it upside down so the tiles
    // display right side up since they're made with 0,0 = top left, not 0,0 =
    // bottom left.
    newRenderQuad.bottomLeft.setPosition(glm::vec2(0, 0));
    newRenderQuad.bottomRight.setPosition(glm::vec2(1.0, 0));
    newRenderQuad.topLeft.setPosition(glm::vec2(0, 1.0));
    newRenderQuad.topRight.setPosition(glm::vec2(1.0, 1.0));

    // Finally, store the quad.
    newQuad.setRenderQuad(newRenderQuad);
    this->tileQuads->push_back(newQuad);
  }

  return this->tileQuads.get();
}

glm::ivec2 TileSet::parseVector(const Json::Value value, const glm::ivec2 defaultValue) const
{
  if(!value.isNull())
  {
    const auto x = value["x"];
    const auto y = value["y"];

    if(!x.isNull() && !y.isNull())
    {
      return glm::ivec2(x.asInt(), y.asInt());
    }
  }

  return defaultValue;
}

SmartTile *TileSet::getSmartTile(const int index) const
{
  assert(index >= 0 && index < this->smartTiles.size());

  return this->smartTiles[index].get();
}

const int TileSet::getSmartTileCount() const
{
  return this->smartTiles.size();
}

const std::vector<std::string>& TileSet::getSmartTileNames() const
{
  return this->smartTileNames;
}

}}
