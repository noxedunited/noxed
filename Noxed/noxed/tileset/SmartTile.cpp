/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "SmartTile.h"

namespace noxed { namespace tileset {

void SmartTile::initialize(const std::vector<int>& tiles, const Json::Value& options)
{
  if(!options.isNull())
  {
    // Insert unconditionals first
    if(options.isMember("tags"))
    {
      for(const auto& tag : options["tags"])
      {
        this->tags.insert(std::make_pair(tag.asString(), std::vector<int>(0)));
      }
    }
    else if(options.isMember("tag"))
    {
      this->tags.insert(std::make_pair(options["tag"].asString(), std::vector<int>(0)));
    }

    // Insert conditionals
    if(options.isMember("conditionalTags"))
    {
      auto conditionalTags = options["conditionalTags"];

      for(const auto tagName : conditionalTags.getMemberNames())
      {
        std::vector<int> tileIndices;

        for(const auto tileIndex : conditionalTags[tagName])
        {
          tileIndices.push_back(tileIndex.asInt());
        }

        this->tags.insert(std::make_pair(tagName, tileIndices));
      }
    }
  }
}

const bool SmartTile::hasTag(const std::string tag, const int resolvedIndex) const
{
  auto it = this->tags.find(tag);

  if(it != this->tags.end())
  {
    if(it->second.size() == 0)
    {
      return true;
    }
    else if(resolvedIndex != -1)
    {
      bool negative = false;

      for(int number : it->second)
      {
        if (number < 0)
        {
          // From now on, not finding the number should mean it's good
          negative = true;

          if(-(number + 1) == resolvedIndex)
          {
            return false;
          }
        }
        else if(number == resolvedIndex)
        {
          return true;
        }
      }

      return negative;
    }
  }

  return false;
}

const bool SmartTile::canHaveTag(const std::string tag) const
{
  return (this->tags.find(tag) != this->tags.end());
}

}}
