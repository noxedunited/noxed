/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <json/value.h>

#include <noxed/tileset/TileGrid.h>

#include <string>

namespace noxed { namespace tileset {

class TileSet;
class TileGrid;

/**
 * A smart tile type. This class is to convert a smart tile to a regular dumb
 * tile by using the arguments it is provided.
 *
 * There is one instance per smart tile in a tile set. It should not keep state
 * that is relevant to a single tile map.
 */
class SmartTile
{
public:
  using IdType = std::string;

  SmartTile() = default;

  /**
   * Initialize the smart tile with
   *
   * @param tiles A list of tile parameters
   * @param options A json object with options
   */
  virtual void initialize(const std::vector<int>& tiles, const Json::Value& options);

  /**
   * Resolve the current tile into the underlying tileset tile.
   *
   * @param grid The TileGrid where the tile is located
   * @param set The TileSet where the smart tile instance is located
   * @param x The logical tile x coordinate; xTile parameters in the grid
   * @param y The logical tile y coordinate; yTile parameters in the grid
   * @param index The index of this smart tile in the tile set.
   */
  virtual const int resolve(const TileGrid *grid, const TileSet *set, const int x, const int y, const int index) = 0;

  /**
   * Ask if the tile has a tag. This is for checking instances across instances
   * of smart tiles. For example, you may want two different wall tiles to be
   * context sensitive.
   *
   * If your tile class do not need this, return false without any other checks.
   *
   * @param tag The text string to ask it for.
   * @param resolvedIndex The index resolve() has given, or -1 to not bother
   */
  virtual const bool hasTag(const std::string tag, const int resolvedIndex) const;

  /**
   * Check if the tag is at all possible for this smart tile.
   *
   * @param tag The tag word
   * @return True if the tag is possible, were the right conditions met
   */
  virtual const bool canHaveTag(const std::string tag) const;

  /**
   * Get the type string that should be the constnat ID.
   */
  virtual const std::string getType() const = 0;

protected:
  std::map<std::string, std::vector<int>> tags;
};

}}
