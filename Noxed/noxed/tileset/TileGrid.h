/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <vector>
#include <memory>
#include <json/value.h>
#include <glm/vec2.hpp>

namespace noxed { namespace tileset {

/**
 * A data structure used to store numbers to represent tiles. It uses
 * blocks to store them, which does have overhead in a sparse tile set, but
 * an insignificant one. Worst case scenario, adding one tile will add ~1KiB
 * to the total memory usage, but it allows quicker sequential tile processing.
 *
 * There are two argument types for position: tileX/Y and blockX/Y. The former
 * measures the tile index, while the latter count the position in blockWidth-s.
 * All blocks are square, which means there is no blockHeight.
 *
 * Blocks are never deleted except for when the entire grid is, but
 * serialization does skip empty blocks.
 */
class TileGrid
{
public:
  /**
   * A block of tiles stored in the tile grid. It has a position that
   * is counted in blockWidths. These are created and destroyed by the tile
   * grid, but is available to other classes as a non constant.
   */
  struct Block
  {
    int x, y, blockWidth;
    unsigned int id;
    std::vector<int> tiles;

    Block(const int x, const int y, const int blockWidth);

    /**
     * Converts an absolute tile position to a relative index that will get you
     * the tile in the tile index.
     *
     * @param tileX Absolute x index of the tile
     * @param tileY Absolute y index of the tile
     * @returns The index of the tile positon
     */
    const int getIndex(const int tileX, const int tileY) const;
  };

  TileGrid(int blockWidth);

  /**
   * Get the tile at the position
   *
   * @param tileX x index of the tile
   * @param tileY y index of the tile
   * @returns The tile index at that position, -1 means no tile
   */
  const int getTile(const int tileX, const int tileY) const;

  /**
   * Set a tile at that value. -1 indicate that there is no tile there,
   * and that value is used to "delete" a tile.
   *
   * @param tileX x index of the tile
   * @param tileY y index of the tile
   * @param value The value to set.
   * @returns The block index of the block that was changed.
   */
  const int setTile(const int tileX, const int tileY, const int value);

  /**
  * Get the block at the specified index, but it will not make a new block if
  * none exists.
  *
  * @param blockX x index of the block
  * @param blockY y index of the block
  * @returns The block at that block index
   */
  Block *getBlock(const int blockX, const int blockY) const;

  /**
  * Get the block at the specified flat index.
  *
  * @param index The flat index of the block, ordered by creation time
  * @returns The block covering that tile index, or nullptr if none exists
   */
  Block *getBlock(const unsigned int index) const;

  /**
  * Get the block at the specified absolute tile index.
  *
  * @param blockX x index of the block
  * @param blockY y index of the block
  * @returns The block covering that tile index, or nullptr if none exists
   */
  Block *getBlockAt(const int tileX, const int tileY) const;

  /**
  * Get or creates a block at the specified block index
  *
  * @param blockX x index of the block
  * @param blockY y index of the block
  * @returns The block at that block index
  */
  Block *getRequiredBlock(const int blockX, const int blockY);

  /**
  * Get or creates a block at the specified absolute tile index
  *
  * @param tileX x index of the tile
  * @param tileY y index of the tile
  * @returns The block covering that tile index
  */
  Block *getRequiredBlockAt(const int tileX, const int tileY);

  /**
   * Get the amount of blocks that are currently managed by this grid, useful
   * for the getBlock function that uses the flat index.
   */
  const int getBlockCount() const;

  /**
   * Get the relative float vector position of the block. This do not account
   * for the position of the TileMap component.
   *
   * @param block The block to the position of
   * @return The position, 1.0 per tile
   */
  glm::vec2 getBlockPosition(const Block *block) const;

  /**
   * Get the relative float vector position for a tile by the given index in a
   * block.
   *
   * @param block The block where the tile reside
   * @param tileIndex The flat index of the tile inside the block
   * @return The position, 1.0 per tile
   */
   glm::vec2 getTilePosition(const Block *block, const int index) const;

  /**
   * Turn the tile grid into JSON data for future deserialization
   *
   * @param target The JSON object to serialize to
   */
  void serialize(Json::Value& target) const;

  /**
   * Populate this object with data from the JSON data
   *
   * @param source The JSON object to deserialize from
   */
  void deserialize(const Json::Value& source);

private:
  Block *createBlock(const int blockX, const int blockY);

  Block *lastBlock;
  int blockWidth;
  std::vector<std::unique_ptr<Block>> blocks;
  std::map<int, std::unique_ptr<std::map<int, Block*>>> map;
  unsigned int nextBlockId;
};

}}
