/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "FourBitTile.h"

#include <noxed/tileset/TileSet.h>
#include <noxed/tileset/TileGrid.h>

namespace noxed { namespace tileset { namespace tiletypes {

const SmartTile::IdType FourBitTile::ID = "4bit";

void FourBitTile::initialize(const std::vector<int>& tiles, const Json::Value& options)
{
  this->SmartTile::initialize(tiles, options);

  this->tiles = tiles;

  this->enableExtra = options.get("extraBits", false).asBool();
  this->fullEightBit = options.get("full8Bit", false).asBool();

  this->groupTag = "";
  for(auto it : this->tags)
  {
    if(it.first.find("tile-group:") == 0)
    {
      this->groupTag = it.first;
      break;
    }
  }
}

const int FourBitTile::resolve(const TileGrid *grid, const TileSet *set, const int x, const int y, const int index)
{
  unsigned int mask = 0U;

  if(this->groupTag.size() > 0)
  {
    mask |= (this->isGroupMember(grid, set, x - 1, y) == index);
    mask |= (this->isGroupMember(grid, set, x, y + 1) == index) << 1;
    mask |= (this->isGroupMember(grid, set, x + 1, y) == index) << 2;
    mask |= (this->isGroupMember(grid, set, x, y - 1) == index) << 3;

    if(this->enableExtra && mask == 15)
    {
      mask |= (this->isGroupMember(grid, set, x - 1, y + 1) == index) << 4;
      mask |= (this->isGroupMember(grid, set, x + 1, y + 1) == index) << 5;
      mask |= (this->isGroupMember(grid, set, x + 1, y - 1) == index) << 6;
      mask |= (this->isGroupMember(grid, set, x - 1, y - 1) == index) << 7;
    }
  }
  else
  {
    mask |= (grid->getTile(x - 1, y) == index);
    mask |= (grid->getTile(x, y + 1) == index) << 1;
    mask |= (grid->getTile(x + 1, y) == index) << 2;
    mask |= (grid->getTile(x, y - 1) == index) << 3;

    if(this->enableExtra && mask == 15)
    {
      mask |= (grid->getTile(x - 1, y + 1) == index) << 4;
      mask |= (grid->getTile(x + 1, y + 1) == index) << 5;
      mask |= (grid->getTile(x + 1, y - 1) == index) << 6;
      mask |= (grid->getTile(x - 1, y - 1) == index) << 7;
    }
  }

  if(mask > 15 && !this->fullEightBit)
  {
    switch(mask)
    {
      case 239: mask = 16; break;
      case 223: mask = 17; break;
      case 191: mask = 18; break;
      case 127: mask = 19; break;
      default: mask = 15; break;
    }
  }

  if(mask < this->tiles.size()) // Correct tile if available
  {
    return this->tiles[mask];
  }
  else if(this->tiles.size() > 15) // Middle tile if available
  {
    return this->tiles[15];
  }
  else // Give up and return the empty
  {
    return 0;
  }
}

const std::string FourBitTile::getType() const
{
  return ID;
}

const bool FourBitTile::isGroupMember(const TileGrid *grid, const TileSet *set, const int x, const int y) const
{
  const int index = grid->getTile(x, y);

  if(index < 0 && index > set->getSmartTileCount())
  {
    return false;
  }
  else
  {
    return set->getSmartTile(index)->hasTag(this->groupTag, -1);
  }
}

}}}
