/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "BigTile.h"

#include <noxed/tileset/TileSet.h>
#include <noxed/tileset/TileGrid.h>

#include <iostream>

namespace noxed { namespace tileset { namespace tiletypes {

const SmartTile::IdType BigTile::ID = "bigtile";

void BigTile::initialize(const std::vector<int>& tiles, const Json::Value& options)
{
  this->SmartTile::initialize(tiles, options);

  this->tiles = tiles;

  if(!options.isNull())
  {
    this->width = options.get("width", 4).asInt();
    this->height = options.get("height", 4).asInt();
  }
  else
  {
    this->width = this->height = 4;
  }
}

const int BigTile::resolve(const TileGrid *grid, const TileSet *set, const int x, const int y, const int index)
{
  int xIndex = (x % this->width);
  int yIndex = (y % this->height);

  if(xIndex < 0)
  {
    xIndex = (this->width) + xIndex;
  }

  if(yIndex < 0)
  {
    yIndex = (this->height) + yIndex;
  }

  int flatIndex = (yIndex * this->width) + xIndex;
  return this->tiles[flatIndex % this->tiles.size()];
}

const std::string BigTile::getType() const
{
  return ID;
}

}}}
