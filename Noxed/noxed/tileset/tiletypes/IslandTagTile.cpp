/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "IslandTagTile.h"

#include <noxed/tileset/TileSet.h>
#include <noxed/tileset/TileGrid.h>

namespace noxed { namespace tileset { namespace tiletypes {

const SmartTile::IdType IslandTagTile::ID = "island_tag";

void IslandTagTile::initialize(const std::vector<int>& tiles, const Json::Value& options)
{
  this->SmartTile::initialize(tiles, options);

  if(tiles.size() > 1)
  {
    this->innerTile = tiles[0];
    this->outerTile = tiles[1];
  }
  else
  {
    this->innerTile = this->outerTile = -1;
  }

  this->groupTag = "";
  for(auto it : this->tags)
  {
    if(it.first.find("tile-group:") == 0)
    {
      this->groupTag = it.first;
      break;
    }
  }
}

const int IslandTagTile::resolve(const TileGrid *grid, const TileSet *set, const int x, const int y, const int index)
{
  const int maxIndex = set->getSmartTileCount();

  for(int i = -1; i <= 1; ++i)
  {
    for(int j = -1; j <= 1; ++j)
    {
      // On any combination of offsets aside from 0,0:
      if(j == 0 && i == 0)
      {
        continue;
      }

      const int neighborIndex = grid->getTile(x + i, y + j);

      if(neighborIndex != index)
      {
        if(neighborIndex >= 0 && neighborIndex < maxIndex)
        {
          if(!set->getSmartTile(neighborIndex)->hasTag(this->groupTag, -1))
          {
            return outerTile;
          }
        }
        else
        {
          return outerTile;
        }
      }
    }
  }

  return innerTile;
}

const std::string IslandTagTile::getType() const
{
  return ID;
}

}}}
