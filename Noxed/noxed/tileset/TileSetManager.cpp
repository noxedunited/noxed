/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "TileSetManager.h"


#include <nox/app/resource/data/JsonExtraData.h>

namespace noxed { namespace tileset {

TileSetManager::TileSetManager()
{
}

void TileSetManager::load(const Json::Value& jsonObject)
{
  auto tileset = std::make_unique<TileSet>(jsonObject, this);

  this->map.insert(std::make_pair(tileset->getName(), tileset.get()));
  this->list.push_back(std::move(tileset));
}

void TileSetManager::loadAll(const std::string& directoryPath, nox::app::resource::IResourceAccess *resourceAccess)
{
  auto descriptors = resourceAccess->getResourcesRecursivelyInDirectory(directoryPath);

  for(auto descriptor : descriptors)
  {
    auto handle = resourceAccess->getHandle(descriptor);
    const Json::Value& jsonRoot = handle->getExtraData<nox::app::resource::JsonExtraData>()->getRootValue();

    this->load(jsonRoot);
  }
}

TileSet *TileSetManager::find(const std::string& key) const
{
  auto it = this->map.find(key);

  if(it != this->map.end())
  {
    return it->second;
  }
  else
  {
    return nullptr;
  }
}

TileSet *TileSetManager::find(const unsigned int index) const
{
  if(index >= 0 && index < this->list.size())
  {
    return this->list[index].get();
  }
  else
  {
    return nullptr;
  }
}

const int TileSetManager::getCount() const
{
  return this->list.size();
}

std::unique_ptr<SmartTile> TileSetManager::createSmartTile(const Json::Value& definition) const
{
  return this->createSmartTile(definition.get("type", "static").asString(), definition);
}

std::unique_ptr<SmartTile> TileSetManager::createSmartTile(const std::string& type, const Json::Value& definition) const
{
  std::vector<int> tiles;
  for(const auto& tile : definition["tiles"])
  {
    tiles.push_back(tile.asInt());
  }

  return this->createSmartTile(type, tiles, definition["options"]);
}

std::unique_ptr<SmartTile> TileSetManager::createSmartTile(const std::string& type, const std::vector<int>& tiles, const Json::Value& options) const
{
  auto smartTile = this->smartTileFactory.createObject(type);

  if(smartTile != nullptr)
  {
    smartTile->initialize(tiles, options);
  }

  return smartTile;
}


}}
