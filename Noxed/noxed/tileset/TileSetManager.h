/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <string>
#include <vector>
#include <memory>

#include <glm/vec2.hpp>
#include <json/value.h>

#include <nox/app/resource/Descriptor.h>
#include <nox/app/resource/IResourceAccess.h>
#include <nox/util/GenericObjectFactory.h>

#include "TileSet.h"
#include "SmartTile.h"

namespace noxed { namespace tileset {

class TileSetManager
{
public:
  TileSetManager();

  /**
   * Load a tile set defintiion from the JSON object.
   *
   * @param jsonObject The source object to load from
   */
  void load(const Json::Value& jsonObject);

  /**
   * Load all tile sets in the relative directory path.
   *
   * @param directoryPath The path to laod them from
   * @param resourceAccess The resource accesser to use to find the resources
   */
  void loadAll(const std::string& directoryPath, nox::app::resource::IResourceAccess *resourceAccess);

  /**
   * Find the tile set by its name
   *
   * @param key The name of the tile set
   */
  TileSet *find(const std::string& key) const;

  /**
   * Find the tileset by its index
   *
   * @param index The index it is stored at
   */
  TileSet *find(const unsigned int index) const;

  /**
   * Get the count of tile sets, for use with finding tilesets by their
   * numerical index.
   */
  const int getCount() const;

  /**
   * Create a smart tile instance from JSON definition.
   *
   * @param definition The json object representing the smart tile.
   * @returns The unique pointer to the new smart tile.
   */
  std::unique_ptr<SmartTile> createSmartTile(const Json::Value& definition) const;

  /**
   * Create a smart tile instance from type name and JSON definition. If you
   * already have the type name in the definition, use the above overload
   * instead
   *
   * @param type The type name of the smart tile (e.g. StaticTile::ID)
   * @param definition The json object representing the smart tile.
   * @returns The unique pointer to the new smart tile.
   */
  std::unique_ptr<SmartTile> createSmartTile(const std::string& type, const Json::Value& definition) const;

  /**
   * Create a smart tile instance with the type, tiles it has to pick from, and
   * a JSON object representing any arbitrary options the tile type may need.
   * The above functions use this one in the end.
   *
   * @param type The type name of the smart tile (e.g. StaticTile::ID)
   * @param tiles The list of tiles it has to pick from.
   * @param options The json object representing the options
   * @returns The unique pointer to the new smart tile.
   */
  std::unique_ptr<SmartTile> createSmartTile(const std::string& type, const std::vector<int>& tiles, const Json::Value& options) const;

  /**
   * Regsiter the smart tile type. This has to be done *before* the tile sets
   * are loaded, lest the smart tiles will not know where to find them.
   *
   * It uses the static ID property of the tile class for the index key.
   */
  template <typename SmartTileType>
  void registerSmartTileType();

private:
  std::map<std::string, TileSet *> map;
  std::vector<std::unique_ptr<TileSet>> list;
  nox::util::GenericObjectFactory<SmartTile::IdType, SmartTile> smartTileFactory;
};

template <typename SmartTileType>
void TileSetManager::registerSmartTileType()
{
  static_assert(std::is_base_of<SmartTile, SmartTileType>::value == true, "SmartTileType is not derived from SmartTile");

  this->smartTileFactory.addObjectCreation<SmartTileType>(SmartTileType::ID);
}

}}
