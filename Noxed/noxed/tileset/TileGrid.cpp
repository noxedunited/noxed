/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "TileGrid.h"

#include <algorithm> // std::fill
#include <math.h>    // floor
#include <cassert>
#include <iostream>

namespace noxed { namespace tileset {


TileGrid::TileGrid(int blockWidth):
  blockWidth(blockWidth),
  lastBlock(nullptr),
  nextBlockId(0)
{
}

const int TileGrid::getTile(const int tileX, const int tileY) const
{
  // Get the block and position relative to it, which is needed to put the
  // value in the right place.
  auto block = this->getBlockAt(tileX, tileY);
  if(block != nullptr)
  {
    return block->tiles[block->getIndex(tileX, tileY)];
  }
  else
  {
    return -1;
  }
}

const int TileGrid::setTile(const int tileX, const int tileY, const int value)
{
  auto block = this->getRequiredBlockAt(tileX, tileY);
  block->tiles[block->getIndex(tileX, tileY)] = value;

  return block->id;
}

TileGrid::Block *TileGrid::getBlock(const int blockX, const int blockY) const
{
  // Try save some time by checking if this is a repeat lookup
  if(lastBlock != nullptr && lastBlock->x == blockX && lastBlock->y == blockY)
  {
    return lastBlock;
  }
  else
  {
    // Look it up on this->map[blockX][blockY];
    auto itX = this->map.find(blockX);

    if(itX != this->map.end())
    {
      auto mapY = itX->second.get();
      auto itY = mapY->find(blockY);

      if(itY != mapY->end())
      {
        return itY->second;
      }
    }

    return nullptr;
  }
}

TileGrid::Block *TileGrid::getBlockAt(const int tileX, const int tileY) const
{
  int blockX, blockY;

  if(tileX < 0)
  {
    blockX = ((tileX + 1) / this->blockWidth) - 1;
  }
  else
  {
    blockX = (tileX / this->blockWidth);
  }

  if(tileY < 0)
  {
    blockY = ((tileY + 1) / this->blockWidth) - 1;
  }
  else
  {
    blockY = (tileY / this->blockWidth);
  }

  return this->getBlock(blockX, blockY);
}

TileGrid::Block *TileGrid::getBlock(const unsigned int index) const
{
  return this->blocks[index].get();
}

TileGrid::Block *TileGrid::getRequiredBlock(const int blockX, const int blockY)
{
  TileGrid::Block *block = this->getBlock(blockX, blockY);

  if(block != nullptr) // If there's a hit, we're done!
  {
    return block;
  }
  else // They want the block.
  {
    return this->createBlock(blockX, blockY);
  }
}

TileGrid::Block *TileGrid::getRequiredBlockAt(const int tileX, const int tileY)
{
  int blockX, blockY;

  if(tileX < 0)
  {
    blockX = ((tileX + 1) / this->blockWidth) - 1;
  }
  else
  {
    blockX = (tileX / this->blockWidth);
  }

  if(tileY < 0)
  {
    blockY = ((tileY + 1) / this->blockWidth) - 1;
  }
  else
  {
    blockY = (tileY / this->blockWidth);
  }

  return this->getRequiredBlock(blockX, blockY);
}

void TileGrid::serialize(Json::Value& target) const
{
  Json::Value blockArray = Json::arrayValue;
  for(auto& block : this->blocks)
  {
    Json::Value tileArray = Json::arrayValue;
    for(const int& tile : block->tiles)
    {
      tileArray.append(tile);
    }

    Json::Value blockJson;
    blockJson["x"] = block->x;
    blockJson["y"] = block->y;
    blockJson["tiles"] = tileArray;

    blockArray.append(blockJson);
  }

  target["blockWidth"] = this->blockWidth;
  target["blocks"] = blockArray;
}

void TileGrid::deserialize(const Json::Value& source)
{
  // Wipe the object in case it's non-empty.
  this->blocks.clear();
  this->map.clear();
  this->lastBlock = nullptr;
  this->nextBlockId = 0;

  this->blockWidth = source.get("blockWidth", 16).asInt();

  for(const auto& blockDef : source["blocks"])
  {
    auto block = this->createBlock(blockDef.get("x", 0).asInt(), blockDef.get("y", 0).asInt());
    int index = 0;

    assert(blockDef["tiles"].size() <= block->tiles.size());

    for(const auto& tile : blockDef["tiles"])
    {
      block->tiles[index++] = tile.asInt();
    }
  }
}

const int TileGrid::getBlockCount() const
{
  return this->blocks.size();
}

glm::vec2 TileGrid::getBlockPosition(const TileGrid::Block *block) const
{
  return glm::vec2(block->x * this->blockWidth, block->y * this->blockWidth);
}

glm::vec2 TileGrid::getTilePosition(const Block *block, const int index) const
{
  int xIndex = index % this->blockWidth;
  int yIndex = index / this->blockWidth;

  return this->getBlockPosition(block) + glm::vec2(xIndex, yIndex);
}

// PRIVATE PRIVATE PRIVATE PRIVATE PRIVATE PRIVATE PRIVATE PRIVATE PRIVATE
TileGrid::Block *TileGrid::createBlock(const int blockX, const int blockY)
{
  auto newBlock = std::make_unique<TileGrid::Block>(blockX, blockY, this->blockWidth);
  auto newBlockPtr = newBlock.get();

  newBlockPtr->id = this->nextBlockId++;

  auto itX = this->map.find(blockX);
  if(itX == this->map.end())
  {
    this->map.insert(std::make_pair(blockX, std::make_unique<std::map<int, TileGrid::Block*>>()));
  }

  this->map[blockX].get()->insert(std::make_pair(blockY, newBlockPtr));
  this->blocks.push_back(std::move(newBlock));

  return newBlockPtr;
}



TileGrid::Block::Block(const int x, const int y, const int blockWidth):
  x(x),
  y(y),
  blockWidth(blockWidth)
{
  this->tiles.resize(blockWidth * blockWidth);
  std::fill(this->tiles.begin(), this->tiles.end(), -1);
}

const int TileGrid::Block::getIndex(const int tileX, const int tileY) const
{
  // Convert absolute to relative tile index
  int relativeX = tileX - (this->x * this->blockWidth);
  int relativeY = tileY - (this->y * this->blockWidth);

  // Let's get an error if things go horribly
  assert(relativeX >= 0 && relativeX < blockWidth);
  assert(relativeY >= 0 && relativeY < blockWidth);

  // Convert 2d index to flat index and return it
  return (relativeY * blockWidth) + relativeX;
}



}}
