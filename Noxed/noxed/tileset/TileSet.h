/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <string>
#include <glm/vec2.hpp>
#include <json/value.h>

#include <nox/app/graphics/TextureQuad.h>
#include <nox/app/graphics/TextureRenderer.h>
#include <nox/app/graphics/TextureManager.h>

namespace noxed { namespace tileset {

class SmartTile;
class TileSetManager;

/**
 * A tile set.
 *
 * It contains the data on how to read the image and split it up into tiles.
 *
 * It contains the smart tiles, as well as plain old regular tiles
 */
class TileSet
{
public:
  /**
   * Construct the tile set
   *
   * @param jsonDefinition The JSON definition to construct it from
   * @param manager The manager that is managing this tileset.
   */
  TileSet(const Json::Value& jsonDefinition, TileSetManager* manager);

  /**
   * Get the name of the tile set
   */
  const std::string getName() const;

  /**
   * Get the name of the source image
   */
  const std::string getImageName() const;

  /**
   * Get the size of a single tile
   */
  const glm::ivec2 getTileSize() const;

  /**
   * Get the size of the separator between the tiles
   */
  const glm::ivec2 getTileSep() const;

  /**
   * Get the width and height (in tiles) of the entire set
   */
  const glm::ivec2 getSetSize() const;

  /**
   * Get the fractions of the tile set source image a single tile occupies
   */
  const glm::vec2 getTileFraction() const;

  /**
   * Get the fraction of the tile set to multiply with for the stride. This is
   * the fraction for tile size + separator.
   */
  const glm::vec2 getStrideFraction() const;

  /**
   * Get the flat length of the tile set, which means the amount of tiles there
   * are
   */
  const int getFlatLength() const;

  /**
   * Get or creates the quads needed to render the tiles in this tile set
   *
   * @param textureManager Where to find the image
   */
  std::vector<nox::app::graphics::TextureQuad>* getQuads(const nox::app::graphics::TextureManager& textureManager);

  /**
   * Get the smart tile at the specified index. See below function for way to
   * get the upper boundary, because there is no bound check offered here.
   *
   * @param index The index of the smart tile.
   */
  SmartTile *getSmartTile(const int index) const;

  /**
   * Get the amount of smart tiles in this tile set.
   */
  const int getSmartTileCount() const;

  /**
   * Get the names of smart tiles
   */
  const std::vector<std::string>& getSmartTileNames() const;

private:
  std::string name;
  std::string imageName;
  glm::ivec2 tileSize;
  glm::ivec2 tileSep;
  glm::ivec2 setSize;
  int setLength;

  std::unique_ptr<std::vector<nox::app::graphics::TextureQuad>> tileQuads;
  std::vector<std::unique_ptr<SmartTile>> smartTiles;
  std::vector<std::string> smartTileNames;

  glm::ivec2 parseVector(const Json::Value value, const glm::ivec2 defaultValue) const;
};

}}
