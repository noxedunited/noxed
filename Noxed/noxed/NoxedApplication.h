/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Strømme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#pragma once

#include <nox/app/SdlApplication.h>
#include <nox/app/log/Logger.h>
#include <nox/util/Timer.h>
#include <nox/logic/Logic.h>

#include <glm/vec2.hpp>

#ifdef _WIN32
#include <functional>
#endif


class NoxedWindowView;
//!  NoxedApplication
/*!
Extends the SdlApplication class.
*/
class NoxedApplication : public nox::app::SdlApplication
{
public:
  NoxedApplication(const char *projectName);

  virtual bool onInit() override;
  void onUpdate(const nox::Duration& deltaTime) override;
  void onSdlEvent(const SDL_Event& event) override;

protected:
  bool initializeResourceCache();
  nox::logic::Logic* initializeLogic();
	nox::logic::physics::Simulation* initializePhysics(nox::logic::Logic* logic);
	nox::logic::world::Manager* initializeWorldManager(nox::logic::Logic* logic);
  virtual void initializeWindow(nox::logic::Logic* logic);
  bool loadWorldFile(nox::logic::IContext* logicContext, nox::logic::world::Manager* worldManager);

  nox::app::log::Logger log;
  nox::util::Timer<nox::Duration> outputTimer;
  nox::logic::Logic* getLogicPointer();

  NoxedWindowView *window;
  glm::vec2 lastCameraPosition;

  std::string initialWorldName;
  std::vector<std::function<void(nox::logic::world::Manager*)>> worldLoadCallbacks;

private:
  nox::logic::Logic *logicPointer;
};
