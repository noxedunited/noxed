/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "ExposedComponent.h"
#include "ExposedArgument.h"
#include <nox/logic/actor/Actor.h>

using nox::logic::actor::Actor;

namespace noxed { namespace expose {

ExposedComponent::ExposedComponent()
{
}

ExposedComponent::ExposedComponent(const std::string& name, const Json::Value& object):
  ExposedComponent()
{
  this->initialize(name, object);
}

ExposedComponent::~ExposedComponent()
{
}

void ExposedComponent::initialize(const std::string& name, const Json::Value& object)
{
  // Copy the data off the parameters and object.
  this->name = name;
  this->json = object;
  this->useEditorComponent = object.get("useEditorComponent", false).asBool();

  // In case it's a re-initialize, clear the list.
  children.clear();

  // Get the arguments and proceed if it's non-empty
  const auto& args = this->json["args"];
  if(!args.isNull())
  {
    // Get each key under args
    for(const auto& key : args.getMemberNames())
    {
      // Read the current argument into an ExposedArgument instance.
      const auto& currentArg = args[key];
      auto argument = std::make_unique<ExposedArgument>(key, currentArg);

      // Put it on the list.
      this->children.push_back(std::move(argument));
    }
  }
}

void ExposedComponent::serialize(Json::Value& object) const
{
  Json::Value args;
  for(const auto& child : this->children)
  {
    args[child->getKey()] = child->makeObject();
  }

  object["useEditorComponent"] = this->useEditorComponent;
  object["args"] = args;
}

Json::Value ExposedComponent::save(const Actor *editorActor) const
{
  Json::Value result;

  // Use the editor component, or a "base" JSON object where requested
  if(this->useEditorComponent)
  {
    const auto component = editorActor->findComponent(this->name);

    if(component != nullptr)
    {
      component->serialize(result);
    }
  }
  else if(this->json.isMember("base"))
  {
    result = this->json["base"];
  }

  // Write the properties
  for(const auto& child : this->children)
  {
    child->writeTo(result);
  }

  return result;
}

const std::string ExposedComponent::getName() const
{
  return this->name;
}

const std::vector<std::unique_ptr<ExposedArgument>>& ExposedComponent::getChildren()
{
  return this->children;
}

const void ExposedComponent::set(const std::string& key, const Json::Value& value)
{
  for(auto& child : this->children)
  {
    if(child->getKey() == key)
    {
      child->setValue(value);
      break;
    }
  }
}

}}
