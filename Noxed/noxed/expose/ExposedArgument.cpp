/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "ExposedArgument.h"

#include <noxed/gui/ElementLayout.h>
#include <noxed/gui/ElementType.h>

using noxed::gui::ElementLayout;
using noxed::gui::ElementType;

namespace noxed { namespace expose {

ExposedArgument::ExposedArgument(const std::string& key, const Json::Value& object):
  key(key),
  object(object)
{
  this->value = object["value"];
  this->type = object.get("type", "string").asString();
  this->object["default"] = object["value"];
}

ExposedArgument::~ExposedArgument()
{
}

const Json::Value ExposedArgument::getValue() const
{
  return this->value;
}

void ExposedArgument::setValue(const Json::Value& value)
{
  this->value = value;
}

const std::string& ExposedArgument::getKey() const
{
  return this->key;
}

const Json::Value ExposedArgument::getMin() const
{
  if(this->object["min"].isNumeric())
  {
    return this->object["min"];
  }
  else
  {
    return Json::nullValue;
  }
}

const Json::Value ExposedArgument::getMax() const
{
  if(this->object["max"].isNumeric())
  {
    return this->object["max"];
  }
  else
  {
    return Json::nullValue;
  }
}

bool ExposedArgument::isValid() const
{
  // Range check if numeric and limits are set
  if(this->value.isNumeric())
  {
    if(this->object["min"].isNumeric())
    {
      if(this->value < this->object["min"])
      {
        return false;
      }
    }
    if(this->object["max"].isNumeric())
    {
      if(this->value > this->object["max"])
      {
        return false;
      }
    }
  }

  // Type validation
  if(this->isString() && !this->value.isString())
  {
    return false;
  }

  if((this->isInt() || this->isFloat()) && !this->value.isNumeric())
  {
    return false;
  }

  if(this->isBool() && !this->value.isBool())
  {
    return false;
  }

  // If it got past this, then it's valid.
  return true;
}

bool ExposedArgument::isString() const
{
  return (this->type == "string");
}

bool ExposedArgument::isInt() const
{
  return (this->type == "int");
}

bool ExposedArgument::isFloat() const
{
  return (this->type == "float");
}

bool ExposedArgument::isBool() const
{
  return (this->type == "boolean");
}

bool ExposedArgument::isVector() const
{
  return (this->type == "vector");
}

void ExposedArgument::writeTo(Json::Value& target) const
{
  // This is a bit ugly, but it traverses the object, creating the intermediate
  // paths if they are not already there.
  // This is to be able to set values deeper into the component parameter
  // structure.

  Json::Value *iter = &target;

  if(this->object.isMember("path"))
  {
    for(auto& item : this->object["path"])
    {
      std::string itemKey = item.asString();

      if(!iter->isMember(itemKey))
      {
        (*iter)[itemKey] = Json::Value();
      }

      iter = &(*iter)[itemKey];
    }
  }

  (*iter)[this->key] = this->value;
}

Json::Value ExposedArgument::makeObject() const
{
  Json::Value copy = this->object;
  copy["value"] = copy["default"];
  copy.removeMember("default");

  return copy;
}

std::shared_ptr<ElementLayout> ExposedArgument::makeInputLayout(const std::string& componentName) const
{
  std::string layoutKey = componentName + "." + this->key;
  std::string layoutLabel = this->object.get("label", layoutKey).asString();

  if(this->isInt())
  {
    return std::make_shared<ElementLayout>(layoutKey, layoutLabel, ElementType::NUMBER_INT, this->getValue(), this->getMin(), this->getMax());
  }
  else if(this->isFloat())
  {
    return std::make_shared<ElementLayout>(layoutKey, layoutLabel, ElementType::NUMBER_FLOAT, this->getValue(), this->getMin(), this->getMax());
  }
  else if(this->isString())
  {
    return std::make_shared<ElementLayout>(layoutKey, layoutLabel, ElementType::STRING, this->getValue());
  }
  else if(this->isBool())
  {
    return std::make_shared<ElementLayout>(layoutKey, layoutLabel, ElementType::BOOLEAN, this->getValue());
  }
  else if(this->isVector())
  {
    return std::make_shared<ElementLayout>(layoutKey, layoutLabel, ElementType::VEC2_FLOAT, this->getValue());
  }
  else
  {
    return std::make_shared<ElementLayout>(layoutKey, layoutLabel, ElementType::OUTPUT_STRING, this->getValue());
  }
}

}}
