/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <nox/util/json_utils.h>

#include <string> // std::string
#include <memory> // std::unique_ptr

namespace noxed
{
  namespace gui
  {
    struct ElementLayout;
  }
}

namespace noxed { namespace expose {

/**
 * A template for an exposed component argument.
 */
class ExposedArgument
{
public:
  /**
   * Construct the exposed argument
   *
   * @param key The key to use
   * @param value The default value of the argument
   */
  ExposedArgument(const std::string& key, const Json::Value& value);
  ~ExposedArgument();

  /**
   * Get the value
   */
  const Json::Value getValue() const;

  /**
   * Set the value
   */
  void setValue(const Json::Value& value);

  /**
   * Get the key
   */
  const std::string& getKey() const;

  /**
   * Get the minimum
   */
  const Json::Value getMin() const;

  /**
   * Get the maximum
   */
  const Json::Value getMax() const;

  /**
   * Get the validity of this argument
   */
  bool isValid() const;

  /**
   * Get whether it is a string
   */
  bool isString() const;

  /**
   * Get whether it is an integer
   */
  bool isInt() const;

  /**
   * Get whether it is a floating point number
   */
  bool isFloat() const;

  /**
   * Get whether it is a boolean
   */
  bool isBool() const;

  /**
   * Get whether it is a vector
   */
  bool isVector() const;

  /**
   * Writes to a target component object
   */
  void writeTo(Json::Value& target) const;

  /**
   * Make the JSON object.
   */
  Json::Value makeObject() const;

  /**
   * Make an InputLayout with key <componentName>.<this->getName()> representing this object.
   */
  std::shared_ptr<noxed::gui::ElementLayout> makeInputLayout(const std::string& componentName) const;

private:
  std::string key;
  std::string type;
  Json::Value object;
  Json::Value value;
};

}}
