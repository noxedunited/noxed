/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <nox/util/json_utils.h>

#include <vector> // std::vector
#include <string> // std::string
#include <memory> // std::unique_ptr

// nox-engine prototypes
namespace nox { namespace logic { namespace actor {
  class Actor;
}}}

namespace noxed { namespace expose {

class ExposedArgument;

/**
 * A template for an exposed component.
 */
class ExposedComponent
{
public:
  /**
   * Construct the object and call initialize on it with the provided
   * parameters.
   *
   * @param name The name of the component
   * @param object The source JSON object to deserialize
   */
  ExposedComponent(const std::string& name, const Json::Value& object);

  /**
   * This constructor sets everything to default. Use this if you intend to
   * call initialize separately.
   */
  ExposedComponent();

  /**
   * This is a destructor. It is not very destructive.
   */
  ~ExposedComponent();

  /**
   * Initialize, or re-initialize, the exposed component.
   *
   * @param name The name of the component
   * @param object The source JSON object to deserialize
   */
  void initialize(const std::string& name, const Json::Value& object);

  /**
   * Serialize the instructions, and is NOT the function to use when
   * saving the results to an object. It is, however, useful if an application
   * changes the editor instructions on how to make this component happen.
   *
   * @param object The target object to write to.
   */
  void serialize(Json::Value& object) const;

  /**
   * Save the results from the edited object values and returns the result. Use
   * getName() to get the key that is meant to be used with the result in the
   * actor component list.
   *
   * @param editorActor The actor this is to read from
   * @returns The result that can be used to deserialize a component
   */
  Json::Value save(const nox::logic::actor::Actor *editorActor) const;

  /**
   * Get the component name.
   */
  const std::string getName() const;

  /**
   * Get all the children
   */
  const std::vector<std::unique_ptr<ExposedArgument>>& getChildren();

  /**
   * Set a parameter
   */
  const void set(const std::string& key, const Json::Value& value);

private:
  std::string name;
  Json::Value json;
  bool useEditorComponent;
  std::vector<std::unique_ptr<ExposedArgument>> children;
};

}}
