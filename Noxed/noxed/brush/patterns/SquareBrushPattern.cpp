/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "SquareBrushPattern.h"

namespace noxed { namespace brush { namespace patterns {

const SquareBrushPattern::IdType SquareBrushPattern::ID = "square";

std::vector<glm::ivec2> SquareBrushPattern::getCore(int brushSize) const
{
  brushSize = brushSize - 2;
  std::vector<glm::ivec2> results(1, glm::ivec2(0, 0));

  // If it's just 1 grid cell in size, return a blank vector
  if(brushSize < 0)
  {
    return std::vector<glm::ivec2>();
  }

  // Fill the core
  for(int i = -brushSize; i <= brushSize; ++i)
  {
    for(int j = -brushSize; j <= brushSize; ++j)
    {
      // Except for the center
      if(i != 0 || j != 0)
      {
        results.push_back(glm::ivec2(i, j));
      }
    }
  }

  return results;
}

std::vector<glm::ivec2> SquareBrushPattern::getEdge(int brushSize) const
{
  brushSize = brushSize - 1;
  std::vector<glm::ivec2> results;

  // If it's just 1 grid cell in size, return a blank vector
  if(brushSize < 1)
  {
    return std::vector<glm::ivec2>(1, glm::ivec2(0, 0));
  }

  // Go through the positions
  for(int i = -brushSize; i <= brushSize; ++i)
  {
    for(int j = -brushSize; j <= brushSize; ++j)
    {
      // Only add the position if it's on the edge
      if(i == -brushSize || i == brushSize || j == -brushSize || j == brushSize)
      {
        results.push_back(glm::ivec2(i, j));
      }
    }
  }

  return results;
}

}}}
