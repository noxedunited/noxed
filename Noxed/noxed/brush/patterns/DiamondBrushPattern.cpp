/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "DiamondBrushPattern.h"

namespace noxed { namespace brush { namespace patterns {

const DiamondBrushPattern::IdType DiamondBrushPattern::ID = "diamond";

std::vector<glm::ivec2> DiamondBrushPattern::getCore(int brushSize) const
{
  brushSize = brushSize - 3;
  std::vector<glm::ivec2> results(1, glm::ivec2(0, 0));

  // If it's just 1 grid cell in size, return a blank vector
  if(brushSize < 0)
  {
    return std::vector<glm::ivec2>();
  }

  // Fill the core
  for(int i = -brushSize; i <= brushSize; ++i)
  {
    for(int j = -brushSize+abs(i); j <= brushSize-abs(i); ++j)
    {
      // Except for the center
      if(i != 0 || j != 0)
      {
        results.push_back(glm::ivec2(i, j));
      }
    }
  }

  return results;
}

std::vector<glm::ivec2> DiamondBrushPattern::getEdge(int brushSize) const
{
  int cutoff = brushSize - 3;

  brushSize = brushSize - 1;
  std::vector<glm::ivec2> results;

  // If it's just 1 grid cell in size, return a blank vector
  if(brushSize < 0)
  {
    return std::vector<glm::ivec2>(1, glm::ivec2(0, 0));
  }

  // Go through the positions
  for(int i = -brushSize; i <= brushSize; ++i)
  {
    for(int j = -brushSize+abs(i); j <= brushSize-abs(i); ++j)
    {
      // Get the manhattan distance
      int dist = (abs(i) + abs(j));

      // If the manhattan distance is larger than the core's "radius"
      if(dist > cutoff)
      {
        results.push_back(glm::ivec2(i, j));
      }
    }
  }

  return results;
}

}}}

/*
     E
    EEE
   EECEE
  EECCCEE
   EECEE
    EEE
     E
*/
