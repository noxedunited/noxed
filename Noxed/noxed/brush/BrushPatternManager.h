/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <noxed/brush/BrushPattern.h>
#include <memory>
#include <map>

namespace noxed { namespace brush {

/**
 * A singleton that manages the brush patterns. It can be registered with, or
 * get instances of the brush patterns registered.
 */
class BrushPatternManager
{
public:

  /**
   * Get the brush pattern by id
   *
   * @param id The ID that matches the ::ID member of the brush pattern
   * @returns The brush pattern identified by that id, or nullptr if none do
   */
   BrushPattern *getPattern(BrushPattern::IdType id);

   /**
    * Get the brush pattern by index
    *
    * @param index The index
    * @returns The brush pattern identified by that id, or nullptr if none do
    */
   BrushPattern *getPattern(const int index);

   /**
    * Get the count of patterns, to find out the bounds.
    */
   const int getPatternCount() const;

  /**
   * This makes an instance of the pattern type and associates it with the ID.
   */
  template <typename BrushPatternType>
  void registerPatternType();

  /**
   * Fetch the singleton instance.
   */
  static BrushPatternManager *getInstance();

private:
  BrushPatternManager() = default;

  std::map<BrushPattern::IdType, std::unique_ptr<BrushPattern>> patterns;
  std::vector<BrushPattern *> patternArray;

  static BrushPatternManager *instance;
};

template <typename BrushPatternType>
void BrushPatternManager::registerPatternType()
{
  static_assert(std::is_base_of<BrushPattern, BrushPatternType>::value == true, "BrushPatternType is not derived from BrushPattern");

  auto ptr = std::make_unique<BrushPatternType>();

  this->patternArray.push_back(ptr.get());
    this->patterns.insert(std::make_pair(BrushPatternType::ID, std::move(ptr)));
}

}}
