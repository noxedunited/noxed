/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <nox/logic/actor/event/Event.h>
#include <json/value.h>

namespace nox
{
  namespace logic
  {
    namespace actor
    {
      class Actor;
    }
  }
}

namespace noxed { namespace events {

/**
 * A broadcast calling upon every actor listening actor to write a deserializlbe
 * piece of JSON to the array for further processing.
 */
class WorldSaveBroadcast: public nox::logic::actor::Event
{
public:
	static const IdType ID;

  /**
   * Construct a broadcast
   *
   * @param actor World-saving actor
   * @param worldRoot Root JSON object for the world
   * @param actorArray JSON object to put the actors in.
   */
  WorldSaveBroadcast(nox::logic::actor::Actor* actor, const Json::Value& worldRoot, const Json::Value& actorArray);

  /**
   * Get the actor array JSON object. Only use this if it is necessary, and not
   * covered by the two submission functions.
   */
  Json::Value& getWorldRoot();

  /**
   * Get the actor array JSON object. Only use this if it is necessary, and not
   * covered by the two submission functions.
   */
  Json::Value& getActorArray();

  /**
   * Submit actor JSON data
   */
  void submitActor(const Json::Value& value);

  /**
   * Submit actor JSON data, and the fact that this can be controlled. The last one to
   * submit in this way will end up being the controlled actor in the saved world.
   */
  void submitControllerActor(const Json::Value& value);

protected:
  std::string name;
  Json::Value worldRoot;
  Json::Value actorArray;
};

}}
