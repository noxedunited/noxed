/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <nox/logic/actor/event/Event.h>
#include <glm/vec2.hpp>
#include <json/value.h>

#include <noxed/gui/ElementType.h>
#include <noxed/gui/ElementLayout.h>

namespace nox
{
  namespace logic
  {
    namespace actor
    {
      class Actor;
    }
  }
}

namespace noxed { namespace events {

/**
 * An event telling an actor that a GUI value has changed. The actor should be
 * the same one as the event that requested the creation of the GUI element.
 */
class GuiValueChange: public nox::logic::actor::Event
{
public:
	static const IdType ID;

  /**
   * Construct a tile placement event.
   *
   * @param actor Actor to associate event with
   * @param elementId Element id where value was changed
   * @param key Name of the variable
   * @param value The new value for the variable
   */
  GuiValueChange(nox::logic::actor::Actor* actor, const std::string& elementId, const std::string& key, const Json::Value& value);

  /**
   * Get the element id
   */
  const std::string& getElementId() const;

  /**
   * Get the key from the changed input
   */
  const std::string& getKey() const;

  /**
   * Get the value from the changed input
   */
  const Json::Value& getValue() const;

protected:
  std::string elementId;
  std::string key;
  Json::Value value;
};

}}
