/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <nox/logic/actor/event/Event.h>
#include <glm/vec2.hpp>

namespace nox
{
  namespace logic
  {
    namespace actor
    {
      class Actor;
    }
  }
}

namespace noxed { namespace events {

/**
 * A request for a named tile set. It will be answered by an emitted
 * TileSetResponse event with the same name.
 */
class TileSetRequest: public nox::logic::actor::Event
{
public:
	static const IdType ID;

  /**
   * Construct a request
   *
   * @param controledActor Currently controlled actor.
   * @param modeName Name of the mode.
   * @param modeValue Value of the mode.
   */
  TileSetRequest(nox::logic::actor::Actor* actor, const std::string name, const std::string sender);

  /**
   * Get the name of the tile set that is requested.
   */
  const std::string getName() const;

  /**
   * Get the name of the sender tile map
   */
  const std::string getSender() const;

protected:
  std::string name;
  std::string sender;
};

}}
