/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <nox/logic/actor/event/Event.h>
#include <glm/vec2.hpp>

namespace nox
{
  namespace logic
  {
    namespace actor
    {
      class Actor;
    }
  }
}

namespace noxed { namespace events {

/**
 * An actor event representing a mode change from a key. This can
 * be for changing editor modes, selecting weapons, or firing abilities. It is
 * not meant to keep track of modes, but to change them in components that are
 * listening. At any rate, it's a way to associate a key with an arbitrary mode
 * name and signed integer value.
 *
 * It's statelessness make it only support setting modes, not toggling them,
 * although a listener could handle that.
 *
 * This event is emitted by noxed::handlers::ModeChangeKeyboardMapper
 */
class ModeChange: public nox::logic::actor::Event
{
public:
	static const IdType ID;

  /**
   * Construct a mode change with the given name and value.
   *
   * @param controledActor Currently controlled actor.
   * @param modeName Name of the mode.
   * @param modeValue Value of the mode.
   */
  ModeChange(nox::logic::actor::Actor* controlledActor, const std::string modeName, const int modeValue);

  /**
   * Get the name of the mode being set.
   */
  const std::string getName() const;

  /**
   * Get the value of the mode being set.
   */
  const int getValue() const;

protected:
  std::string modeName;
  int modeValue;
};

}}
