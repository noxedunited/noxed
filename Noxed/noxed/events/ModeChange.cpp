/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include <noxed/events/ModeChange.h>

#include <nox/logic/actor/event/Event.h>

#include <SDL.h>

namespace noxed { namespace events {

const ModeChange::IdType ModeChange::ID = "noxed.control.keyboard.modechange";

ModeChange::ModeChange(nox::logic::actor::Actor* controlledActor, const std::string modeName, const int modeValue):
  nox::logic::actor::Event(ID, controlledActor),
  modeName(modeName),
  modeValue(modeValue)
{
}

const std::string ModeChange::getName() const
{
  return this->modeName;
}
const int ModeChange::getValue() const
{
  return this->modeValue;
}

}}
