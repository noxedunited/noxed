/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "WorldSaveBroadcast.h"

#include <iostream>

#include <nox/logic/actor/event/Event.h>

#include <SDL.h>

namespace noxed { namespace events {

const WorldSaveBroadcast::IdType WorldSaveBroadcast::ID = "noxed.worldsave.broadcast";

WorldSaveBroadcast::WorldSaveBroadcast(nox::logic::actor::Actor* actor, const Json::Value& worldRoot, const Json::Value& actorArray):
  nox::logic::actor::Event(ID, actor),
  worldRoot(worldRoot),
  actorArray(actorArray)
{
}

Json::Value& WorldSaveBroadcast::getWorldRoot()
{
  this->worldRoot["actors"] = this->actorArray;
  
  return this->worldRoot;
}

Json::Value& WorldSaveBroadcast::getActorArray()
{
  return this->actorArray;
}

void WorldSaveBroadcast::submitActor(const Json::Value& value)
{
  this->actorArray.append(value);
}

void WorldSaveBroadcast::submitControllerActor(const Json::Value& value)
{
  const auto nextIndex = this->actorArray.size();
  this->worldRoot["views"][0]["controlledActor"] = nextIndex;

  this->submitActor(value);
}


}}
