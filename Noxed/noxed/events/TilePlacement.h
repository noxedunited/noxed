/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <nox/logic/actor/event/Event.h>
#include <glm/vec2.hpp>

namespace nox
{
  namespace logic
  {
    namespace actor
    {
      class Actor;
    }
  }
}

namespace noxed
{
  namespace brush
  {
    class BrushPattern;
  }
}

namespace noxed { namespace events {

/**
 * An event signalling that a named tile map is to set the tile at a given world
 * position. It is up to the tile map to calculate the logical tile position
 * and set it.
 */
class TilePlacement: public nox::logic::actor::Event
{
public:
	static const IdType ID;

  /**
   * Construct a tile placement event.
   *
   * @param actor Actor to associate event with
   * @param name Name of the tile set
   * @param position World position of the tile
   * @param value Tile value to set it to
   */
  TilePlacement(nox::logic::actor::Actor* actor, const std::string name, const glm::vec2 position, const int value, const noxed::brush::BrushPattern *brushPattern, const int brushSize);

  /**
   * Get the tile map name.
   */
  const std::string getName() const;

  /**
   * Get the world position of the tile placement event.
   */
  const glm::vec2 getPosition() const;

  /**
   * Get the value to set the tile to.
   */
  const int getValue() const;

  /**
   * Get brush pattern
   */
  const noxed::brush::BrushPattern* getBrushPattern();

  /**
   * Get brush size
   */
  const int getBrushSize() const;

protected:
  std::string name;
  glm::vec2 position;
  int value;
  const noxed::brush::BrushPattern *brushPattern;
  int brushSize;
};

}}
