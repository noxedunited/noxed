/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <nox/logic/actor/event/Event.h>
#include <glm/vec2.hpp>

namespace nox
{
  namespace logic
  {
    namespace actor
    {
      class Actor;
    }
  }
}

namespace noxed { namespace events {

/**
 * A mouse movement or button press. It has two IDs so
 * a listener can ask for either, or both. It contains the world position,
 * screen position and screen size of the event as well, for more control over
 * the mouse events.
 *
 * This class is handled by NoxedWindowView
 */
class MouseAction: public nox::logic::actor::Event
{
public:
	static const IdType MOVE;
  static const IdType BUTTON;
  static const IdType HOLD;

  static const int BUTTON_LEFT;
  static const int BUTTON_MIDDLE;
  static const int BUTTON_RIGHT;

  /**
   * Create an mouse move action.
   *
   * @param controledActor Currently controlled actor.
   * @param screenPosition Position on screen where the cursor is.
   * @param screenSize Size of screen.
   * @param worldPosition World position derived from screen position.
   */
  MouseAction(IdType type, nox::logic::actor::Actor* controlledActor, const glm::ivec2& screenPosition, const glm::ivec2& screenSize, const glm::vec2& worldPosition, const bool press, const int button);

  /**
   * Get the screen position of the mouse, which can be used along with
   * the screen size.
   */
  const glm::ivec2& getScreenPosition() const;

  /**
   * Get the screen size.
   */
  const glm::ivec2& getScreenSize() const;

  /**
   * Get the world position of the mouse cursor.
   */
  const glm::vec2& getWorldPosition() const;

  /**
   * Get the button number, and returns and int equal to either
   * MouseButtonAction::BUTTON_LEFT, MouseButtonAction::BUTTON_MIDDLE
   * or MouseButtonAction::BUTTON_RIGHT. Those constants are set to the SDL
   * equivalents.
   */
  const int& getButton() const;

  /**
   * Get whether the event is a press.
   */
  const bool& isPress() const;

  /**
   * Get whether the event is a release. It is functionally equal to
   * !isPress();
   */
  const bool isRelease() const;

  /**
   * Returns true if the left button was clicked. Functionally equivalent to
   * checking the getButton() function's result against the constant.
   */
  const bool isLeft() const;

  /**
   * Returns true if the middle button was clicked. Functionally equivalent to
   * checking the getButton() function's result against the constant.
   */
  const bool isMiddle() const;

  /**
   * Returns true if the right button was clicked. Functionally equivalent to
   * checking the getButton() function's result against the constant.
   */
  const bool isRight() const;

protected:
  glm::ivec2 screenPosition;
  glm::ivec2 screenSize;
  glm::vec2 worldPosition;
  bool press;
  int button;
};

}}
