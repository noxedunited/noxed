/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <nox/logic/actor/event/Event.h>
#include <json/value.h>

namespace nox
{
  namespace logic
  {
    namespace actor
    {
      class Actor;
    }
    namespace event
    {
      class Event;
    }
  }
}

namespace noxed { namespace events {

/**
 * A response sent out to the world save event, queued for after the world save
 * event. The world saver can react to this and be sure that all listening
 * listeners have submitted their data.
 */
class WorldSaveResponse: public nox::logic::actor::Event
{
public:
	static const IdType ID;

  /**
   * Construct a response to the broadcast
   *
   * @param actor World-saving actor
   * @param broadcast The broadcast that this is in response to
   */
  WorldSaveResponse(nox::logic::actor::Actor* actor, const std::shared_ptr<nox::logic::event::Event>& event);

  /**
   * Get the actor array JSON object. Only use this if it is necessary, and not
   * covered by the two submission functions.
   */
  const Json::Value& getWorldRoot() const;

protected:
  std::string name;
  std::shared_ptr<nox::logic::event::Event> event;
};

}}
