/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <nox/logic/actor/event/Event.h>
#include <glm/vec2.hpp>
#include <json/value.h>

#include <noxed/gui/ElementType.h>
#include <noxed/gui/ElementLayout.h>

namespace nox
{
  namespace logic
  {
    namespace actor
    {
      class Actor;
    }
  }
}

namespace noxed { namespace events {

/**
 * An event signalling that an UI element should display new data.
 */
 class GuiLayoutNotification: public nox::logic::actor::Event
 {
 public:
 	static const IdType ID;

   /**
    * Construct a tile placement event.
    *
    * @param actor Actor to associate event with
    * @param name Name of the GUI element to update (or add)
    * @param layouts A set of information describing the layout
    */
   GuiLayoutNotification(nox::logic::actor::Actor* actor, const std::string& elementId, const std::vector<std::shared_ptr<noxed::gui::ElementLayout>>& layouts);

   /**
    * Get the unique id describing the GUI element
    */
   const std::string& getElementId() const;

   /**
    * Get the list of input layouts that makes up the GUI element
    */
   const std::vector<std::shared_ptr<noxed::gui::ElementLayout>>& getLayouts() const;

 protected:
   std::string elementId;
   std::vector<std::shared_ptr<noxed::gui::ElementLayout>> layouts;
 };

}}
