/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include <noxed/events/MouseAction.h>

#include <nox/logic/actor/event/Event.h>

#include <SDL.h>

namespace noxed { namespace events {

const MouseAction::IdType MouseAction::MOVE = "noxed.control.mouse.move";
const MouseAction::IdType MouseAction::BUTTON = "noxed.control.mouse.button";
const MouseAction::IdType MouseAction::HOLD = "noxed.control.mouse.hold";

const int MouseAction::BUTTON_LEFT = SDL_BUTTON_LEFT;
const int MouseAction::BUTTON_MIDDLE = SDL_BUTTON_MIDDLE;
const int MouseAction::BUTTON_RIGHT = SDL_BUTTON_RIGHT;

MouseAction::MouseAction(MouseAction::IdType type, nox::logic::actor::Actor* controlledActor, const glm::ivec2& screenPosition, const glm::ivec2& screenSize, const glm::vec2& worldPosition, const bool press, const int button):
  nox::logic::actor::Event(type, controlledActor),
  screenPosition(screenPosition),
  screenSize(screenSize),
  worldPosition(worldPosition),
  press(press),
  button(button)
{
}

const glm::ivec2& MouseAction::getScreenPosition() const
{
  return this->screenPosition;
}

const glm::ivec2& MouseAction::getScreenSize() const
{
  return this->screenSize;
}

const glm::vec2& MouseAction::getWorldPosition() const
{
  return this->worldPosition;
}

const bool& MouseAction::isPress() const
{
  return this->press;
}

const bool MouseAction::isRelease() const
{
  return !this->press;
}

const bool MouseAction::isLeft() const
{
  return (button == BUTTON_LEFT);
}

const bool MouseAction::isMiddle() const
{
  return (button == BUTTON_MIDDLE);
}

const bool MouseAction::isRight() const
{
  return (button == BUTTON_RIGHT);
}

}}
