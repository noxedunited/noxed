/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "TilePlacement.h"

#include <noxed/brush/BrushPattern.h>

#include <SDL.h>

using noxed::brush::BrushPattern;

namespace noxed { namespace events {

const TilePlacement::IdType TilePlacement::ID = "noxed.tileset.tileplacement";

TilePlacement::TilePlacement(nox::logic::actor::Actor* actor, const std::string name, const glm::vec2 position, const int value, const BrushPattern *brushPattern, const int brushSize):
  nox::logic::actor::Event(ID, actor),
  name(name),
  position(position),
  value(value),
  brushPattern(brushPattern),
  brushSize(brushSize)
{
}

const std::string TilePlacement::getName() const
{
  return this->name;
}

const glm::vec2 TilePlacement::getPosition() const
{
  return this->position;
}

const int TilePlacement::getValue() const
{
  return this->value;
}

const int TilePlacement::getBrushSize() const
{
  return this->brushSize;
}

const BrushPattern* TilePlacement::getBrushPattern()
{
  return this->brushPattern;
}

}}
