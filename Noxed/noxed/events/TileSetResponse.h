/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <nox/logic/actor/event/Event.h>
#include <glm/vec2.hpp>

namespace nox
{
  namespace logic
  {
    namespace actor
    {
      class Actor;
    }
  }
}

namespace noxed
{
  namespace tileset
  {
    class TileSet;
    class TileSetManager;
  }
}

namespace noxed { namespace events {

/**
 * A response to a TileSetRequest and contain the tileset, or
 * a null-pointer were the tileset not to be found. It is recommended to make
 * sure initialization code in response to this event do not run twice were
 * another object to request the same tile set.
 */
class TileSetResponse: public nox::logic::actor::Event
{
public:
	static const IdType ID;

  /**
   * Construct a response that a requester would be waiting for.
   *
   * @param actor Currently controlled actor.
   * @param name Name of the mode.
   * @param set Value of the mode.
   */
  TileSetResponse(nox::logic::actor::Actor* actor, const std::string name, const std::string target, noxed::tileset::TileSet* const set, noxed::tileset::TileSetManager* manager);

  /**
   * Get the tile set name. If it matches the one in the reqquest, then this
   * is the answer to the request.
   */
  const std::string getName() const;

  /**
   * Get the target, which is the name of the requester.
   */
  const std::string getTarget() const;

  /**
   * Get the tile set that was requested.
   */
  noxed::tileset::TileSet * const getTileSet() const;

  /**
   * Get a reference to the manager, in case that is needed.
   */
  noxed::tileset::TileSetManager * const getTileSetManager() const;

protected:
  std::string name;
  std::string target;
  noxed::tileset::TileSet *set;
  noxed::tileset::TileSetManager* manager;
};

}}
