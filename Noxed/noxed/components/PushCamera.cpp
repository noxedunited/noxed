/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "PushCamera.h"

#include <math.h>
#include <vector>

#include <nox/app/log/Logger.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/IContext.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/control/actor/ActorControl.h>
#include <nox/util/json_utils.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/logic/control/event/Action.h>

#include <glm/gtc/matrix_transform.hpp>

#include <noxed/events/MouseAction.h>
#include <noxed/events/ModeChange.h>
#include <noxed/NoxedWindowView.h>

using nox::logic::actor::Component;
using nox::logic::actor::Transform;
using nox::logic::actor::TransformChange;
using nox::logic::event::Event;
using nox::logic::control::Action;
using nox::Duration;
using noxed::events::ModeChange;

namespace noxed { namespace components
{

const Component::IdType PushCamera::NAME = "Noxed::PushCamera";

	PushCamera::PushCamera():
	listener("PushCamera")
{

}

PushCamera::~PushCamera() = default;

bool PushCamera::initialize(const Json::Value& componentJsonObject)
{
	this->edgeSize = componentJsonObject.get("edgeSize", 0.1f).asFloat();
	this->speed = componentJsonObject.get("speed", 8.0f).asFloat();
	this->movable = componentJsonObject.get("movable", true).asBool();
	this->pushable = componentJsonObject.get("pushable", true).asBool();
	this->moveLockMode = componentJsonObject.get("moveLockMode", "moveLock").asString();
	this->moveLock = false;

	this->keyboardMovement = glm::vec2(0.0f, 0.0f);
	this->mouseMovement = glm::vec2(0.0f, 0.0f);

	this->listener.setup(this, this->getLogicContext()->getEventBroadcaster());
	if(this->movable)
	{
		this->listener.addEventTypeToListenFor(Action::ID);
	}
	if(this->pushable)
	{
		this->listener.addEventTypeToListenFor(TransformChange::ID);
	}
	this->listener.addEventTypeToListenFor(ModeChange::ID);
	this->listener.startListening();

	this->log = this->getLogicContext()->createLogger();
	this->log.setName("Component/PushCamera");

	return true;
}

void PushCamera::serialize(Json::Value& componentObject)
{
	componentObject["edgeSize"] = this->edgeSize;
	componentObject["speed"] = this->speed;
	componentObject["movable"] = this->movable;
	componentObject["pushable"] = this->pushable;
}

void PushCamera::onCreate()
{
	this->actorTransform = this->getOwner()->findComponent<Transform>();

	// Adapt position and target to transform location
	this->position = this->actorTransform->getPosition();
}

const Component::IdType& PushCamera::getName() const
{
	return NAME;
}

void PushCamera::onUpdate(const Duration& deltaTime) {
	if(!this->moveLock)
	{
		float deltaSeconds = std::chrono::duration_cast<std::chrono::duration<float>>(deltaTime).count();
		float deltaSpeed = this->speed * deltaSeconds; //;this->speed * (deltaTime.count() * 1000000);

		glm::vec2 combinedMovement = this->keyboardMovement + this->mouseMovement;

		// Only proceed if the movement is above a small threshold (the smallest non-zero value is 1.0f anyway)
		if(glm::length(combinedMovement) > 0.001) {
			this->position += glm::normalize(combinedMovement) * deltaSpeed;

			const auto id = this->getOwner()->getId();
			const auto view = static_cast<const NoxedWindowView *>(this->getLogicContext()->findControllingView(id));

			if (view != nullptr && view->getControlledActor() == this->getOwner())
			{
				auto camera = view->getActiveCamera();

				camera->setPosition(this->position);
				//camera->setRotation(this->rotation);
			}
		}
	}
}

void PushCamera::onComponentEvent(const std::shared_ptr<Event>& event)
{
	if (event->isType(TransformChange::ID) && this->pushable)
	{
		const auto transformEvent = static_cast<nox::logic::actor::TransformChange*>(event.get());
		const auto id = this->getOwner()->getId();
		const auto view = static_cast<const NoxedWindowView *>(this->getLogicContext()->findControllingView(id));

		if (view != nullptr && view->getControlledActor() == this->getOwner())
		{
			auto camera = view->getActiveCamera();

			glm::vec2 position = transformEvent->getPosition();
			auto bbox = camera->getBoundingAABB();

			// Edge length is the actual length of the pushable screen edge.
			float edgeLength = bbox.getHeight() * this->edgeSize;

			// These are how close to the side of the screen the object is.
			float edgeLeft = position.x - bbox.getLeft();
			float edgeRight = abs(position.x - bbox.getRight());
			float edgeBottom = position.y - bbox.getBottom();
			float edgeTop = abs(position.y - bbox.getTop());

			// Find horizontal push movement
			if(edgeLeft < edgeLength)
			{
				this->mouseMovement.x = -1.0f;
			}
			else if(edgeRight < edgeLength)
			{
				this->mouseMovement.x = 1.0f;
			}
			else
			{
				this->mouseMovement.x = 0.0f;
			}

			// Find vertical push movement
			if(edgeBottom < edgeLength)
			{
				this->mouseMovement.y = -1.0f;
			}
			else if(edgeTop < edgeLength)
			{
				this->mouseMovement.y = 1.0f;
			}
			else
			{
				this->mouseMovement.y = 0.0f;
			}
		}
	}
}

void PushCamera::onEvent(const std::shared_ptr<Event>& event)
{
	if (event->isType(Action::ID) && this->movable)
	{
		const auto actionEvent = static_cast<nox::logic::control::Action*>(event.get());

		if(actionEvent->getControlName() == "move")
		{
			this->keyboardMovement = actionEvent->getControlVector();
		}
	} 
	else if (event->isType(ModeChange::ID))
	{
		auto modeChange = std::static_pointer_cast<ModeChange>(event);

		if(modeChange->getName() == this->moveLockMode)
		{
			this->moveLock = static_cast<bool>(modeChange->getValue());
		}
	}
}

void PushCamera::makePushable()
{
	if(!this->pushable)
	{
		this->pushable = true;
		this->listener.addEventTypeToListenFor(TransformChange::ID);
	}
}
void PushCamera::makeMovable()
{
	if(!this->movable)
	{
		this->movable = true;
		this->listener.addEventTypeToListenFor(Action::ID);
	}
}

} }
