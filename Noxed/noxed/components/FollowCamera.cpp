/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#include "FollowCamera.h"

#include <math.h>
#include <vector>

#include <nox/app/log/Logger.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/IContext.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/control/actor/ActorControl.h>
#include <nox/util/json_utils.h>
#include <nox/logic/actor/event/TransformChange.h>

#include <glm/gtc/matrix_transform.hpp>

#include <noxed/NoxedWindowView.h>

using nox::logic::actor::Component;
using nox::logic::actor::Transform;
using nox::logic::event::Event;
using nox::util::writeJsonVec;
using nox::util::parseJsonVec;
using nox::logic::actor::TransformChange;

namespace noxed { namespace components
{

const Component::IdType FollowCamera::NAME = "Noxed::FollowCamera";

FollowCamera::FollowCamera():
	listener("FollowCamera")
{

}

FollowCamera::~FollowCamera() = default;

bool FollowCamera::initialize(const Json::Value& componentJsonObject)
{
	this->offset = parseJsonVec(componentJsonObject["offset"], glm::vec2(0.0f, 0.0f));
	this->moveCamera = componentJsonObject.get("move", true).asBool();
	this->rotateCamera = componentJsonObject.get("rotate", true).asBool();

	this->listener.setup(this, this->getLogicContext()->getEventBroadcaster());
	this->listener.addEventTypeToListenFor(TransformChange::ID);
	this->listener.startListening();

	this->log = this->getLogicContext()->createLogger();
	this->log.setName("Component/FollowCamera");

	return true;
}

void FollowCamera::serialize(Json::Value& componentObject)
{
	componentObject["offset"] = writeJsonVec(this->offset);
	componentObject["move"] = this->moveCamera;
	componentObject["rotate"] = this->rotateCamera;
}

void FollowCamera::onCreate()
{
	this->actorTransform = this->getOwner()->findComponent<Transform>();
}

const Component::IdType& FollowCamera::getName() const
{
	return NAME;
}

void FollowCamera::onComponentEvent(const std::shared_ptr<Event>& event)
{
	if (event->isType(TransformChange::ID))
	{
		const auto transformEvent = static_cast<nox::logic::actor::TransformChange*>(event.get());
		const auto id = this->getOwner()->getId();
		const auto view = static_cast<const NoxedWindowView *>(this->getLogicContext()->findControllingView(id));

		if (view != nullptr && view->getControlledActor() == this->getOwner())
		{
			auto camera = view->getActiveCamera();

			if(this->moveCamera)
			{
				camera->setPosition(transformEvent->getPosition() + this->offset);
			}
			if(this->rotateCamera)
			{
				camera->setRotation(transformEvent->getRotation());
			}
		}
	}
}

void FollowCamera::onEvent(const std::shared_ptr<Event>& event)
{

}

} }
