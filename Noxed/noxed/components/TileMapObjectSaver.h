/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#pragma once

#include <nox/logic/actor/Component.h>
#include <nox/logic/event/Event.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/app/log/Logger.h>
#include <glm/vec2.hpp>

namespace nox
{
	namespace logic
	{
		namespace actor
		{
			class Transform;
		}
	}
}

namespace noxed
{
	namespace components
	{
		class TileMap;
	}
	namespace tileset
	{
		class TileSet;
	}
}

namespace noxed { namespace components
{

/**
 * This saves objects at tile positions upon getting a world save broadcast.
 *
 * # JSON Description
 * ## Name
 * %Noxed::TileMapObjectSaver
 *
 * ## Properties
 * - __objects__ a map of tags and what the object extends
 */
class TileMapObjectSaver final: public nox::logic::actor::Component, public nox::logic::event::IListener
{
public:
	const static IdType NAME;

	TileMapObjectSaver();
	virtual ~TileMapObjectSaver();

	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;
	void onCreate() override;
	const IdType& getName() const override;

	/**
	 * If not pushable, make it pushable. This function does nothing if it already is, and can not be
	 * undone.
	 */
	void makePushable();

	/**
	 * If not movable, make it movable. This function does nothing if it already is, and can not be
	 * undone.
	 */
	void makeMovable();

	/**
	 * Set tile set
	 */
	void setTileSet(const noxed::tileset::TileSet *tileset);

	virtual void onComponentEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;
	virtual void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;

private:
	nox::logic::actor::Transform* actorTransform;
	noxed::components::TileMap* actorTileMap;

	nox::logic::event::ListenerManager listener;
	nox::app::log::Logger log;
	std::vector<std::string> objectTiles;
	std::vector<std::string> tags;
	std::map<std::string, Json::Value> objectMap;
};

} }
