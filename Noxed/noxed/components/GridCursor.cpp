/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "GridCursor.h"

#include <math.h>
#include <vector>

#include <boost/lexical_cast.hpp>
#include <nox/app/log/Logger.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/IContext.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/control/event/Action.h>
#include <nox/logic/control/actor/ActorControl.h>
#include <nox/util/json_utils.h>
#include <glm/gtc/matrix_transform.hpp>
#include <nox/app/resource/Handle.h>
#include <nox/app/resource/IResourceAccess.h>

#include <noxed/NoxedWindowView.h>
#include <noxed/gui/ElementLayout.h>
#include <noxed/gui/ElementType.h>
#include <noxed/events/GuiLayoutNotification.h>
#include <noxed/events/GuiValueChange.h>
#include <noxed/events/MouseAction.h>
#include <noxed/events/ModeChange.h>

using nox::logic::actor::Actor;
using nox::logic::actor::Component;
using nox::logic::actor::Transform;
using nox::logic::event::Event;
using nox::util::writeJsonVec;
using nox::util::parseJsonVec;
using noxed::gui::ElementLayout;
using noxed::gui::ElementType;
using noxed::events::MouseAction;
using noxed::events::ModeChange;
using noxed::events::GuiLayoutNotification;
using noxed::events::GuiValueChange;

namespace noxed { namespace components
{

const Component::IdType GridCursor::NAME = "Noxed::GridCursor";

GridCursor::GridCursor():
	listener("GridCursor")
{

}

GridCursor::~GridCursor() = default;

bool GridCursor::initialize(const Json::Value& componentJsonObject)
{
	this->gridSize = parseJsonVec(componentJsonObject["gridSize"], glm::vec2(1.0f, 1.0f));
	this->offset = parseJsonVec(componentJsonObject["offset"], glm::vec2(0.0f, 0.0f));
	this->moveLockMode = componentJsonObject.get("moveLockMode", "moveLock").asString();
	this->moveLock = false;

	this->listener.setup(this, this->getLogicContext()->getEventBroadcaster());
	this->listener.addEventTypeToListenFor(MouseAction::MOVE);
	this->listener.addEventTypeToListenFor(ModeChange::ID);
	this->listener.addEventTypeToListenFor(GuiValueChange::ID);
	this->listener.startListening();

	this->log = this->getLogicContext()->createLogger();
	this->log.setName("Component/GridCursor");
	this->log.info().raw("Component Created!");

	this->worldManager = this->getLogicContext()->getWorldManager();

	return true;
}

void GridCursor::serialize(Json::Value& componentObject)
{
	componentObject["gridSize"] = writeJsonVec(this->gridSize);
	componentObject["offset"] = writeJsonVec(this->offset);
	componentObject["moveLockMode"] = this->moveLockMode;
}

void GridCursor::onCreate()
{
	this->actorTransform = this->getOwner()->findComponent<Transform>();

	this->initGuiLayout();
}

const Component::IdType& GridCursor::getName() const
{
	return NAME;
}

void GridCursor::onComponentEvent(const std::shared_ptr<Event>& event)
{
}

void GridCursor::onEvent(const std::shared_ptr<Event>& event)
{
	if (event->isType(MouseAction::MOVE))
	{
		if(this->moveLock)
		{
			return;
		}

		auto action = std::static_pointer_cast<MouseAction>(event);

		auto vector = this->snap(action->getWorldPosition());
		this->actorTransform->setPosition(vector);

		this->updateGuiLayout();
	}
	else if (event->isType(ModeChange::ID))
	{
		auto modeChange = std::static_pointer_cast<ModeChange>(event);

		if(modeChange->getName() == this->moveLockMode)
		{
			this->moveLock = static_cast<bool>(modeChange->getValue());
		}
	}
	else if (event->isType(GuiValueChange::ID))
	{
		auto valueChange = std::static_pointer_cast<GuiValueChange>(event);

		if(valueChange->getKey() == "gridSize")
		{
			this->gridSize = parseJsonVec(valueChange->getValue(), glm::vec2(0.f, 0.f));

			auto position = this->actorTransform->getPosition();
			this->actorTransform->setPosition(this->snap(position));

			this->updateGuiLayout();
		}
	}
}

glm::vec2 GridCursor::snap(const glm::vec2 position) const
{
	glm::vec2 vector = position;

	if(this->gridSize.x > 0.0f) // If snap x
	{
		int xSnap = roundf(vector.x / this->gridSize.x);
		vector.x = static_cast<float>(xSnap * this->gridSize.x);
	}

	if(this->gridSize.y > 0.0f) // If snap y
	{
		int ySnap = roundf(vector.y / this->gridSize.y);
		vector.y = static_cast<float>(ySnap * this->gridSize.y);
	}

	return vector;
}

void GridCursor::initGuiLayout()
{
	// Create elements
	this->gridSizeLayout = std::make_shared<ElementLayout>("gridSize", "Grid Size", ElementType::VEC2_FLOAT, Json::Value());
	this->positionLayout = std::make_shared<ElementLayout>("centerPosition", "Cursor Position", ElementType::OUTPUT_STRING, "0, 0");

	// Add them to list
	this->layouts.push_back(this->gridSizeLayout);
	this->layouts.push_back(this->positionLayout);

	// Update it
	this->updateGuiLayout();
}

void GridCursor::updateGuiLayout()
{
	// Update grid size
	this->gridSizeLayout->value["x"] = this->gridSize.x;
	this->gridSizeLayout->value["y"] = this->gridSize.y;

	// Update position
	const float x = this->actorTransform->getPosition().x;
	const float y = this->actorTransform->getPosition().y;
	this->positionLayout->value = boost::lexical_cast<std::string>(x) + ", " + boost::lexical_cast<std::string>(y);

	// Notify GUI Handler
	auto event = std::make_shared<GuiLayoutNotification>(this->getOwner(), "GridCursorOptions", this->layouts);
	this->getLogicContext()->getEventBroadcaster()->queueEvent(event);
}

} }
