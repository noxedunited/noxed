/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "EditorBlueprint.h"

#include <math.h>
#include <vector>

#include <glm/gtc/matrix_transform.hpp>

#include <nox/app/log/Logger.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/IContext.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/control/actor/ActorControl.h>
#include <nox/util/json_utils.h>
#include <nox/logic/actor/event/TransformChange.h>

#include <noxed/NoxedWindowView.h>
#include <noxed/expose/ExposedComponent.h>
#include <noxed/expose/ExposedArgument.h>
#include <noxed/events/WorldSaveBroadcast.h>
#include <noxed/events/GuiLayoutNotification.h>
#include <noxed/events/GuiValueChange.h>
#include <noxed/events/EntitySelection.h>
#include <noxed/gui/ElementLayout.h>

using nox::logic::actor::Component;
using nox::logic::actor::Transform;
using nox::logic::event::Event;
using nox::util::writeJsonVec;
using nox::util::parseJsonVec;
using nox::logic::actor::TransformChange;
using noxed::expose::ExposedComponent;
using noxed::expose::ExposedArgument;
using noxed::events::WorldSaveBroadcast;
using noxed::events::GuiLayoutNotification;
using noxed::events::GuiValueChange;
using noxed::events::EntitySelection;
using noxed::gui::ElementLayout;
using noxed::gui::ElementType;

namespace noxed { namespace components
{

const Component::IdType EditorBlueprint::NAME = "Noxed::EditorBlueprint";

EditorBlueprint::EditorBlueprint():
	listener("EditorBlueprint")
{

}

EditorBlueprint::~EditorBlueprint() = default;

bool EditorBlueprint::initialize(const Json::Value& componentJsonObject)
{
	this->extends = componentJsonObject.get("extends", "").asString();
	this->controlled = componentJsonObject.get("controlled", false).asBool();
	this->occupant = componentJsonObject.get("occupant", true).asBool();
	this->components = componentJsonObject["components"];

	this->selected = false;

	this->listener.setup(this, this->getLogicContext()->getEventBroadcaster());
	this->listener.addEventTypeToListenFor(WorldSaveBroadcast::ID);
	this->listener.addEventTypeToListenFor(GuiValueChange::ID);
	this->listener.addEventTypeToListenFor(EntitySelection::ID);
	this->listener.startListening();

	this->log = this->getLogicContext()->createLogger();
	this->log.setName("Component/EditorBlueprint");

	// Set up the component blue prints
	const auto componentNameList = this->components.getMemberNames();
	for(const auto& componentName : componentNameList)
	{
		const auto& component = this->components[componentName];
		auto newComp = std::make_unique<ExposedComponent>(componentName, component);

		this->exposedComponents.push_back(std::move(newComp));
	}

	return true;
}

void EditorBlueprint::serialize(Json::Value& componentObject)
{
	componentObject["extends"] = this->extends;
	componentObject["controlled"] = this->controlled;
	componentObject["components"] = this->components;
}

void EditorBlueprint::onCreate()
{
	this->actorTransform = this->getOwner()->findComponent<Transform>();

	this->initGuiLayout();
}

const Component::IdType& EditorBlueprint::getName() const
{
	return NAME;
}

void EditorBlueprint::exportJson(Json::Value& objectRoot) const
{
	// This is the "components" part of the object
	Json::Value objectComponents;

	// Add transform
	Json::Value transformComponentObject;
	this->actorTransform->serialize(transformComponentObject);
	objectComponents["Transform"] = transformComponentObject;

	// For each component, save their result
	for(const auto& component : this->exposedComponents)
	{
		objectComponents[component->getName()] = component->save(this->getOwner());
	}

	// Save the entire object.
	if(this->extends != "")
	{
		objectRoot["extend"] = this->extends;
	}
	objectRoot["components"] = objectComponents;
}

const bool EditorBlueprint::isControlled() const
{
	return this->controlled;
}

const bool EditorBlueprint::canOccupyGrid() const
{
	return this->occupant;
}

const bool EditorBlueprint::isSelected() const
{
	return this->selected;
}

void EditorBlueprint::onComponentEvent(const std::shared_ptr<Event>& event)
{
}

void EditorBlueprint::onEvent(const std::shared_ptr<Event>& event)
{
	if(event->isType(WorldSaveBroadcast::ID))
	{
		auto saveBroadcast = static_cast<WorldSaveBroadcast *>(event.get());

		Json::Value actorDefinition;
		this->exportJson(actorDefinition);

		if(this->controlled)
		{
			saveBroadcast->submitControllerActor(actorDefinition);
		}
		else
		{
			saveBroadcast->submitActor(actorDefinition);
		}
	}
	else if(event->isType(GuiValueChange::ID))
	{
		auto valueChange = static_cast<GuiValueChange*>(event.get());

		if(valueChange->getActor() == this->getOwner())
		{
			std::string key = valueChange->getKey();

			if(key.find(".") != std::string::npos)
			{
				auto it = exposedArgumentMap.find(key);

				if(it != exposedArgumentMap.end())
				{
					// C++ is being weird, somehow this log entry prevents a crash.
					this->log.debug().format("EntityBlueprint set(%s = %s)", key, it->second->getValue());
					it->second->setValue(valueChange->getValue());
				}
			}

			this->updateGuiLayout();
		}
	}
	else if(event->isType(EntitySelection::ID))
	{
		auto entitySelection = static_cast<EntitySelection*>(event.get());

		this->selected = (entitySelection->getActor() == this->getOwner());

		this->updateGuiLayout();
	}
}

void EditorBlueprint::initGuiLayout()
{
	for(auto& component : exposedComponents)
	{
		auto componentTitleLayout = std::make_shared<ElementLayout>("", "", ElementType::LAYOUT_SEPARATOR);
		this->layouts.push_back(componentTitleLayout);

		for(auto& child : component->getChildren())
		{
			auto childLayout = child->makeInputLayout(component->getName());
			this->layouts.push_back(childLayout);

			exposedArgumentMap.insert(std::make_pair(childLayout->key, child.get()));
		}
	}

	this->updateGuiLayout();
}

void EditorBlueprint::updateGuiLayout()
{
	if(this->selected)
	{
		int currentLayout = 0;
		bool foundArgument = false;

		for(auto& component : exposedComponents)
		{
			++currentLayout;

			for(auto& child : component->getChildren())
			{
				auto childLayout = this->layouts[currentLayout++];
				childLayout->value = child->getValue();

				foundArgument = true;
			}
		}

		if(foundArgument)
		{
			auto event = std::make_shared<GuiLayoutNotification>(this->getOwner(), "EditorBlueprint", this->layouts);
			this->getLogicContext()->getEventBroadcaster()->queueEvent(event);
		}
		else
		{
			auto event = std::make_shared<GuiLayoutNotification>(this->getOwner(), "EditorBlueprint", this->emptyLayouts);
			this->getLogicContext()->getEventBroadcaster()->queueEvent(event);
		}
	}
}

} }
