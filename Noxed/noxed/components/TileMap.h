/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <vector>
#include <map>
#include <memory>

#include <glm/vec2.hpp>

#include <nox/logic/actor/Component.h>
#include <nox/logic/graphics/actor/ActorGraphics.h>
#include <nox/logic/graphics/event/SceneNodeEdited.h>
#include <nox/logic/event/Event.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/app/log/Logger.h>
#include <nox/app/graphics/2d/TransformationNode.h>
#include <nox/app/graphics/2d/SceneGraphNode.h>

#include <noxed/tileset/TileSet.h>
#include <noxed/tileset/TileGrid.h>
#include <noxed/graphics/TileBlockRenderNode.h>

namespace nox
{
	namespace logic
	{
		namespace actor
		{
			class Transform;
		}
	}
}

namespace noxed
{
	namespace components
	{
		class EditorBlueprint;
	}
	namespace gui
	{
		struct ElementLayout;
	}
}

namespace noxed { namespace components
{

/**
 * Displays an editable tile map at the actor's position.
 *
 * # JSON Description
 * ## Name
 * %Noxed::TileMap
 *
 * ## Properties
 * - __offset__:vec2 - Offset from actor position.
 * - __name__: - Name of tile map
 * - __tileset__: - Name of tile set to use
 * - __renderLevel__:int - Render level of tile map.
 * - __blockWidth__:int - Width of each tile grid block.
 * - __smart__:bool - Use smart tiles
 * - __dumbDown__:bool - If smart tiles are used, export as non-smart tile set
 * - __editorMap__:bool - Makes map respond to GUI and enetiy selection events
 * - __blocks__:array - Blocks in the tile grid
 * - __blocks[N].x__:int - X index of the block
 * - __blocks[N].y__:int - Y index of the block
 * - __blocks[N].items__:array - Array of tile indices in the block
 */
class TileMap final: public nox::logic::actor::Component, public nox::logic::event::IListener
{
public:
	const static IdType NAME;

	TileMap();
	virtual ~TileMap();

	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;
	void onCreate() override;
	void onDestroy() override;
	void onDeactivate() override;
	void onActivate() override;
	const IdType& getName() const override;

	virtual void onComponentEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;
	virtual void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;

	/**
	 * Get a constant pointer to the tile set this tile map uses.
	 */
	const noxed::tileset::TileSet *getTileSet() const;

	/**
	 * Get a constant pointer to the smart grid for read operations.
	 */
	const noxed::tileset::TileGrid *getSmartGrid() const;

	/**
	 * Get a constant pointer to the resolved grid for read operations.
	 */
	const noxed::tileset::TileGrid *getGrid() const;

	/**
	 * Get the name of this map.
	 */
	const std::string getMapName() const;

	/**
	 * Get the name of the tile set this tile map uses.
	 */
	const std::string getTileSetName() const;

	/**
	 * Get the offset
	 */
	const glm::vec2 getOffset() const;

	/**
	 * Set render level
	 */
	void setRenderLevel(const int value);

	/**
	 * Set the tile set
	 */
	void setTileSet(noxed::tileset::TileSet *set);

	/**
	 * Set the map name
	 */
	void setMapName(const std::string& name);

	/**
	 * Update GUI layout.
	 */
	void updateGuiLayout();

private:

	/**
	 * Adds a block from the tile grid to this map for rendering and syncronization.
	 *
	 * @param block The block
	 */
	void addBlock(noxed::tileset::TileGrid::Block *block);

	/**
	 * Updates the actor transform node for the renderer
	 */
	void updateActorTransform();

	/**
	 * Tells the scene graph what our root node is, and that it and children
	 * should be rendered
	 *
	 * @param node The node to broadcast
	 */
	void broadcastSceneNodeCreation(std::shared_ptr<nox::app::graphics::SceneGraphNode> node);

	/**
	 * Tells the scene graph to hide this tile map
	 *
	 * @param node The node to broadcast
	 */
	void broadcastSceneNodeRemoval(std::shared_ptr<nox::app::graphics::SceneGraphNode> node);

	/**
	 * Marks the block in question for changing, adding it if it's not already
	 * covered by a render node. This is a free operation and can be called
	 * multiple times in the same frame without incuring more node recreations
	 *
	 * @param blockId The block ID to flag.
	 */
	void syncBlockChange(const int blockId);

	/**
	 * Sets the tile and syncs the block change. If it's smart, it's resolved
	 * along with its neighbors.
	 *
	 * @param tileX The tile x index to write to
	 * @param tileY The tile y index to write to
	 * @param value The value to write. -1 effectively "deletes" a tile
	 */
	void setTile(const int tileX, const int tileY, const int value, const bool resolveNeighbors, const bool resolveSelf);

	/**
	 * Manually resolves the smart tile index at that position into a "dumb" tile
	 * index at the same position in the other grid. It is used by setTile if
	 * "resolveSelf" argument is true
	 *
	 * Neighbor resolution is not recursive.
	 *
	 * @param tileX The tile x index
	 * @param tileY The tile y index
	 * @param resolveNeighbors If true, it will also resolve its neighbors
	 */
	void resolveTile(const int tileX, const int tileY, const bool resolveNeighbors);

	/**
	 * This resolves all smart tiles. It's used during construction, and should
	 * only be used when the entire tileset is changed, and not changed by the
	 * above method for setting tiles.
	 */
	void resolveAllSmartTiles();

	/**
	 * Called when the object is ready to set up the GUI elemetns.
	 */
	void initGuiLayout();

	nox::logic::actor::Transform* actorTransform;
	noxed::components::EditorBlueprint* actorBlueprint;
	nox::logic::event::ListenerManager listener;
	nox::app::log::Logger log;

	std::unique_ptr<noxed::tileset::TileGrid> grid;
	std::unique_ptr<noxed::tileset::TileGrid> smartGrid;
	noxed::tileset::TileSet *tileset;

	glm::vec2 offset;
	std::string name;
	std::string tilesetName;
	bool enabled;
	bool smart;
	bool dumbDown;
	bool editorMap;
	int renderLevel;

	std::shared_ptr<nox::app::graphics::TransformationNode> rootTransformNode;
	std::shared_ptr<nox::app::graphics::TransformationNode> renderTransformNode;

	std::vector<std::shared_ptr<noxed::graphics::TileBlockRenderNode>> renderNodes;

	std::vector<std::shared_ptr<noxed::gui::ElementLayout>> layouts;
	std::shared_ptr<noxed::gui::ElementLayout> headerLayout;
	std::shared_ptr<noxed::gui::ElementLayout> nameLayout;
	std::shared_ptr<noxed::gui::ElementLayout> offsetLayout;
	std::shared_ptr<noxed::gui::ElementLayout> dumbDownLayout;

	std::vector<std::shared_ptr<noxed::gui::ElementLayout>> emptyLayout;
};

} }
