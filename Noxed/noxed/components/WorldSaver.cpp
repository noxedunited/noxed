/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "WorldSaver.h"

#include <fstream>

#include <nox/app/log/Logger.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/IContext.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/control/actor/ActorControl.h>
#include <nox/util/json_utils.h>

#include <noxed/NoxedWindowView.h>
#include <noxed/components/EditorBlueprint.h>
#include <noxed/events/WorldSaveBroadcast.h>
#include <noxed/events/WorldSaveResponse.h>
#include <noxed/events/ModeChange.h>

using nox::logic::actor::Component;
using nox::logic::event::Event;
using nox::util::writeJsonVec;
using nox::util::parseJsonVec;

using noxed::events::WorldSaveBroadcast;
using noxed::events::WorldSaveResponse;
using noxed::events::ModeChange;

namespace noxed { namespace components
{

const Component::IdType WorldSaver::NAME = "Noxed::WorldSaver";

WorldSaver::WorldSaver() :
  listener("WorldSaver")
{

}

WorldSaver::~WorldSaver() = default;

bool WorldSaver::initialize(const Json::Value& componentJsonObject)
{
  this->targetFile = componentJsonObject.get("file", "./world.json").asString();

	this->listener.setup(this, this->getLogicContext()->getEventBroadcaster());
	this->listener.addEventTypeToListenFor(ModeChange::ID);
  this->listener.addEventTypeToListenFor(WorldSaveResponse::ID);
	this->listener.startListening();

	this->log = this->getLogicContext()->createLogger();
	this->log.setName("Component/WorldSaver");

  this->worldManager = this->getLogicContext()->getWorldManager();

	return true;
}

void WorldSaver::serialize(Json::Value& componentObject)
{

}

void WorldSaver::onCreate()
{

}

const Component::IdType& WorldSaver::getName() const
{
	return NAME;
}

void WorldSaver::onEvent(const std::shared_ptr<Event>& event)
{
  if (event->isType(ModeChange::ID))
  {
    const auto moveEvent = static_cast<ModeChange*>(event.get());

    if (moveEvent->getName() == "saveWorld" && moveEvent->getValue() == 1)
    {
      this->log.info().raw("Saving...");
      this->saveWorld();
    }
  }
  else if (event->isType(WorldSaveResponse::ID))
  {
    const auto responsEvent = static_cast<WorldSaveResponse*>(event.get());
    const auto worldRoot = responsEvent->getWorldRoot();

    this->log.info().format("World saved! Controlled actor = %i, Entity count = %i", worldRoot["views"][0]["controlledActor"].asInt(), worldRoot["actors"].size());

    std::ofstream jsonFile(this->targetFile);

    if (jsonFile.is_open())
    {
      Json::StyledStreamWriter writer;

      writer.write(jsonFile, worldRoot);
      jsonFile.close();
    }
    else
    {
      this->log.info().raw("Failed to open file");
    }
  }
}

void WorldSaver::saveWorld()
{
  Json::StyledStreamWriter writer;

  Json::Value root;
  Json::Value views;
  Json::Value actors = Json::arrayValue;

  Json::Value viewParameters;

  viewParameters["type"] = "registered";
  viewParameters["registeredIndex"] = 0;
  viewParameters["controlledActor"] = 0;

  views.append(viewParameters);
  root["views"] = views;
  root["actors"] = actors;

  auto event = std::make_shared<WorldSaveBroadcast>(this->getOwner(), root, actors);
  auto broadcaster = this->getLogicContext()->getEventBroadcaster();
  broadcaster->queueEvent(event);
}


}}
