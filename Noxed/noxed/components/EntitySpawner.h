/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#pragma once

#include <nox/logic/actor/Component.h>
#include <nox/logic/event/Event.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/app/log/Logger.h>
#include <glm/vec2.hpp>

#include <string>
#include <vector>

namespace nox
{
	namespace logic
	{
		namespace actor
		{
			class Transform;
			class Actor;
		}
		namespace world
		{
			class Manager;
		}
	}
}

namespace noxed
{
	namespace gui
	{
		struct ElementLayout;
	}
}

namespace noxed { namespace components
{

/**
 * Spawns, selects or deletes actors at the current position.
 *
 * # JSON Description
 * ## Name
 * %Noxed::EntitySpawner
 *
 * ## Properties
 * - __offset__:vec2 - Offset from actor position. Use half the width and height to center it.
 * - __editorMode__:int - Which editor mode that will activate this component.
 * - __quickbar.modeName__:string - Key-bindable mode name that sets selected actor.
 * - __quickbar.defaults__:array - List of actor names to bind to keyboard keys.
 */
class EntitySpawner final: public nox::logic::actor::Component, public nox::logic::event::IListener
{
public:
	const static IdType NAME;

	EntitySpawner();
	virtual ~EntitySpawner();

	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;
	void onCreate() override;
	const IdType& getName() const override;

	virtual void onComponentEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;
	virtual void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;

	void updateGuiLayout();

private:
	void initGuiLayout();
	void selectActor(nox::logic::actor::Actor* actor);

	nox::logic::actor::Transform* actorTransform;
	nox::logic::world::Manager* worldManager;
	nox::app::log::Logger log;
	nox::logic::event::ListenerManager listener;
	glm::vec2 offset;

	int editorModeValue;
	int currentEditorMode;
	nox::logic::actor::Actor* selectedActor;

	std::string quickbarModeName;
	std::vector<std::string> quickbarActorNames;
	int quickbarActorIndex;

	std::vector<std::shared_ptr<noxed::gui::ElementLayout>> layouts;
	std::shared_ptr<noxed::gui::ElementLayout> entitySelectLayout;
	std::shared_ptr<noxed::gui::ElementLayout> entityNameLayout;
	std::vector<std::shared_ptr<noxed::gui::ElementLayout>> emptyLayoutArray;
};

} }
