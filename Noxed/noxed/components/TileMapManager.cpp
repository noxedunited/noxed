/*
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 * Copyright (c) 2016 Gisle Aune (dev@gisle.me
 * Copyright (c) 2016 Tor Strømme (tor@stroemme.no)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "TileMapManager.h"

#include <math.h>
#include <vector>

#include <boost/lexical_cast.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <nox/app/log/Logger.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/IContext.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/control/event/Action.h>
#include <nox/logic/control/actor/ActorControl.h>
#include <nox/util/json_utils.h>
#include <nox/app/resource/Handle.h>
#include <nox/app/resource/IResourceAccess.h>

#include <noxed/NoxedWindowView.h>
#include <noxed/brush/BrushPattern.h>
#include <noxed/brush/BrushPatternManager.h>
#include <noxed/components/EditorBlueprint.h>
#include <noxed/components/TileMap.h>
#include <noxed/components/TileSpawner.h>
#include <noxed/components/TileMapObjectSaver.h>
#include <noxed/events/MouseAction.h>
#include <noxed/events/ModeChange.h>
#include <noxed/events/TilePlacement.h>
#include <noxed/events/TileSetRequest.h>
#include <noxed/events/TileSetResponse.h>
#include <noxed/events/EntitySelection.h>
#include <noxed/events/GuiLayoutNotification.h>
#include <noxed/events/GuiValueChange.h>
#include <noxed/gui/ElementLayout.h>

using nox::logic::actor::Actor;
using nox::logic::actor::Component;
using nox::logic::actor::Transform;
using nox::logic::actor::TransformChange;
using nox::logic::event::Event;
using nox::util::writeJsonVec;
using nox::util::parseJsonVec;
using noxed::components::EditorBlueprint;
using noxed::components::TileMap;
using noxed::components::TileSpawner;
using noxed::components::TileMapObjectSaver;
using noxed::events::MouseAction;
using noxed::events::ModeChange;
using noxed::events::TilePlacement;
using noxed::events::TileSetRequest;
using noxed::events::TileSetResponse;
using noxed::events::EntitySelection;
using noxed::events::GuiLayoutNotification;
using noxed::events::GuiValueChange;
using noxed::brush::BrushPattern;
using noxed::brush::BrushPatternManager;
using noxed::gui::ElementLayout;
using noxed::gui::ElementType;

namespace noxed { namespace components
{

const Component::IdType TileMapManager::NAME = "Noxed::TileMapManager";

TileMapManager::TileMapManager():
	listener("TileMapManager")
{

}

TileMapManager::~TileMapManager() = default;

bool TileMapManager::initialize(const Json::Value& componentJsonObject)
{
	this->spawnActor = componentJsonObject.get("spawnActor", "TileMap").asString();

	this->listener.setup(this, this->getLogicContext()->getEventBroadcaster());
	this->listener.addEventTypeToListenFor(MouseAction::HOLD);
	this->listener.addEventTypeToListenFor(ModeChange::ID);
	this->listener.addEventTypeToListenFor(GuiValueChange::ID);
	this->listener.addEventTypeToListenFor(TileSetResponse::ID);
	this->listener.startListening();

	this->log = this->getLogicContext()->createLogger();
	this->log.setName("Component/TileMapManager");

	// May I speak with your [tile set] manager, please?
	auto broadcaster = this->getLogicContext()->getEventBroadcaster();
	broadcaster->queueEvent(std::make_shared<TileSetRequest>(this->getOwner(), "", ""));

	this->worldManager = this->getLogicContext()->getWorldManager();

	selectedSet = -1;
	selectedMap = -1;
	mapCounter = 0;

	return true;
}

void TileMapManager::serialize(Json::Value& componentObject)
{
}

void TileMapManager::onCreate()
{
}

const Component::IdType& TileMapManager::getName() const
{
	return NAME;
}

void TileMapManager::onComponentEvent(const std::shared_ptr<Event>& event)
{
}

void TileMapManager::onEvent(const std::shared_ptr<Event>& event)
{
	if(event->isType(GuiValueChange::ID))
	{
		auto valueChange = static_cast<GuiValueChange*>(event.get());

		if(valueChange->getActor() == this->getOwner())
		{
			if(valueChange->getKey() == "btnAddMap")
			{
				if(selectedSet >= 0 && selectedSet < this->manager->getCount())
				{
					auto instance = this->worldManager->createActorFromDefinitionName(this->spawnActor);

					auto tileMap = instance->findComponent<TileMap>();
					auto objectSaver = instance->findComponent<TileMapObjectSaver>();

					if(tileMap != nullptr)
					{
						auto set = this->manager->find(selectedSet);

						this->mapActors.push_back(instance.get());

						instance->activate();
						this->worldManager->manageActor(std::move(instance));

						objectSaver->setTileSet(set);
						tileMap->setTileSet(set);
						tileMap->setMapName(set->getName() + " " + boost::lexical_cast<std::string>(mapCounter++));
					}
					else
					{
						this->log.error().format("Actor definition for \"%s\" lacks a TileMap component", this->spawnActor);
					}
				}
			}
			else	if(valueChange->getKey() == "btnRemoveMap")
			{
				if(selectedMap >= 0 && selectedMap < this->mapActors.size())
				{
					// Find
					auto actor = this->mapActors[selectedMap];

					// Unselect (if necessary)
					if(actor->findComponent<EditorBlueprint>()->isSelected())
					{
						auto selectEvent = std::make_shared<EntitySelection>(nullptr);
						auto guiEvent = std::make_shared<GuiLayoutNotification>(this->getOwner(), "TileMapOptions", this->emptyLayouts);
						this->getLogicContext()->getEventBroadcaster()->queueEvent(selectEvent);
						this->getLogicContext()->getEventBroadcaster()->queueEvent(guiEvent);
					}

					// Kill
					this->mapActors.erase(this->mapActors.begin() + selectedMap);
					actor->deactivate();
					actor->destroy();
					this->worldManager->removeActor(actor->getId());
				}
			}
			else if(valueChange->getKey() == "mapList")
			{
				this->selectedMap = valueChange->getValue().asInt();

				if(this->selectedMap >= 0)
				{
					auto actor = this->mapActors[this->selectedMap];
					auto tileMap = actor->findComponent<TileMap>();
					auto tileSpawner = this->getOwner()->findComponent<TileSpawner>();

					auto event = std::make_shared<EntitySelection>(actor);
					this->getLogicContext()->getEventBroadcaster()->queueEvent(event);

					if(tileMap != nullptr && tileSpawner != nullptr)
					{
						tileSpawner->setTarget(tileMap->getMapName());
						tileSpawner->setTileSet(tileMap->getTileSet());
					}
					else
					{
						this->log.error().format("tileMap != nullptr: %b", (tileMap != nullptr));
						this->log.error().format("tileSpawner != nullptr: %b", (tileSpawner != nullptr));
					}
				}
			}
			else if(valueChange->getKey() == "setList")
			{
				this->selectedSet = valueChange->getValue().asInt();
			}

			this->updateGuiLayout();
		}
		else if(event->isType(EntitySelection::ID))
		{
			this->updateGuiLayout();
		}
	}
	else if(event->getType() == TileSetResponse::ID)
	{
		auto responseEvent = static_cast<TileSetResponse*>(event.get());
		this->manager = responseEvent->getTileSetManager();

		// The component has heard enough
		this->listener.removeEventTypeToListenFor(TileSetResponse::ID);

		// Initialize GUI
		this->initGuiLayout();
	}
}

void TileMapManager::initGuiLayout()
{
	this->headerLayout = std::make_shared<ElementLayout>("", "Tile Maps", ElementType::LAYOUT_HEADER);
	this->mapListboxLayout = std::make_shared<ElementLayout>("mapList", "Tile Maps", ElementType::LIST_STRING);
	this->addButtonLayout = std::make_shared<ElementLayout>("btnAddMap", " Add Map ", ElementType::BUTTON);
	this->removeButtonLayout = std::make_shared<ElementLayout>("btnRemoveMap", " Remove Map ", ElementType::BUTTON);
	this->setListboxLayout = std::make_shared<ElementLayout>("setList", "Tile Sets", ElementType::LIST_STRING);

	this->layouts.push_back(this->headerLayout);
	this->layouts.push_back(this->mapListboxLayout);
	this->layouts.push_back(this->removeButtonLayout);
	this->layouts.push_back(this->setListboxLayout);
	this->layouts.push_back(this->addButtonLayout);

	for(int i = 0; i < this->manager->getCount(); ++i)
	{
		auto set = this->manager->find(i);
		this->setListboxLayout->listItems.push_back(set->getName());

		this->log.info().raw(set->getName());
	}

	this->updateGuiLayout();
}

void TileMapManager::updateGuiLayout()
{
	// The indices
	this->setListboxLayout->value = this->selectedSet;

	// Set up listbox and find selection
	this->mapListboxLayout->listItems.clear();
	this->mapListboxLayout->value = this->selectedMap;
	for(int i = 0; i < this->mapActors.size(); ++i)
	{
		auto actor = this->mapActors[i];
		auto blueprint = actor->findComponent<EditorBlueprint>();
		auto tileMap = actor->findComponent<TileMap>();

		this->mapListboxLayout->listItems.push_back(tileMap->getMapName());
	}

	auto event = std::make_shared<GuiLayoutNotification>(this->getOwner(), "TileMapManager", this->layouts);
	this->getLogicContext()->getEventBroadcaster()->queueEvent(event);
}

} }
