/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "TileMapObjectSaver.h"

#include <math.h>
#include <vector>

#include <nox/app/log/Logger.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/IContext.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/control/actor/ActorControl.h>
#include <nox/util/json_utils.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/logic/control/event/Action.h>

#include <glm/gtc/matrix_transform.hpp>

#include <noxed/NoxedWindowView.h>
#include <noxed/components/TileMap.h>
#include <noxed/events/WorldSaveBroadcast.h>
#include <noxed/events/TileSetResponse.h>
#include <noxed/tileset/TileSet.h>

using nox::logic::actor::Component;
using nox::logic::actor::Transform;
using nox::logic::event::Event;
using nox::util::writeJsonVec;
using nox::util::parseJsonVec;
using noxed::events::WorldSaveBroadcast;
using noxed::events::TileSetResponse;
using noxed::components::TileMap;
using noxed::tileset::TileSet;

namespace noxed { namespace components
{

const Component::IdType TileMapObjectSaver::NAME = "Noxed::TileMapObjectSaver";

	TileMapObjectSaver::TileMapObjectSaver():
	listener("TileMapObjectSaver")
{
}

TileMapObjectSaver::~TileMapObjectSaver() = default;

bool TileMapObjectSaver::initialize(const Json::Value& componentJsonObject)
{
	this->listener.setup(this, this->getLogicContext()->getEventBroadcaster());
	this->listener.addEventTypeToListenFor(WorldSaveBroadcast::ID);
	this->listener.addEventTypeToListenFor(TileSetResponse::ID);
	this->listener.startListening();

	this->log = this->getLogicContext()->createLogger();
	this->log.setName("Component/TileMapObjectSaver");

	if(!componentJsonObject.isMember("objects"))
	{
		return false;
	}

	const auto& objects = componentJsonObject["objects"];
	this->tags = objects.getMemberNames();

	for(const std::string& tag : this->tags)
	{
		Json::Value objectTemplate;
		objectTemplate["extend"] = objects[tag].asString();
		objectTemplate["components"] = Json::Value();
		objectTemplate["components"]["Transform"] = Json::Value();

		this->objectMap.insert(std::make_pair(tag, objectTemplate));
	}

	return true;
}

void TileMapObjectSaver::serialize(Json::Value& componentObject)
{
	Json::Value objects;

	for(auto& it : this->objectMap) {
		objects[it.first] = it.second["extends"].asString();
	}

	componentObject["objects"] = objects;
}

void TileMapObjectSaver::onCreate()
{
	this->actorTransform = this->getOwner()->findComponent<Transform>();
	this->actorTileMap = this->getOwner()->findComponent<TileMap>();
}

const Component::IdType& TileMapObjectSaver::getName() const
{
	return NAME;
}

void TileMapObjectSaver::setTileSet(const TileSet *tileset)
{
	this->log.info().raw("Correct TileSetResponse acquired!");

	// Map all smart tile indices to empty string, so they're randomly
	// accessible. That means the object can instantly know if smart tile #4
	// is something this object should spawn something at.
	this->objectTiles.resize(tileset->getSmartTileCount(), "");

	for(int i = 0; i < tileset->getSmartTileCount(); ++i)
	{
		const auto smartTile = tileset->getSmartTile(i);

		for(const std::string& tag : this->tags)
		{
			if(smartTile->canHaveTag(tag))
			{
				this->objectTiles[i] = tag;
				break;
			}
		}
	}
}

void TileMapObjectSaver::onComponentEvent(const std::shared_ptr<Event>& event)
{
}

void TileMapObjectSaver::onEvent(const std::shared_ptr<Event>& event)
{
	if (event->isType(WorldSaveBroadcast::ID))
	{
		const auto broadcast = static_cast<WorldSaveBroadcast*>(event.get());
		const auto tileset = this->actorTileMap->getTileSet();
		const auto smartGrid = this->actorTileMap->getSmartGrid();
		const auto resolvedGrid = this->actorTileMap->getGrid();
		const int smartTileCount = tileset->getSmartTileCount();
		const auto offset = glm::vec2(std::ceil(this->actorTileMap->getOffset().x), std::ceil(this->actorTileMap->getOffset().y));

		if(smartGrid == nullptr)
		{
			this->log.error().format("TileMap \"%s\" does not have any smart tiles! Skipping world save...", this->actorTileMap->getMapName());
			return;
		}

		for(int i = 0; i < smartGrid->getBlockCount(); ++i)
		{
			const auto smartBlock = smartGrid->getBlock(i);
			const auto resolvedBlock = resolvedGrid->getBlock(smartBlock->x, smartBlock->y);

			if(resolvedBlock != nullptr)
			{
				//this->log.info().format("Checking block [%i, %i] for object tiles...", smartBlock->x, smartBlock->y);

				for(int j = 0; j < smartBlock->tiles.size(); ++j)
				{
					const int smartIndex = smartBlock->tiles[j];

					if(smartIndex >= 0 && smartIndex < smartTileCount)
					{
						const int resolvedIndex = resolvedBlock->tiles[j];
						const auto smartTile = tileset->getSmartTile(smartIndex);
						const auto tileTag = this->objectTiles[smartIndex];

						if(!tileTag.empty() && smartTile->hasTag(tileTag, resolvedIndex))
						{
							Json::Value actorDefinition = this->objectMap[tileTag];
							glm::vec2 position = this->actorTransform->getPosition() + offset + smartGrid->getTilePosition(smartBlock, j);

							actorDefinition["components"]["Transform"]["position"] = writeJsonVec(position);

							broadcast->submitActor(actorDefinition);
						}
					}
				}
			}
		}
	}
	else if(event->isType(TileSetResponse::ID))
	{
		auto responseEvent = static_cast<TileSetResponse*>(event.get());
		const auto tileset = responseEvent->getTileSet();

		if(tileset != nullptr && tileset->getName() == this->actorTileMap->getTileSetName())
		{
			this->setTileSet(tileset);
		}
	}

}

} }
