/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "TileSpawner.h"

#include <math.h>
#include <vector>

#include <glm/gtc/matrix_transform.hpp>
#include <nox/app/log/Logger.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/IContext.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/control/event/Action.h>
#include <nox/logic/control/actor/ActorControl.h>
#include <nox/util/json_utils.h>
#include <nox/app/resource/Handle.h>
#include <nox/app/resource/IResourceAccess.h>

#include <noxed/NoxedWindowView.h>
#include <noxed/brush/BrushPattern.h>
#include <noxed/brush/BrushPatternManager.h>
#include <noxed/components/Brush.h>
#include <noxed/events/MouseAction.h>
#include <noxed/events/ModeChange.h>
#include <noxed/events/TilePlacement.h>
#include <noxed/events/TileSetResponse.h>
#include <noxed/events/GuiLayoutNotification.h>
#include <noxed/events/GuiValueChange.h>
#include <noxed/gui/ElementLayout.h>
#include <noxed/tileset/TileSet.h>

using nox::logic::actor::Actor;
using nox::logic::actor::Component;
using nox::logic::actor::Transform;
using nox::logic::actor::TransformChange;
using nox::logic::event::Event;
using nox::logic::world::Manager;
using nox::util::writeJsonVec;
using nox::util::parseJsonVec;
using noxed::brush::BrushPattern;
using noxed::brush::BrushPatternManager;
using noxed::events::MouseAction;
using noxed::events::ModeChange;
using noxed::events::TilePlacement;
using noxed::events::TileSetResponse;
using noxed::events::GuiLayoutNotification;
using noxed::events::GuiValueChange;
using noxed::gui::ElementLayout;
using noxed::gui::ElementType;
using noxed::tileset::TileSet;

namespace noxed { namespace components
{

const Component::IdType TileSpawner::NAME = "Noxed::TileSpawner";

TileSpawner::TileSpawner():
	listener("TileSpawner")
{

}

TileSpawner::~TileSpawner() = default;

bool TileSpawner::initialize(const Json::Value& componentJsonObject)
{
	this->offset = parseJsonVec(componentJsonObject["offset"], glm::vec2(0.0f, 0.0f));
	this->target = componentJsonObject.get("target", "default").asString();
	this->editorModeValue = componentJsonObject.get("editorMode", 0).asInt();
	this->currentEditorMode = 0;
	this->quickbarModeName = componentJsonObject.get("quickbarModeName", "quickbar").asString();
	this->quickbarTileIndex = 0;

	this->listener.setup(this, this->getLogicContext()->getEventBroadcaster());
	this->listener.addEventTypeToListenFor(MouseAction::HOLD);
	this->listener.addEventTypeToListenFor(ModeChange::ID);
	this->listener.addEventTypeToListenFor(GuiValueChange::ID);
	this->listener.addEventTypeToListenFor(TileSetResponse::ID);
	this->listener.startListening();

	this->log = this->getLogicContext()->createLogger();
	this->log.setName("Component/TileSpawner");

	return true;
}

void TileSpawner::serialize(Json::Value& componentObject)
{
	componentObject["offset"] = writeJsonVec(this->offset);
}

void TileSpawner::onCreate()
{
	this->actorTransform = this->getOwner()->findComponent<Transform>();

	this->initGuiLayout();
}

const Component::IdType& TileSpawner::getName() const
{
	return NAME;
}

void TileSpawner::setTileSet(const TileSet *set)
{
	if(set != nullptr)
	{
		this->smartTileNames = set->getSmartTileNames();

		this->tileListLayout->listItems.clear();
		for(const auto& tileName : this->smartTileNames)
		{
			this->tileListLayout->listItems.push_back(tileName);
		}
	}
	else
	{
		this->smartTileNames.clear();
	}

	this->updateGuiLayout();
}

void TileSpawner::setTarget(const std::string& mapName)
{
	this->target = mapName;
}

void TileSpawner::onComponentEvent(const std::shared_ptr<Event>& event)
{
	if(event->isType(TransformChange::ID)) {
		this->updateGuiLayout();
	}
}

void TileSpawner::onEvent(const std::shared_ptr<Event>& event)
{
	if (event->isType(MouseAction::HOLD))
	{
		if(this->currentEditorMode != this->editorModeValue)
		{
			return;
		}

		auto action = std::static_pointer_cast<MouseAction>(event);
		auto broadcaster = this->getLogicContext()->getEventBroadcaster();

		BrushPattern *pattern = nullptr;
		int brushSize = 1;

		auto brush = this->getOwner()->findComponent<Brush>();
		if(brush != nullptr)
		{
			pattern = brush->getPattern();
			brushSize = brush->getSize();
		}

		glm::vec2 position = this->actorTransform->getPosition() + this->offset;

		// Send quickbar tile index if left-clicked, or -1 if right-clicked
		if(action->isLeft())
		{
			broadcaster->queueEvent(std::make_shared<TilePlacement>(this->getOwner(), this->target, position, this->quickbarTileIndex, pattern, brushSize));
		}
		else if (action->isRight())
		{
			broadcaster->queueEvent(std::make_shared<TilePlacement>(this->getOwner(), this->target, position, -1, pattern, brushSize));
		}
	}
	else if(event->getType() == ModeChange::ID) {
		auto modeChange = std::static_pointer_cast<ModeChange>(event);

		if(this->currentEditorMode == this->editorModeValue && modeChange->getName() == this->quickbarModeName)
		{
			this->quickbarTileIndex = modeChange->getValue();

			this->updateGuiLayout();
		}
		else if (modeChange->getName() == "editorMode")
		{
			this->currentEditorMode = modeChange->getValue();

			if(this->currentEditorMode == this->editorModeValue)
			{
				auto brush = this->getOwner()->findComponent<Brush>();
				if(brush != nullptr)
				{
					brush->setResizable(true);
				}

				this->updateGuiLayout();
			}
		}
	}
	else if(event->isType(GuiValueChange::ID))
	{
		auto valueChange = static_cast<GuiValueChange*>(event.get());

		if(valueChange->getActor() == this->getOwner())
		{
			if(valueChange->getKey() == "tileList")
			{
				this->quickbarTileIndex = valueChange->getValue().asInt();
			}

			this->updateGuiLayout();
		}
	}
	else if(event->getType() == TileSetResponse::ID)
	{
		auto responseEvent = static_cast<TileSetResponse*>(event.get());

		if(responseEvent->getTarget() == this->target) {
			this->setTileSet(responseEvent->getTileSet());
		}
	}
}

void TileSpawner::initGuiLayout()
{
	auto brush = this->getOwner()->findComponent<Brush>();
	auto patternManager = BrushPatternManager::getInstance();

	this->brushSizeLayout = std::make_shared<ElementLayout>("brushSize", "Brush Size", ElementType::NUMBER_INT, brush->getSize(), 1, brush->getMaxSize());
	this->brushPatternLayout = std::make_shared<ElementLayout>("brushPattern", "Brush Pattern", ElementType::NUMBER_INT, 0, 0, patternManager->getPatternCount() - 1);
	this->tileListLayout = std::make_shared<ElementLayout>("tileList", "Tiles", ElementType::LIST_STRING);

	this->layouts.push_back(this->brushSizeLayout);
	this->layouts.push_back(this->brushPatternLayout);
	this->layouts.push_back(this->tileListLayout);

	this->updateGuiLayout();
}

void TileSpawner::updateGuiLayout()
{
	auto brush = this->getOwner()->findComponent<Brush>();
	auto patternManager = BrushPatternManager::getInstance();

	// The simple ones
	this->brushSizeLayout->value = brush->getSize();
	this->tileListLayout->value = this->quickbarTileIndex;
	this->tileListLayout->visible = (tileListLayout->listItems.size() > 0);

	// Brush Pattern
	for(int i = 0; i < patternManager->getPatternCount(); ++i)
	{
		if(patternManager->getPattern(i) == brush->getPattern())
		{
			this->brushPatternLayout->value = i;
			break;
		}
	}

	if(this->currentEditorMode == this->editorModeValue)
	{
		auto event = std::make_shared<GuiLayoutNotification>(this->getOwner(), "CursorOptions", this->layouts);
		this->getLogicContext()->getEventBroadcaster()->queueEvent(event);
	}
}

} }
