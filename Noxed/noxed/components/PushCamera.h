/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#pragma once

#include <nox/logic/actor/Component.h>
#include <nox/logic/event/Event.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/app/log/Logger.h>
#include <glm/vec2.hpp>

namespace nox
{
	namespace logic
	{
		namespace actor
		{
			class Transform;
		}
		namespace world
		{
			class Manager;
		}
	}
}

namespace noxed { namespace components
{

/**
 * This is a more independent camera that is meant to move manually and/or be
 * pushed by the object it is attached to. This component do not maintain the
 * camera, it just controls it.
 *
 * # JSON Description
 * ## Name
 * %Noxed::PushCamera
 *
 * ## Properties
 * - speed:float - World units to move per second
 * - movable:bool - Move upon receiving "move" Action events.
 * - pushable:bool - Push the camera when mouse is near the edges.
 * - edgeSize:float - Amount, as factor of screen height, to consider the edge.
 */
class PushCamera final: public nox::logic::actor::Component, public nox::logic::event::IListener
{
public:
	const static IdType NAME;

	PushCamera();
	virtual ~PushCamera();

	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;
	void onCreate() override;
	void onUpdate(const nox::Duration& deltaTime) override;
	const IdType& getName() const override;

	/**
	 * If not pushable, make it pushable. This function does nothing if it already is, and can not be
	 * undone.
	 */
	void makePushable();

	/**
	 * If not movable, make it movable. This function does nothing if it already is, and can not be
	 * undone.
	 */
	void makeMovable();

	virtual void onComponentEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;
	virtual void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;

private:

	nox::logic::actor::Transform* actorTransform;

	nox::logic::event::ListenerManager listener;
	nox::app::log::Logger log;
	std::string moveLockMode;
	bool moveLock;

	float edgeSize;
	float speed;
	glm::vec2 position;
	glm::vec2 mouseMovement;
	glm::vec2 keyboardMovement;
	bool movable;
	bool pushable;
};

} }
