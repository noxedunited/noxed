/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "TileMap.h"

#include <math.h>
#include <vector>

#include <nox/app/log/Logger.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/IContext.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/control/actor/ActorControl.h>
#include <nox/util/json_utils.h>
#include <nox/logic/actor/event/TransformChange.h>

#include <glm/gtc/matrix_transform.hpp>

#include <noxed/NoxedWindowView.h>
#include <noxed/brush/BrushPattern.h>
#include <noxed/components/EditorBlueprint.h>
#include <noxed/gui/ElementType.h>
#include <noxed/events/TileSetRequest.h>
#include <noxed/events/TileSetResponse.h>
#include <noxed/events/TilePlacement.h>
#include <noxed/events/GuiLayoutNotification.h>
#include <noxed/events/GuiValueChange.h>
#include <noxed/events/EntitySelection.h>
#include <noxed/tileset/TileSet.h>

using nox::app::graphics::SceneGraphNode;
using nox::app::graphics::TransformationNode;
using nox::logic::actor::Component;
using nox::logic::actor::Transform;
using nox::logic::event::Event;
using nox::logic::graphics::SceneNodeEdited;
using nox::util::writeJsonVec;
using nox::util::parseJsonVec;
using nox::logic::actor::TransformChange;
using noxed::brush::BrushPattern;
using noxed::components::EditorBlueprint;
using noxed::gui::ElementLayout;
using noxed::gui::ElementType;
using noxed::events::TileSetRequest;
using noxed::events::TileSetResponse;
using noxed::events::TilePlacement;
using noxed::events::GuiLayoutNotification;
using noxed::events::GuiValueChange;
using noxed::events::EntitySelection;
using noxed::graphics::TileBlockRenderNode;
using noxed::tileset::TileSet;
using noxed::tileset::TileGrid;

namespace noxed { namespace components
{

const Component::IdType TileMap::NAME = "Noxed::TileMap";

TileMap::TileMap():
	listener("TileMap")
{

}

TileMap::~TileMap() = default;

bool TileMap::initialize(const Json::Value& componentJsonObject)
{
	this->log = this->getLogicContext()->createLogger();
	this->log.setName("Component/TileMap");

	this->rootTransformNode = std::make_shared<TransformationNode>();

	this->offset = parseJsonVec(componentJsonObject["offset"], glm::vec2(0.0f, 0.0f));
	this->name = componentJsonObject.get("name", "default").asString();
	this->smart = componentJsonObject.get("smart", false).asBool();
	this->tilesetName = componentJsonObject.get("tileset", "").asString();
	this->dumbDown = componentJsonObject.get("dumbDown", false).asBool();
	this->renderLevel = componentJsonObject.get("renderLevel", 100).asInt();
	this->editorMap = componentJsonObject.get("editorMap", false).asBool();
	int blockWidth = componentJsonObject.get("blockWidth", 64).asInt();

	this->grid = std::make_unique<TileGrid>(blockWidth);

	if(this->smart)
	{
		this->smartGrid = std::make_unique<TileGrid>(blockWidth);
		this->smartGrid->deserialize(componentJsonObject);
	}
	else
	{
		this->grid->deserialize(componentJsonObject);
	}

	this->listener.setup(this, this->getLogicContext()->getEventBroadcaster());
	this->listener.addEventTypeToListenFor(TileSetResponse::ID);
	this->listener.addEventTypeToListenFor(TilePlacement::ID);
	if(this->editorMap)
	{
		this->listener.addEventTypeToListenFor(EntitySelection::ID);
		this->listener.addEventTypeToListenFor(GuiValueChange::ID);
	}
	this->listener.startListening();

	if(this->tilesetName != "")
	{
		auto broadcaster = this->getLogicContext()->getEventBroadcaster();
		broadcaster->queueEvent(std::make_shared<TileSetRequest>(this->getOwner(), this->tilesetName, this->name));
	}

	this->tileset = nullptr;

	this->broadcastSceneNodeCreation(this->rootTransformNode);

	return true;
}

void TileMap::serialize(Json::Value& componentObject)
{
	componentObject["offset"] = writeJsonVec(this->offset);
	componentObject["name"] = this->name;
	componentObject["smart"] = (this->smart && !this->dumbDown);
	componentObject["renderLevel"] = this->renderLevel;
	componentObject["editorMap"] = false;

	if(this->tileset != nullptr)
	{
		componentObject["tileset"] = this->tileset->getName();
	}
	else
	{
		componentObject["tileset"] = this->tilesetName;
	}

	if(this->smart && !this->dumbDown)
	{
		this->smartGrid->serialize(componentObject);
	}
	else
	{
		this->grid->serialize(componentObject);
	}
}

void TileMap::onCreate()
{
	this->actorTransform = this->getOwner()->findComponent<Transform>();
	this->actorBlueprint = this->getOwner()->findComponent<EditorBlueprint>();

	this->broadcastSceneNodeCreation(this->rootTransformNode);
	this->updateActorTransform();
	this->initGuiLayout();
}

void TileMap::onDestroy()
{
}

void TileMap::onDeactivate()
{
	if (this->enabled == true)
	{
		this->broadcastSceneNodeRemoval(this->rootTransformNode);
	}
}

void TileMap::onActivate()
{
	if (!this->enabled == true)
	{
		this->broadcastSceneNodeCreation(this->rootTransformNode);
	}
}

const Component::IdType& TileMap::getName() const
{
	return NAME;
}

void TileMap::onComponentEvent(const std::shared_ptr<Event>& event)
{
	if (event->isType(TransformChange::ID))
	{
		this->updateActorTransform();
	}
}

void TileMap::onEvent(const std::shared_ptr<Event>& event)
{
	if(event->isType(TileSetResponse::ID) && this->renderNodes.size() == 0)
	{
		auto responseEvent = static_cast<TileSetResponse*>(event.get());
		auto set = responseEvent->getTileSet();

		if(set != nullptr && responseEvent->getName() == this->tilesetName)
		{
			this->setTileSet(set);
		}
	}
	else if(event->isType(TilePlacement::ID))
	{
		auto placement = static_cast<TilePlacement*>(event.get());

		if(placement->getName() == this->name)
		{
			const glm::vec2 position = placement->getPosition() - glm::vec2(std::ceil(this->offset.x), std::ceil(this->offset.y)) - this->actorTransform->getPosition();
			const int value = placement->getValue();
			const glm::ivec2 tileCoords = glm::ivec2(position);

			const auto brushPattern = placement->getBrushPattern();
			const int brushSize = placement->getBrushSize();

			if(brushPattern == nullptr || brushSize <= 1)
			{
				this->setTile(tileCoords.x, tileCoords.y, value, true, true);
			}
			else
			{
				std::vector<glm::ivec2> core = brushPattern->getCore(brushSize);
				std::vector<glm::ivec2> edge = brushPattern->getEdge(brushSize);

				// Set the tiles
				for(glm::ivec2 brushCoords : core)
				{
					this->setTile(tileCoords.x + brushCoords.x, tileCoords.y + brushCoords.y, value, false, false);
				}
				for(glm::ivec2 brushCoords : edge)
				{
					this->setTile(tileCoords.x + brushCoords.x, tileCoords.y + brushCoords.y, value, true, false);
				}

				// Resolve the tiles when all is set to avoid errors when the tile
				// resolves incorrectly because they neighbor isn't there yet.
				for(glm::ivec2 brushCoords : core)
				{
					this->resolveTile(tileCoords.x + brushCoords.x, tileCoords.y + brushCoords.y, false);
				}
				for(glm::ivec2 brushCoords : edge)
				{
					this->resolveTile(tileCoords.x + brushCoords.x, tileCoords.y + brushCoords.y, true);
				}
			}
		}
	}
	else if(event->isType(EntitySelection::ID))
	{
		this->updateGuiLayout();
	}
	else if(event->isType(GuiValueChange::ID))
	{
		auto valueChange = static_cast<GuiValueChange*>(event.get());

		if(valueChange->getActor() == this->getOwner())
		{
			std::string key = valueChange->getKey();

			if(key == "offset")
			{
				this->offset = parseJsonVec(valueChange->getValue(), glm::vec2(0.0f, 0.0f));
				this->updateActorTransform();
			}
			else if(key == "dumbDown")
			{
				this->dumbDown = valueChange->getValue().asBool();
			}

			this->updateGuiLayout();
		}
	}
}

const noxed::tileset::TileSet *TileMap::getTileSet() const
{
	return this->tileset;
}

const noxed::tileset::TileGrid *TileMap::getSmartGrid() const
{
	return this->smartGrid.get();
}

const noxed::tileset::TileGrid *TileMap::getGrid() const
{
	return this->grid.get();
}

const std::string TileMap::getMapName() const
{
	return this->name;
}

void TileMap::setMapName(const std::string& name)
{
	this->name = name;
}

const std::string TileMap::getTileSetName() const
{
	return this->tilesetName;
}

const glm::vec2 TileMap::getOffset() const
{
	return this->offset;
}

void TileMap::setTileSet(TileSet *set)
{
	this->tileset = set;
	this->tilesetName = set->getName();

	if(this->smart)
	{
		this->resolveAllSmartTiles();
	}

	if(this->renderNodes.size() > 0)
	{
		for(auto& renderNode : this->renderNodes)
		{
			this->rootTransformNode->removeChild(renderNode);
		}

		this->renderNodes.clear();
	}

	const int length = this->grid->getBlockCount();
	for(int i = 0; i < length; ++i)
	{
		this->addBlock(this->grid->getBlock(i));
	}
}

void TileMap::addBlock(TileGrid::Block *block)
{
	auto renderNode = std::make_shared<TileBlockRenderNode>(this->tileset, block);
	renderNode->setRenderLevel(this->renderLevel);

	this->rootTransformNode->addChild(renderNode);
	this->renderNodes.push_back(renderNode);
}

void TileMap::updateActorTransform()
{
	const glm::vec2& position = this->actorTransform->getPosition();
	const glm::vec2& scale = this->actorTransform->getScale();
	const float rotation = this->actorTransform->getRotation();

	glm::mat4 transformMatrix;
	transformMatrix = glm::translate(transformMatrix, glm::vec3(this->offset, 0.0f));
	transformMatrix = glm::translate(transformMatrix, glm::vec3(position, 0.0f));
	transformMatrix = glm::rotate(transformMatrix, rotation, glm::vec3(0.0f, 0.0f, 1.0f));
	transformMatrix = glm::scale(transformMatrix, glm::vec3(scale, 1.0f));

	this->rootTransformNode->setTransform(transformMatrix);
}

void TileMap::broadcastSceneNodeCreation(std::shared_ptr<SceneGraphNode> node)
{
	this->enabled = true;

	auto sceneNodeAddEvent = std::make_shared<SceneNodeEdited>(node, SceneNodeEdited::Action::CREATE);
	this->getLogicContext()->getEventBroadcaster()->queueEvent(sceneNodeAddEvent);
}

void TileMap::broadcastSceneNodeRemoval(std::shared_ptr<SceneGraphNode> node)
{
	this->enabled = false;

	auto sceneNodeRemoveEvent = std::make_shared<SceneNodeEdited>(node, SceneNodeEdited::Action::REMOVE);
	this->getLogicContext()->getEventBroadcaster()->queueEvent(sceneNodeRemoveEvent);
}

void TileMap::syncBlockChange(const int blockId)
{
	if(blockId >= this->renderNodes.size())
	{
		this->addBlock(this->grid->getBlock(blockId));
	}
	else
	{
		this->renderNodes[blockId]->markAsEdited();
	}
}

void TileMap::setTile(const int tileX, const int tileY, const int value, const bool resolveNeighbors, const bool resolveSelf)
{
	// Handle smart tiles differently
	if(this->smart && this->tileset != nullptr)
	{
		// If value is out of the upper bound, reset it to a deleted tile
		this->smartGrid->setTile(tileX, tileY, value);

		if(resolveSelf)
		{
			this->resolveTile(tileX, tileY, resolveNeighbors);
		}
	}
	else
	{
		this->syncBlockChange(this->grid->setTile(tileX, tileY, value));
	}
}

void TileMap::resolveTile(const int tileX, const int tileY, const bool resolveNeighbors)
{
	const int value = this->smartGrid->getTile(tileX, tileY);
	int block;

	// If it's not a deletion, resolve the smart tile
	if(value > -1 && value < this->tileset->getSmartTileCount())
	{
		auto smartTile = this->tileset->getSmartTile(value);
		block = this->grid->setTile(tileX, tileY, smartTile->resolve(this->smartGrid.get(), this->tileset, tileX, tileY, value));
		this->syncBlockChange(block);
	}
	else
	{
		block = this->grid->setTile(tileX, tileY, -1);
		this->syncBlockChange(block);
	}

	// Re-resolve neighbors in case they're affected
	if(resolveNeighbors)
	{
		for(int i = -1; i <= 1; ++i)
		{
			for(int j = -1; j <= 1; ++j)
			{
				// On any combination of offsets aside from 0,0:
				if(j == 0 && i == 0)
				{
					continue;
				}

				// Get neighbor coordinates
				int x = tileX + i;
				int y = tileY + j;

				// Get the smart tile, but continue if it's out of bounds
				int stId = this->smartGrid->getTile(x, y);
				if(stId < 0 || stId >= this->tileset->getSmartTileCount())
				{
					continue;
				}

				// Get the smart tile
				auto smartTile = this->tileset->getSmartTile(stId);

				// Read the current and get the next.
				int currentTile = this->grid->getTile(x, y);
				int newTile = smartTile->resolve(this->smartGrid.get(), this->tileset, x, y, stId);

				// If the change means the smart tile looks different:
				if(currentTile != newTile)
				{
					// Change the tile and mark the block for re-render
					// The marking itself is a cheap operation, so no worries about
					// doing it over and over for the same block.
					this->syncBlockChange(this->grid->setTile(x, y, newTile));
				}
			}
		}
	}
}

void TileMap::resolveAllSmartTiles()
{
	assert(this->smart);

	int length = this->smartGrid->getBlockCount();

	for(int i = 0; i < length; ++i)
	{
		auto block = this->smartGrid->getBlock(i);
		int blockSize = block->blockWidth * block->blockWidth;

		for(int j = 0; j < blockSize; ++j)
		{
			int x = (block->x * block->blockWidth) + (j % block->blockWidth);
			int y = (block->y * block->blockWidth) + (j / block->blockWidth);
			int index = block->tiles[j];

			if(index >= 0 && index < this->tileset->getSmartTileCount())
			{
				auto smartTile = this->tileset->getSmartTile(index);

				this->grid->setTile(x, y, smartTile->resolve(this->smartGrid.get(), this->tileset, x, y, index));
			}
		}
	}
}

void TileMap::initGuiLayout()
{
	if(this->actorBlueprint != nullptr)
	{
		this->headerLayout = std::make_shared<ElementLayout>("", this->name, ElementType::LAYOUT_HEADER);
		this->nameLayout = std::make_shared<ElementLayout>("setName", "Tile Set", ElementType::OUTPUT_STRING);
		this->offsetLayout = std::make_shared<ElementLayout>("offset", "Tile Offset", ElementType::VEC2_FLOAT, Json::Value());
		this->dumbDownLayout = std::make_shared<ElementLayout>("dumbDown", "Dumb Down (save only resolved tiles)", ElementType::BOOLEAN);

		this->layouts.push_back(this->headerLayout);
		this->layouts.push_back(this->nameLayout);
		this->layouts.push_back(this->offsetLayout);
		this->layouts.push_back(this->dumbDownLayout);

		this->updateGuiLayout();
	}
}

void TileMap::updateGuiLayout()
{
	if(this->actorBlueprint != nullptr && this->actorBlueprint->isSelected())
	{
		if(this->tileset != nullptr)
		{
			this->nameLayout->value = this->tileset->getName();
		}
		else
		{
			this->nameLayout->value = "(null)";
		}

		this->offsetLayout->value["x"] = this->offset.x;
		this->offsetLayout->value["y"] = this->offset.y;
		this->dumbDownLayout->value = this->dumbDown;

		auto event = std::make_shared<GuiLayoutNotification>(this->getOwner(), "TileMapOptions", this->layouts);
		this->getLogicContext()->getEventBroadcaster()->queueEvent(event);
	}
}

} }
