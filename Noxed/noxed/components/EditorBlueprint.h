/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#pragma once

#include <nox/logic/actor/Component.h>
#include <nox/logic/event/Event.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/app/log/Logger.h>
#include <glm/vec2.hpp>

namespace nox
{
	namespace logic
	{
		namespace actor
		{
			class Transform;
		}
		namespace world
		{
			class Manager;
		}
	}
}

namespace noxed
{
	namespace expose
	{
		class ExposedComponent;
		class ExposedArgument;
	}
	namespace gui
	{
		struct ElementLayout;
	}
}

namespace noxed { namespace components
{

/**
 * A blueprint that will create an actor in the exported world.
 *
 * # JSON Description
 * ## Name
 * %Noxed::EditorBlueprint
 *
 * ## Properties
 * - __extends__:string - The "extend" field on the exported actor.
 * - __controlled__:bool - If true, this will be marked as the controlled actor upon exporting.
 * - __occupant__:bool - If true, this actor cannot be overlapped and it can be deleted.
 * - __components__:object - Components
 *
 * Components are added to the blueprint similarly to how they're added to actors, but the layout inside
 * them differs. First you have to choose whether the component definition is based on the editor
 * representation's component, a JSON object, or nothing. The former is useful for editable components
 * like the tile map, as well as component that you want to base on the editor component.
 * "base" is an option if you don't use the editor component, and as its name implies, it will serve
 * as the base for the arguments to write upon.
 *
 *
 */
class EditorBlueprint final: public nox::logic::actor::Component, public nox::logic::event::IListener
{
public:
	const static IdType NAME;

	EditorBlueprint();
	virtual ~EditorBlueprint();

	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;
	void onCreate() override;
	const IdType& getName() const override;

	/**
	 * Export the JSON data for world saving.
	 *
	 * @param actorRoot The root of the actor object
	 */
	void exportJson(Json::Value& actorRoot) const;

	/**
	 * Get whether this blueprint is marked as controlled
	 */
	const bool isControlled() const;

	/**
	 * Get whether it is an occupant of grid space
	 */
	const bool canOccupyGrid() const;

	/**
	 * Get whether it is selected
	 */
	const bool isSelected() const;

	void updateGuiLayout();

	virtual void onComponentEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;
	virtual void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;

private:
	void initGuiLayout();

	nox::logic::actor::Transform* actorTransform;

	nox::logic::event::ListenerManager listener;
	nox::app::log::Logger log;

	std::string extends;
	bool controlled;
	bool occupant;
	Json::Value components;

	bool selected;

	std::vector<std::unique_ptr<noxed::expose::ExposedComponent>> exposedComponents;
	std::vector<std::shared_ptr<noxed::gui::ElementLayout>> layouts;
	std::map<std::string, noxed::expose::ExposedArgument*> exposedArgumentMap;
	std::vector<std::shared_ptr<noxed::gui::ElementLayout>> emptyLayouts;
};

} }
