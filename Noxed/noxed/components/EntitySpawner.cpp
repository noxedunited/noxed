/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "EntitySpawner.h"

#include <math.h>
#include <vector>

#include <boost/lexical_cast.hpp>
#include <nox/app/log/Logger.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/IContext.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/control/event/Action.h>
#include <nox/logic/control/actor/ActorControl.h>
#include <nox/util/json_utils.h>
#include <glm/gtc/matrix_transform.hpp>
#include <nox/app/resource/Handle.h>
#include <nox/app/resource/IResourceAccess.h>

#include <noxed/NoxedWindowView.h>
#include <noxed/components/EditorBlueprint.h>
#include <noxed/components/Brush.h>
#include <noxed/events/MouseAction.h>
#include <noxed/events/ModeChange.h>
#include <noxed/events/GuiLayoutNotification.h>
#include <noxed/events/GuiValueChange.h>
#include <noxed/events/EntitySelection.h>
#include <noxed/gui/ElementLayout.h>
#include <noxed/gui/ElementType.h>

using nox::logic::actor::Actor;
using nox::logic::actor::Component;
using nox::logic::actor::Transform;
using nox::logic::actor::TransformChange;
using nox::logic::event::Event;
using nox::util::writeJsonVec;
using nox::util::parseJsonVec;
using noxed::events::MouseAction;
using noxed::events::ModeChange;
using noxed::events::GuiLayoutNotification;
using noxed::events::GuiValueChange;
using noxed::events::EntitySelection;
using noxed::gui::ElementLayout;
using noxed::gui::ElementType;

namespace noxed { namespace components
{

const Component::IdType EntitySpawner::NAME = "Noxed::EntitySpawner";

EntitySpawner::EntitySpawner():
	listener("EntitySpawner")
{

}

EntitySpawner::~EntitySpawner() = default;

bool EntitySpawner::initialize(const Json::Value& componentJsonObject)
{
	this->offset = parseJsonVec(componentJsonObject["offset"], glm::vec2(0.0f, 0.0f));
	this->editorModeValue = componentJsonObject.get("editorMode", 0).asInt();
	this->currentEditorMode = 0;

	// If there's quick bar paremters provided, use them.
	const auto& qb = componentJsonObject["quickbar"];
	if(!qb.isNull()) {
		this->quickbarModeName = qb.get("modeName", "").asString();
		this->quickbarActorIndex = 0;

		for(const auto actorName : qb["defaults"])
		{
			this->quickbarActorNames.push_back(actorName.asString());
		}
	}
	else
	{
		this->quickbarModeName = "";
	}

	this->listener.setup(this, this->getLogicContext()->getEventBroadcaster());
	this->listener.addEventTypeToListenFor(MouseAction::BUTTON);
	this->listener.addEventTypeToListenFor(ModeChange::ID);
	this->listener.addEventTypeToListenFor(GuiValueChange::ID);
	this->listener.startListening();

	this->log = this->getLogicContext()->createLogger();
	this->log.setName("Component/EntitySpawner");

	this->worldManager = this->getLogicContext()->getWorldManager();

	this->selectedActor = nullptr;

	return true;
}

void EntitySpawner::serialize(Json::Value& componentObject)
{
	componentObject["offset"] = writeJsonVec(this->offset);
}

void EntitySpawner::onCreate()
{
	this->actorTransform = this->getOwner()->findComponent<Transform>();

	this->initGuiLayout();
}

const Component::IdType& EntitySpawner::getName() const
{
	return NAME;
}

void EntitySpawner::onComponentEvent(const std::shared_ptr<Event>& event)
{
	if(event->isType(TransformChange::ID)) {
		this->updateGuiLayout();
	}
}

void EntitySpawner::onEvent(const std::shared_ptr<Event>& event)
{
	if (event->isType(MouseAction::BUTTON))
	{
		if(this->currentEditorMode != this->editorModeValue)
		{
			return;
		}

		auto action = std::static_pointer_cast<MouseAction>(event);

		if(action->isPress() && action->isLeft())
		{
			glm::vec2 position = this->actorTransform->getPosition();

			auto gridOccupants = this->worldManager->findActorsWithinRange(position, 0.25);
			Actor *occupant = nullptr;

			if(gridOccupants.size() > 1) {
				for(int i = 0; i < gridOccupants.size(); ++i) {
					if(gridOccupants[i] != this->getOwner()) {
						auto component = gridOccupants[i]->findComponent<EditorBlueprint>();

						if(component != nullptr)
						{
							if(component->canOccupyGrid())
							{
								occupant = gridOccupants[i];
								break;
							}
						}
					}
				}
			}

			if(occupant == nullptr)
			{
				if(this->quickbarActorIndex < this->quickbarActorNames.size())
				{
					const std::string actorName = this->quickbarActorNames[this->quickbarActorIndex];

					auto instance = this->worldManager->createActorFromDefinitionName(actorName);

					auto instanceTransform = instance->findComponent<Transform>();
					instanceTransform->setPosition(position);

					std::string childName = actorName + boost::lexical_cast<std::string>	(instance->getId().getValue());

					this->selectActor(instance.get());

					this->getOwner()->attachChildActor(std::move(instance), childName);
					this->getOwner()->findChildActor(childName)->activate();
				}
				else
				{
					this->log.info().format("No quick bar item at position %i", this->quickbarActorIndex);
				}
			}
			else
			{
				this->log.info().format("Grid is occuped by %s (%i)!", occupant->getName(), occupant->getId());
				this->selectActor(occupant);
			}
		}
		else if (action->isPress() && action->isRight())
		{
			glm::vec2 position = this->actorTransform->getPosition();
			auto gridOccupants = this->worldManager->findActorsWithinRange(position, 0.25);

			if(gridOccupants.size() > 1) {
				Actor *occupant = nullptr;

				for(int i = 0; i < gridOccupants.size(); ++i) {
					if(gridOccupants[i] != this->getOwner()) {
						auto component = gridOccupants[i]->findComponent<EditorBlueprint>();

						if(component != nullptr)
						{
							if(component->canOccupyGrid())
							{
								occupant = gridOccupants[i];
								break;
							}
						}
					}
				}

				if(occupant != nullptr) {
					this->log.info().format("Grid is occuped by %s (%i)!", occupant->getName(), occupant->getId());

					auto children = this->getOwner()->findChildrenRecursively();
					for(auto child : children)
					{
						if(child.actor == occupant)
						{
							auto actor = this->getOwner()->detachChildActor(child.name);
							actor->deactivate();
							actor->destroy();

							if(this->selectedActor == actor.get())
							{
								this->selectActor(nullptr);
							}
						}
					}
				}
			}
		}
	}
	else if(event->getType() == ModeChange::ID) {
		auto modeChange = std::static_pointer_cast<ModeChange>(event);

		this->log.info().format("Mode: %s = %i", modeChange->getName(), modeChange->getValue());

		if(this->currentEditorMode == this->editorModeValue && modeChange->getName() == this->quickbarModeName)
		{
			this->quickbarActorIndex = modeChange->getValue();
		}
		else if (modeChange->getName() == "editorMode")
		{
			this->currentEditorMode = modeChange->getValue();

			if(this->currentEditorMode == this->editorModeValue)
			{
				auto brush = this->getOwner()->findComponent<Brush>();
				if(brush != nullptr)
				{
					brush->setResizable(false);
				}
			}
		}

		this->updateGuiLayout();
	}
	else if(event->isType(GuiValueChange::ID))
	{
		auto valueChange = static_cast<GuiValueChange*>(event.get());

		if(valueChange->getActor() == this->getOwner())
		{
			if(valueChange->getKey() == "entityIndex")
			{
				this->quickbarActorIndex = valueChange->getValue().asInt();
			}

			this->updateGuiLayout();
		}
	}
}

void EntitySpawner::initGuiLayout()
{
	this->entitySelectLayout = std::make_shared<ElementLayout>("entityIndex", "Entity Index", ElementType::NUMBER_INT, 0, 0, static_cast<int>(this->quickbarActorNames.size() - 1U));
	this->entityNameLayout = std::make_shared<ElementLayout>("centerPosition", "Entity Name", ElementType::OUTPUT_STRING, "");

	this->layouts.push_back(this->entitySelectLayout);
	this->layouts.push_back(this->entityNameLayout);

	this->updateGuiLayout();
}

void EntitySpawner::updateGuiLayout()
{
	this->entitySelectLayout->value = this->quickbarActorIndex;

	if(this->quickbarActorIndex < this->quickbarActorNames.size())
	{
		this->entityNameLayout->value = this->quickbarActorNames[this->quickbarActorIndex];
	}
	else
	{
		this->entityNameLayout->value = "(none)";
	}

	if(this->currentEditorMode == this->editorModeValue)
	{
		auto event = std::make_shared<GuiLayoutNotification>(this->getOwner(), "CursorOptions", this->layouts);
		this->getLogicContext()->getEventBroadcaster()->queueEvent(event);
	}
}

void EntitySpawner::selectActor(nox::logic::actor::Actor* actor)
{
	if(this->selectedActor != actor)
	{
		auto event = std::make_shared<EntitySelection>(actor);
		this->getLogicContext()->getEventBroadcaster()->queueEvent(event);

		this->selectedActor = actor;

		if(this->selectedActor == nullptr)
		{
			auto event = std::make_shared<GuiLayoutNotification>(this->getOwner(), "EditorBlueprint", this->emptyLayoutArray);
			this->getLogicContext()->getEventBroadcaster()->queueEvent(event);
		}
	}
}

} }
