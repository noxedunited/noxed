/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
#pragma once

#include <vector>

#include <nox/logic/actor/Component.h>
#include <nox/logic/graphics/actor/ActorGraphics.h>
#include <nox/logic/graphics/event/SceneNodeEdited.h>
#include <nox/logic/event/Event.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/app/log/Logger.h>
#include <nox/app/graphics/2d/TransformationNode.h>
#include <nox/app/graphics/2d/SceneGraphNode.h>

#include <glm/vec2.hpp>

#include <noxed/brush/BrushPattern.h>
#include <noxed/graphics/BrushSpriteRenderNode.h>

namespace nox
{
	namespace logic
	{
		namespace actor
		{
			class Transform;
		}
	}
}

namespace noxed { namespace components
{

/**
 * Displays a brush at the actor's position that can be used to select multiple positions.
 *
 * # JSON Description
 * ## Name
 * %Noxed::Brush
 *
 * ## Properties
 * - __offset__:vec2 - Offset from position to draw center point
 * - __spriteName__:string - Sprite name to use for one brush point
 * - __scale__:float - Sprite scale
 * - __renderLevel__:int - Render level of brush sprite
 * - __brush__:string - Brush pattern
 * - __size__:int - Initial brush pattern radius
 * - __maxSize__:int - Max brush pattern radius
 * - __sizeMode__:string - Key-bindable mode to use for resizing
 */
class Brush final: public nox::logic::actor::Component, public nox::logic::event::IListener
{
public:
	const static IdType NAME;

	Brush();
	virtual ~Brush();

	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;
	void onCreate() override;
	const IdType& getName() const override;

	virtual void onComponentEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;
	virtual void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;

	/**
	 * Get the brush pattern.
	 */
	noxed::brush::BrushPattern *getPattern() const;

	/**
	 * Get the brush size.
	 */
	const int getSize() const;

	/**
	 * Get the brush max size.
	 */
	const int getMaxSize() const;

	/**
	 * Get whether the brush is reziable
	 */
	const bool isResizable() const;

	/**
	 * Set whether the brush is reziable. It will still keep track of brush size,
	 * and will display the brush at the right size upon being set to false.
	 *
	 * @param resizable Value to set it to
	 */
	void setResizable(bool resizable);

private:
	/**
	 * Updates the actor transform node for the renderer
	 */
	void updateActorTransform();

	/**
	 * Tells the scene graph what our root node is, and that it and children
	 * should be rendered
	 *
	 * @param node The node to broadcast
	 */
	void broadcastSceneNodeCreation(std::shared_ptr<nox::app::graphics::SceneGraphNode> node);

	/**
	 * Tells the scene graph to hide this object's renderer
	 *
	 * @param node The node to broadcast
	 */
	void broadcastSceneNodeRemoval(std::shared_ptr<nox::app::graphics::SceneGraphNode> node);


	nox::logic::actor::Transform* actorTransform;
	noxed::brush::BrushPattern *pattern;

	nox::logic::event::ListenerManager listener;
	nox::app::log::Logger log;

	glm::vec2 offset;
	std::string spriteName;
	std::string brushName;
	std::string brushSizeMode;
	int brushSize;
	int maxSize;
	bool enabled;
	int renderLevel;
	float scale;
	bool resizable;

	std::shared_ptr<nox::app::graphics::TransformationNode> rootTransformNode;
	std::shared_ptr<nox::app::graphics::TransformationNode> renderTransformNode;
	std::shared_ptr<noxed::graphics::BrushSpriteRenderNode> renderNode;
};

} }
