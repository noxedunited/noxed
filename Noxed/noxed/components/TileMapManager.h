/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#pragma once

#include <nox/logic/actor/Component.h>
#include <nox/logic/event/Event.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/app/log/Logger.h>
#include <glm/vec2.hpp>

#include <string>
#include <vector>

namespace nox
{
	namespace logic
	{
		namespace actor
		{
			class Actor;
			class Transform;
		}
		namespace world
		{
			class Manager;
		}
	}
}
namespace noxed
{
	namespace gui
	{
		struct ElementLayout;
	}

	namespace tileset
	{
		class TileSetManager;
	}
}

namespace noxed { namespace components
{

/**
 * Spawns and manages tile map instances.
 *
 * # JSON Description
 * ## Name
 * %Noxed::TileMapManager
 *
 * ## Properties
 * - __spawnActor__:string - Actor id of tile map actor.
 */
class TileMapManager final: public nox::logic::actor::Component, public nox::logic::event::IListener
{
public:
	const static IdType NAME;

	TileMapManager();
	virtual ~TileMapManager();

	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;
	void onCreate() override;
	const IdType& getName() const override;

	virtual void onComponentEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;
	virtual void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;

	void updateGuiLayout();

private:
	void initGuiLayout();

	nox::logic::event::ListenerManager listener;
	nox::app::log::Logger log;
	std::vector<nox::logic::actor::Actor *> mapActors;
	noxed::tileset::TileSetManager* manager;
	nox::logic::world::Manager* worldManager;

	std::string spawnActor;

	int selectedMap;
	int selectedSet;
	int mapCounter;

	std::vector<std::shared_ptr<noxed::gui::ElementLayout>> layouts;
	std::shared_ptr<noxed::gui::ElementLayout> headerLayout;
	std::shared_ptr<noxed::gui::ElementLayout> mapListboxLayout;
	std::shared_ptr<noxed::gui::ElementLayout> setListboxLayout;
	std::shared_ptr<noxed::gui::ElementLayout> addButtonLayout;
	std::shared_ptr<noxed::gui::ElementLayout> removeButtonLayout;
	std::vector<std::shared_ptr<noxed::gui::ElementLayout>> emptyLayouts;
};

} }
