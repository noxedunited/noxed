/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "Brush.h"

#include <math.h>
#include <vector>

#include <nox/app/log/Logger.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/IContext.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/control/actor/ActorControl.h>
#include <nox/util/json_utils.h>
#include <nox/logic/actor/event/TransformChange.h>

#include <glm/gtc/matrix_transform.hpp>

#include <noxed/NoxedWindowView.h>
#include <noxed/events/ModeChange.h>
#include <noxed/events/GuiValueChange.h>
#include <noxed/brush/BrushPatternManager.h>
#include <noxed/graphics/BrushSpriteRenderNode.h>
#include <noxed/components/TileSpawner.h>

using nox::app::graphics::SceneGraphNode;
using nox::app::graphics::TransformationNode;
using nox::logic::actor::Component;
using nox::logic::actor::Transform;
using nox::logic::event::Event;
using nox::logic::graphics::SceneNodeEdited;
using nox::util::writeJsonVec;
using nox::util::parseJsonVec;
using nox::logic::actor::TransformChange;
using noxed::events::ModeChange;
using noxed::events::GuiValueChange;
using noxed::brush::BrushPattern;
using noxed::brush::BrushPatternManager;
using noxed::graphics::BrushSpriteRenderNode;
using noxed::components::TileSpawner;

namespace noxed { namespace components
{

const Component::IdType Brush::NAME = "Noxed::Brush";

Brush::Brush():
	listener("Brush")
{

}

Brush::~Brush() = default;

bool Brush::initialize(const Json::Value& componentJsonObject)
{
	this->log = this->getLogicContext()->createLogger();
	this->log.setName("Component/Brush");

	this->rootTransformNode = std::make_shared<TransformationNode>();

	this->offset = parseJsonVec(componentJsonObject["offset"], glm::vec2(0.0f, 0.0f));
	this->spriteName = componentJsonObject.get("spriteName", "default.png").asString();
	this->maxSize = componentJsonObject.get("maxSize", 5).asInt();
	this->brushName = componentJsonObject.get("brush", "square").asString();
	this->renderLevel = componentJsonObject.get("renderLevel", 50).asInt();
	this->scale = componentJsonObject.get("scale", 1.0).asFloat();
	this->brushSize = componentJsonObject.get("size", 1).asInt();
	this->brushSizeMode = componentJsonObject.get("sizeMode", "brushSize").asString();
	this->pattern = BrushPatternManager::getInstance()->getPattern(this->brushName);

	this->listener.setup(this, this->getLogicContext()->getEventBroadcaster());
	this->listener.addEventTypeToListenFor(ModeChange::ID);
	this->listener.addEventTypeToListenFor(GuiValueChange::ID);
	this->listener.startListening();

	this->enabled = true;

	this->broadcastSceneNodeCreation(this->rootTransformNode);

	this->renderNode = std::make_shared<BrushSpriteRenderNode>(this->spriteName, this->brushSize, this->maxSize);
	this->renderNode->setBrushPattern(this->pattern);
	this->renderNode->setRenderLevel(this->renderLevel);
	this->renderNode->setScale(this->scale);
	this->rootTransformNode->addChild(this->renderNode);

	if(this->resizable)
	{
		this->renderNode->setBrushSize(this->brushSize);
	}
	else
	{
		this->renderNode->setBrushSize(1);
	}

	return true;
}

void Brush::serialize(Json::Value& componentObject)
{
	componentObject["offset"] = writeJsonVec(this->offset);
	componentObject["sprite"] = this->spriteName;
	componentObject["maxSize"] = this->maxSize;
	componentObject["brush"] = this->brushName;
	componentObject["renderLevel"] = this->renderLevel;
	componentObject["scale"] = this->scale;
	componentObject["size"] = this->brushSize;
	componentObject["sizeMode"] = this->brushSizeMode;
	componentObject["resizable"] = this->resizable;
}

void Brush::onCreate()
{
	this->actorTransform = this->getOwner()->findComponent<Transform>();
	this->broadcastSceneNodeCreation(this->rootTransformNode);
	this->updateActorTransform();
}

const Component::IdType& Brush::getName() const
{
	return NAME;
}

void Brush::onComponentEvent(const std::shared_ptr<Event>& event)
{
	if (event->isType(TransformChange::ID))
	{
		this->updateActorTransform();
	}
}

void Brush::onEvent(const std::shared_ptr<Event>& event)
{
	if(event->isType(ModeChange::ID))
	{
		auto modeChange = static_cast<ModeChange*>(event.get());

		if(modeChange->getName() == brushSizeMode)
		{
			int value = modeChange->getValue();

			if(value >= 1 && value <= this->maxSize)
			{
				this->brushSize = value;

				if(this->resizable)
				{
					this->renderNode->setBrushSize(this->brushSize);
				}
				else
				{
					this->renderNode->setBrushSize(1);
				}

				// Affect the brush size slider
				this->getOwner()->findComponent<TileSpawner>()->updateGuiLayout();
			}
		}
	}
	else if(event->isType(GuiValueChange::ID))
	{
		auto valueChange = static_cast<GuiValueChange*>(event.get());

		if(valueChange->getActor() == this->getOwner())
		{
			if(valueChange->getKey() == "brushSize")
			{
				this->brushSize = valueChange->getValue().asInt();

				if(this->resizable)
				{
					this->renderNode->setBrushSize(this->brushSize);
				}
				else
				{
					this->renderNode->setBrushSize(1);
				}
			}
			else if(valueChange->getKey() == "brushPattern")
			{
				this->pattern = BrushPatternManager::getInstance()->getPattern(valueChange->getValue().asInt());
				this->renderNode->setBrushPattern(this->pattern);
			}

			this->getOwner()->findComponent<TileSpawner>()->updateGuiLayout();
		}
	}
}

noxed::brush::BrushPattern *Brush::getPattern() const
{
	return this->pattern;
}

const int Brush::getSize() const
{
	return this->brushSize;
}

const int Brush::getMaxSize() const
{
	return this->maxSize;
}

const bool Brush::isResizable() const
{
	return this->resizable;
}

void Brush::setResizable(bool resizable)
{
	this->resizable = resizable;

	if(this->resizable)
	{
		this->renderNode->setBrushSize(this->brushSize);
	}
	else
	{
		this->renderNode->setBrushSize(1);
	}
}

void Brush::updateActorTransform()
{
	const glm::vec2& position = this->actorTransform->getPosition();
	const glm::vec2& scale = this->actorTransform->getScale();
	const float rotation = this->actorTransform->getRotation();

	glm::mat4 transformMatrix;
	transformMatrix = glm::translate(transformMatrix, glm::vec3(this->offset, 0.0f));
	transformMatrix = glm::translate(transformMatrix, glm::vec3(position, 0.0f));
	transformMatrix = glm::rotate(transformMatrix, rotation, glm::vec3(0.0f, 0.0f, 1.0f));
	transformMatrix = glm::scale(transformMatrix, glm::vec3(scale, 1.0f));

	this->rootTransformNode->setTransform(transformMatrix);
}

void Brush::broadcastSceneNodeCreation(std::shared_ptr<SceneGraphNode> node)
{
	auto sceneNodeAddEvent = std::make_shared<SceneNodeEdited>(node, SceneNodeEdited::Action::CREATE);
	this->getLogicContext()->getEventBroadcaster()->queueEvent(sceneNodeAddEvent);
}

void Brush::broadcastSceneNodeRemoval(std::shared_ptr<SceneGraphNode> node)
{
	auto sceneNodeRemoveEvent = std::make_shared<SceneNodeEdited>(node, SceneNodeEdited::Action::REMOVE);
	this->getLogicContext()->getEventBroadcaster()->queueEvent(sceneNodeRemoveEvent);
}

} }
