/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "NoxedWindowView.h"

#include <nox/app/graphics/2d/IRenderer.h>
#include <nox/app/graphics/2d/BackgroundGradient.h>
#include <nox/app/IContext.h>
#include <nox/app/resource/Descriptor.h>
#include <nox/logic/IContext.h>
#include <nox/logic/graphics/event/DebugRenderingEnabled.h>
#include <nox/logic/graphics/event/SceneNodeEdited.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/physics/Simulation.h>
#include <nox/logic/control/event/Action.h>

#include <noxed/events/MouseAction.h>
#include <noxed/events/TileSetRequest.h>
#include <noxed/events/TileSetResponse.h>
#include <noxed/events/WorldSaveBroadcast.h>
#include <noxed/events/WorldSaveResponse.h>

#include <noxed/tileset/tiletypes/StaticTile.h>
#include <noxed/tileset/tiletypes/ChequeredTile.h>
#include <noxed/tileset/tiletypes/IslandTile.h>
#include <noxed/tileset/tiletypes/IslandTagTile.h>
#include <noxed/tileset/tiletypes/BigTile.h>
#include <noxed/tileset/tiletypes/RandomTile.h>
#include <noxed/tileset/tiletypes/FourBitTile.h>

#include <noxed/brush/BrushPatternManager.h>
#include <noxed/brush/patterns/SquareBrushPattern.h>
#include <noxed/brush/patterns/DiamondBrushPattern.h>

#include <SDL.h>
#include <imgui/imgui.h>

NoxedWindowView::NoxedWindowView(nox::app::IContext* applicationContext, const std::string& windowTitle):
	nox::window::RenderSdlWindowView(applicationContext, windowTitle),
	renderer(nullptr),
	camera(std::make_shared<nox::app::graphics::Camera>(this->getWindowSize())),
	listener("NoxedWindowView"),
	buttonsHeld(6, false),
	lastMousePos(0, 0)
{
	this->log = applicationContext->createLogger();
	this->log.setName("NoxedWindowView");


	this->listener.addEventTypeToListenFor(nox::logic::graphics::SceneNodeEdited::ID);
  this->listener.addEventTypeToListenFor(nox::logic::actor::TransformChange::ID);
	this->listener.addEventTypeToListenFor(noxed::events::TileSetRequest::ID);
	this->listener.addEventTypeToListenFor(noxed::events::WorldSaveBroadcast::ID);

	this->camera->setScale({60.0f, 60.0f});

	this->rootSceneNode = std::make_shared<nox::app::graphics::TransformationNode>();
}

nox::app::graphics::Camera *NoxedWindowView::getActiveCamera() const
{
	return this->camera.get();
}

const noxed::tileset::TileSetManager& NoxedWindowView::getTileSetManager() const
{
	return this->tilesetManager;
}

void NoxedWindowView::broadcastMousePosition(glm::ivec2 screenPos)
{
	glm::ivec2 screenSize = glm::ivec2(this->getWindowSize().x, this->getWindowSize().y);
	glm::vec2 worldPos = this->convertMouseToWorld(screenPos);

	this->lastMousePos = screenPos;

	auto mouseEvent = std::make_shared<noxed::events::MouseAction>(noxed::events::MouseAction::MOVE, this->getControlledActor(), screenPos, screenSize, worldPos, false, -1);

	this->eventBroadcaster->queueEvent(mouseEvent);
}

bool NoxedWindowView::initialize(nox::logic::IContext* context)
{
	if (this->RenderSdlWindowView::initialize(context) == false)
	{
		return false;
	}

	this->listener.setup(this, context->getEventBroadcaster(), nox::logic::event::ListenerManager::StartListening_t());

  this->eventBroadcaster = context->getEventBroadcaster();

	auto brushPatternManager = noxed::brush::BrushPatternManager::getInstance();

  auto resourceAccess = this->getApplicationContext()->getResourceAccess();

  auto controlLayoutResourceDescriptor = nox::app::resource::Descriptor{ "controls.json" };
  auto controlLayoutResource = resourceAccess->getHandle(controlLayoutResourceDescriptor);

  if (controlLayoutResource == nullptr)
  {
    this->log.error().format("Failed loading control layout resource \"%s\".", controlLayoutResourceDescriptor.getPath().c_str());
    return false;
  }
  else
  {
    this->controlMapper.loadKeyboardLayout(controlLayoutResource);
		this->modeChangeMapper = std::make_unique<noxed::handlers::ModeChangeKeyboardMapper>(controlLayoutResource);
	}

	tilesetManager.registerSmartTileType<noxed::tileset::tiletypes::StaticTile>();
	tilesetManager.registerSmartTileType<noxed::tileset::tiletypes::ChequeredTile>();
	tilesetManager.registerSmartTileType<noxed::tileset::tiletypes::IslandTile>();
	tilesetManager.registerSmartTileType<noxed::tileset::tiletypes::IslandTagTile>();
	tilesetManager.registerSmartTileType<noxed::tileset::tiletypes::BigTile>();
	tilesetManager.registerSmartTileType<noxed::tileset::tiletypes::RandomTile>();
	tilesetManager.registerSmartTileType<noxed::tileset::tiletypes::FourBitTile	>();

	tilesetManager.loadAll("tilesets", resourceAccess);

	brushPatternManager->registerPatternType<noxed::brush::patterns::SquareBrushPattern>();
	brushPatternManager->registerPatternType<noxed::brush::patterns::DiamondBrushPattern>();

	this->log.info().format("TileSetManager found and loaded %i TileSet defintions!", tilesetManager.getCount());

	return true;
}


void NoxedWindowView::onRendererCreated(nox::app::graphics::IRenderer* renderer)
{


	assert(renderer != nullptr);

	const auto graphicsResourceDescriptor = nox::app::resource::Descriptor{"graphics/graphics.json"};
	renderer->loadTextureAtlases(graphicsResourceDescriptor, this->getApplicationContext()->getResourceAccess());
  renderer->setWorldTextureAtlas("atlases/testatlas");

	auto background = std::make_unique<nox::app::graphics::BackgroundGradient>();
	background->setBottomColor({0.0f, 0.0f, 0.0f});
	background->setTopColor({1.0f, 1.0f, 1.0f});
	renderer->setBackgroundGradient(std::move(background));

	renderer->setAmbientLightLevel(1.0f);
	renderer->setCamera(this->camera);
	renderer->setRootSceneNode(this->rootSceneNode);
	renderer->organizeRenderSteps();

	this->renderer = renderer;
}

void NoxedWindowView::onControlledActorChanged(nox::logic::actor::Actor* controlledActor)
{
  this->controlMapper.setControlledActor(controlledActor);

	this->modeChangeMapper->setControlledActor(controlledActor);
}

void NoxedWindowView::onWindowSizeChanged(const glm::uvec2& size)
{
	this->RenderSdlWindowView::onWindowSizeChanged(size);

	this->camera->setSize(size);
}

void NoxedWindowView::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
{
	this->RenderSdlWindowView::onEvent(event);

	using SceneNodeEdit = nox::logic::graphics::SceneNodeEdited;
	using TileSetRequest = noxed::events::TileSetRequest;
	using TileSetResponse = noxed::events::TileSetResponse;
	using WorldSaveBroadcast = noxed::events::WorldSaveBroadcast;
	using WorldSaveResponse = noxed::events::WorldSaveResponse;

	if (event->isType(SceneNodeEdit::ID))
	{
		auto nodeEvent = static_cast<SceneNodeEdit*>(event.get());

		if (nodeEvent->getEditAction() == SceneNodeEdit::Action::CREATE)
		{
			this->rootSceneNode->addChild(nodeEvent->getSceneNode());
		}
		else if (nodeEvent->getEditAction() == SceneNodeEdit::Action::REMOVE)
		{
			this->rootSceneNode->removeChild(nodeEvent->getSceneNode());
		}
	}
	else if(event->isType(TileSetRequest::ID))
	{
		auto requestEvent = static_cast<TileSetRequest*>(event.get());

		this->log.info().format("TileSetRequest for \"%s\" received!", requestEvent->getName());

		auto responseEvent = std::make_shared<TileSetResponse>(this->getControlledActor(), requestEvent->getName(), requestEvent->getSender(), this->tilesetManager.find(requestEvent->getName()), &this->tilesetManager);
		this->eventBroadcaster->queueEvent(responseEvent);
	}
	else if(event->isType(WorldSaveBroadcast::ID))
	{
		auto broadcastEvent = static_cast<WorldSaveBroadcast*>(event.get());

		this->log.info().format("WorldSaveBroadcast received!");

		auto responseEvent = std::make_shared<WorldSaveResponse>(broadcastEvent->getActor(), event);
		this->eventBroadcaster->queueEvent(responseEvent);
	}
}

void NoxedWindowView::onKeyPress(const SDL_KeyboardEvent& event)
{
	const bool ctrlDown = (event.keysym.mod & KMOD_CTRL);
	const bool shiftDown = (event.keysym.mod & KMOD_SHIFT);
	const bool altDown = (event.keysym.mod & KMOD_ALT);
	const bool anyDown = (ctrlDown || shiftDown || altDown);

  if (ctrlDown && event.keysym.sym == SDLK_q && this->renderer != nullptr)
  {
    this->renderer->toggleDebugRendering();

    const auto debugRenderEvent = std::make_shared<nox::logic::graphics::DebugRenderingEnabled>(renderer->isDebugRenderingEnabled());
    this->getLogicContext()->getEventBroadcaster()->queueEvent(debugRenderEvent);
  }

  /*
  * If our controlMapper has a controlled Actor (see onControlledActorChanged()), we map the key
  * press to control::Action events. The mapping is based on the JSON that was parsed in initialize().
  * We then broadcast all of these events to the logic so that the Actor can handle the controls.
  *
  * All events with the "move" action name will be handled by the Actor2dDirectionControl.
  * All events with the "rotate" action name will be handled by the Actor2dRotationControl.
  * You can implement more control components by inheriting the control::ActorControl class
  * and handling control events of your desired names.
  */

  if (this->controlMapper.hasControlledActor() == true && !anyDown)
  {
    auto controlEvents = this->controlMapper.mapKeyPress(event.keysym);

    if (this->eventBroadcaster != nullptr)
    {
      for (const auto& event : controlEvents)
      {
        this->eventBroadcaster->queueEvent(event);
      }
    }
  }

	auto modeChangeEvent = this->modeChangeMapper->getEvent(event.keysym.scancode);
	if(modeChangeEvent != nullptr)
	{
		this->eventBroadcaster->queueEvent(modeChangeEvent);
	}
}

void NoxedWindowView::onKeyRelease(const SDL_KeyboardEvent& event)
{
  /*
  * The same as in onKeyPress(), only that we map a key release in stead of a key press.
  */
  if (this->controlMapper.hasControlledActor() == true)
  {
    auto controlEvents = this->controlMapper.mapKeyRelease(event.keysym);

    if (this->eventBroadcaster != nullptr)
    {
      for (const auto& event : controlEvents)
      {
        this->eventBroadcaster->queueEvent(event);
      }
    }
  }

	this->modeChangeMapper->handleRelease(event.keysym.scancode);
}

void NoxedWindowView::onMouseMove(const SDL_MouseMotionEvent &event)
{
	if(!ImGui::IsPosHoveringAnyWindow(ImVec2(event.x, event.y))) {
		this->broadcastMousePosition(glm::ivec2(event.x, event.y));
	}
}

void NoxedWindowView::onMousePress(const SDL_MouseButtonEvent& event)
{
	if(ImGui::IsPosHoveringAnyWindow(ImVec2(event.x, event.y))) {
		return;
	}

	glm::ivec2 screenPos = glm::ivec2(event.x, event.y);
	glm::ivec2 screenSize = glm::ivec2(this->getWindowSize().x, this->getWindowSize().y);
	glm::vec2 worldPos = this->convertMouseToWorld(screenPos);

	if(event.button < this->buttonsHeld.size())
	{
		this->buttonsHeld[event.button] = true;
	}
	this->lastMousePos = screenPos;

	auto mouseEvent = std::make_shared<noxed::events::MouseAction>(noxed::events::MouseAction::BUTTON, this->getControlledActor(), screenPos, screenSize, worldPos, true, event.button);

	this->eventBroadcaster->queueEvent(mouseEvent);
}

void NoxedWindowView::onMouseRelease(const SDL_MouseButtonEvent& event)
{
	glm::ivec2 screenPos = glm::ivec2(event.x, event.y);
	glm::ivec2 screenSize = glm::ivec2(this->getWindowSize().x, this->getWindowSize().y);
	glm::vec2 worldPos = this->convertMouseToWorld(screenPos);

	if(event.button < this->buttonsHeld.size())
	{
		this->buttonsHeld[event.button] = false;
	}
	this->lastMousePos = screenPos;

	auto mouseEvent = std::make_shared<noxed::events::MouseAction>(noxed::events::MouseAction::BUTTON, this->getControlledActor(), screenPos, screenSize, worldPos, false, event.button);

	this->eventBroadcaster->queueEvent(mouseEvent);
}

// Borrowed from 99-game
glm::vec2 NoxedWindowView::convertMouseToWorld(const glm::ivec2& mousePos) const
{
	// Translate to center of screen.
	glm::vec4 convertedCoordinate(
			static_cast<float>(mousePos.x) - static_cast<float>(this->getWindowSize().x) / 2.0f,
			static_cast<float>((mousePos.y - static_cast<int>(this->getWindowSize().y)) * -1) - static_cast<float>(this->getWindowSize().y) / 2.0f,
			1.0f,
			1.0f
	);

	// Scale to world units.
	convertedCoordinate.x /= this->camera->getScale().x;
	convertedCoordinate.y /= this->camera->getScale().y;

	// Rotate coordinates with the inverted camera rotation.
	float currentRotation = this->camera->getRotation();
	glm::mat4 rotationMatrix = glm::rotate(glm::mat4(1.0f), currentRotation, glm::vec3(0.0f, 0.0f, 1.0f));
	convertedCoordinate = rotationMatrix * convertedCoordinate;

	// Translate coordinates to inverse camera position.
	glm::vec2 currentCameraPosition = this->camera->getPosition();
	convertedCoordinate.x += currentCameraPosition.x;
	convertedCoordinate.y += currentCameraPosition.y;

	return glm::vec2(convertedCoordinate);
}

nox::logic::event::IBroadcaster* NoxedWindowView::getEventBroadcaster() const
{
	return this->eventBroadcaster;
}

const bool NoxedWindowView::isMouseHeld(const int& button) const
{
	return this->buttonsHeld[button];
}

const glm::vec2 NoxedWindowView::getMousePosition() const
{
	return this->lastMousePos;
}
