/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Str�mme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#pragma once

#include <nox/app/graphics/2d/Camera.h>
#include <nox/app/graphics/2d/TransformationNode.h>
#include <nox/window/RenderSdlWindowView.h>
#include <nox/window/SdlKeyboardControlMapper.h>
#include <nox/logic/event/IBroadcaster.h>

#include <noxed/handlers/ModeChangeKeyboardMapper.h>
#include <noxed/tileset/TileSetManager.h>


/*
 * This window view inherits from the nox::window::RenderSdlWindowView.
 * The nox::window::RenderSdlWindowView manages a window with SDL2 and runs a nox::app::graphics::OpenGlRenderer
 * to render things to the screen.
 *
 * Based on sample 10-window
 */
class NoxedWindowView : public nox::window::RenderSdlWindowView
{
public:
	/**
	 * Create an example window view.
	 * @param applicationContext The context that it is created in. This is needed to create a logger and pass it down
	 * the inheritance tree.
	 * @param windowTitle The tilte that will appear on top of the window.
	 */
	NoxedWindowView(nox::app::IContext* applicationContext, const std::string& windowTitle);

	static std::string MOUSE_MOTION;
	static std::string MOUSE_LEFT_DOWN;
	static std::string MOUSE_LEFT_UP;
	static std::string MOUSE_RIGHT_DOWN;
	static std::string MOUSE_RIGHT_UP;
	static std::string MOUSE_MIDDLE_DOWN;
	static std::string MOUSE_MIDDLE_UP;

	/**
	 * Gets the active camera.
	 */
	nox::app::graphics::Camera *getActiveCamera() const;

	/**
	 * Gets the tileset manager.
	 */
	const noxed::tileset::TileSetManager& getTileSetManager() const;

	/**
	 * Convert a mouse position to a world position
	 *
	 * @param mousePos Mouse position.
	 */
	glm::vec2 convertMouseToWorld(const glm::ivec2& mousePos) const;

	/**
	 * Check if the mouse button is held down
	 *
	 * @param button Mouse button number from SDL
	 */
	const bool isMouseHeld(const int& button) const;

	/**
	 * Get the event broadcaster
	 */
	nox::logic::event::IBroadcaster* getEventBroadcaster() const;

	/**
	 * Get the mouse position from the last event
	 */
	const glm::vec2 getMousePosition() const;

	/**
	 * Make the window broadcast a mouse movement event for the given position
	 *
	 * @param screenPos Screen position.
	 */
	void broadcastMousePosition(glm::ivec2 screenPos);
  virtual void onRendererCreated(nox::app::graphics::IRenderer* renderer) override;



protected:
  
  
private:
  
  void onControlledActorChanged(nox::logic::actor::Actor* controlledActor) override;
	void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;
  bool initialize(nox::logic::IContext* context) override;
	
	void onWindowSizeChanged(const glm::uvec2& size) override;
	void onKeyPress(const SDL_KeyboardEvent& event) override;
  void onKeyRelease(const SDL_KeyboardEvent& event) override;
	void onMouseMove(const SDL_MouseMotionEvent &event) override;
	void onMousePress(const SDL_MouseButtonEvent& event) override;
	void onMouseRelease(const SDL_MouseButtonEvent& event) override;
	//void onMouseScroll(const SDL_MouseWheelEvent& event) override; TODO When needed

	nox::app::log::Logger log;
	nox::logic::event::ListenerManager listener;


	std::unique_ptr<noxed::handlers::ModeChangeKeyboardMapper> modeChangeMapper;
	noxed::tileset::TileSetManager tilesetManager;

  // This is the object that will map all our key inputs to control::Action events.
  nox::window::SdlKeyboardControlMapper controlMapper;

  // We need to be able to broadcast our control::Action events.
  nox::logic::event::IBroadcaster* eventBroadcaster;

	nox::app::graphics::IRenderer* renderer;

	// The camera decides how to project the content on the screen. This is an orthographic projection.
	std::shared_ptr<nox::app::graphics::Camera> camera;

	// The root scene node is where all other scene nodes are attached for rendering.
	std::shared_ptr<nox::app::graphics::TransformationNode> rootSceneNode;

	std::vector<bool> buttonsHeld;
	glm::ivec2 lastMousePos;
};
