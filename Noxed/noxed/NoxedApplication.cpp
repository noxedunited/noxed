/*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
* Copyright (c) 2016 Gisle Aune (dev@gisle.me
* Copyright (c) 2016 Tor Strømme (tor@stroemme.no)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "NoxedApplication.h"
#include <noxed/NoxedWindowView.h>

#include <nox/app/resource/cache/LruCache.h>
#include <nox/app/resource/provider/BoostFilesystemProvider.h>
#include <nox/app/resource/loader/JsonLoader.h>
#include <nox/app/resource/data/JsonExtraData.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/world/Loader.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/logic/physics/box2d/Box2DSimulation.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/graphics/actor/ActorSprite.h>
#include <nox/logic/graphics/actor/ActorLight.h>
#include <nox/logic/control/actor/Actor2dDirectionControl.h>
#include <nox/logic/control/actor/Actor2dRotationControl.h>

#include <noxed/components/GridCursor.h>
#include <noxed/components/FollowCamera.h>
#include <noxed/components/PushCamera.h>
#include <noxed/components/EditorBlueprint.h>
#include <noxed/components/WorldSaver.h>
#include <noxed/components/EntitySpawner.h>
#include <noxed/components/TileMap.h>
#include <noxed/components/TileSpawner.h>
#include <noxed/components/Brush.h>
#include <noxed/components/TileMapObjectSaver.h>
#include <noxed/components/TileMapManager.h>

#include <noxed/events/MouseAction.h>

#include <json/value.h>
#include <glm/gtx/string_cast.hpp>
#include <cassert>

using noxed::events::MouseAction;

NoxedApplication::NoxedApplication(const char *projectName) :
  SdlApplication(projectName, "Noxed United"),
  initialWorldName("initial.json")
{
}

/*
* EVENTS
*/
bool NoxedApplication::onInit()
{
  this->log = this->createLogger();
  this->log.setName("NoxedApplication");
  this->outputTimer.setTimerLength(std::chrono::milliseconds(500));

  if (SdlApplication::onInit() == false)
  {
    this->log.error().raw("Failed initializing sdl application.");
    return false;
  }

  this->log.info().raw("Initializing resource cache...");
  if (this->initializeResourceCache() == false)
  {
    this->log.error().raw("Failed initializing resource cache.");
    return false;
  }

  auto logic = this->initializeLogic();
	auto eventBroadcaster = logic->getEventBroadcaster();

	this->initializePhysics(logic);
	auto worldManager = this->initializeWorldManager(logic);

	this->initializeWindow(logic);

	if (this->loadWorldFile(logic, worldManager) == false)
	{
    this->log.error().raw("World load failed.");
		return false;
	}

	logic->pause(false);
  this->logicPointer = logic;
  this->log.info().raw("Noxed Online!");
  return true;
}

void NoxedApplication::onUpdate(const nox::Duration& deltaTime)
{
  this->SdlApplication::onUpdate(deltaTime);

  for(int i = 0; i < 6; ++i)
  {
    if(this->window->isMouseHeld(i))
    {
      glm::ivec2 screenPos = this->window->getMousePosition();
      glm::ivec2 screenSize = glm::ivec2(this->window->getWindowSize().x, this->window->getWindowSize().y);
      glm::vec2 worldPos = this->window->convertMouseToWorld(screenPos);

      auto mouseEvent = std::make_shared<noxed::events::MouseAction>(noxed::events::MouseAction::HOLD, this->window->getControlledActor(), screenPos, screenSize, worldPos, true, i);
      this->window->getEventBroadcaster()->queueEvent(mouseEvent);
    }
  }

  const auto camera = this->window->getActiveCamera();
  if(camera->getPosition() != lastCameraPosition)
  {
    this->window->broadcastMousePosition(this->window->getMousePosition());

    lastCameraPosition = camera->getPosition();
  }

  this->outputTimer.spendTime(deltaTime);

  if (this->outputTimer.timerReached() == true)
  {
    if(this->getTps() < 55.0)
    {
      this->log.warning().format("TPS dipped to %.2f", this->getTps());
    }
    this->outputTimer.reset();
  }

  // We need to manually render the window (the Logic doesn't know that it is a window and can render).
	this->window->render();
}

void NoxedApplication::onSdlEvent(const SDL_Event & sdlEvent)
{
  this->SdlApplication::onSdlEvent(sdlEvent);

  assert(this->window != nullptr);
  this->window->onSdlEvent(sdlEvent);
}

/*
* PRIVATE
*/
bool NoxedApplication::initializeResourceCache()
{
	const auto cacheSizeMb = 512u;
	auto resourceCache = std::make_unique<nox::app::resource::LruCache>(cacheSizeMb);

	resourceCache->setLogger(this->createLogger());

  this->log.info().format("Project Name = \"%s\"", this->getName());

	// We need to get resources from the project specific assets.
	const auto projectAssetDirectory = this->getName() + "/assets";
	if (resourceCache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(projectAssetDirectory)) == false)
	{
		this->log.error().format("Could not initialized resource cache to \"%s\".", projectAssetDirectory.c_str());
		return false;
	}

	// We need to get resources from the NOX assets.
	const auto noxAssetsDirectory = std::string{"nox-engine/assets"};
	if (resourceCache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(noxAssetsDirectory)) == false)
	{
		this->log.error().format("Could not initialized resource cache to \"%s\".", noxAssetsDirectory.c_str());
		return false;
	}

	resourceCache->addLoader(std::make_unique<nox::app::resource::JsonLoader>(this->createLogger()));

	this->setResourceCache(std::move(resourceCache));

	return true;
}

nox::logic::Logic* NoxedApplication::initializeLogic()
{
	auto logic = std::make_unique<nox::logic::Logic>();
	auto logicPtr = logic.get();

	this->addProcess(std::move(logic));

	return logicPtr;
}

nox::logic::physics::Simulation* NoxedApplication::initializePhysics(nox::logic::Logic* logic)
{
	auto physics = std::make_unique<nox::logic::physics::Box2DSimulation>(logic);
	physics->setLogger(this->createLogger());

	auto physicsPtr = physics.get();

	logic->setPhysics(std::move(physics));

	return physicsPtr;
}

nox::logic::world::Manager* NoxedApplication::initializeWorldManager(nox::logic::Logic* logic)
{
	auto world = std::make_unique<nox::logic::world::Manager>(logic);

	world->registerActorComponent<nox::logic::actor::Transform>();
	world->registerActorComponent<nox::logic::physics::ActorPhysics>();
  world->registerActorComponent<nox::logic::graphics::ActorSprite>();
  world->registerActorComponent<nox::logic::graphics::ActorLight>();

  world->registerActorComponent<nox::logic::control::Actor2dDirectionControl>();
  world->registerActorComponent<nox::logic::control::Actor2dRotationControl>();

  world->registerActorComponent<noxed::components::GridCursor>();
  world->registerActorComponent<noxed::components::FollowCamera>();
  world->registerActorComponent<noxed::components::PushCamera>();
  world->registerActorComponent<noxed::components::EditorBlueprint>();
  world->registerActorComponent<noxed::components::WorldSaver>();
  world->registerActorComponent<noxed::components::EntitySpawner>();
  world->registerActorComponent<noxed::components::TileMap>();
  world->registerActorComponent<noxed::components::TileSpawner>();
  world->registerActorComponent<noxed::components::Brush>();
  world->registerActorComponent<noxed::components::TileMapObjectSaver>();
  world->registerActorComponent<noxed::components::TileMapManager>();

  for(auto callback : this->worldLoadCallbacks)
  {
    callback(world.get());
  }

	const auto actorDirectory = std::string{"actor"};
	world->loadActorDefinitions(this->getResourceAccess(), actorDirectory);

	auto worldPtr = world.get();

	logic->setWorldManager(std::move(world));

	return worldPtr;
}

bool NoxedApplication::loadWorldFile(nox::logic::IContext* logicContext, nox::logic::world::Manager* worldManager)
{
	const auto worldFileDescriptor = nox::app::resource::Descriptor{"world/" + initialWorldName};
	const auto worldFileHandle = this->getResourceAccess()->getHandle(worldFileDescriptor);

	if (worldFileHandle == nullptr)
	{
		this->log.error().format("Could not load world: %s", worldFileDescriptor.getPath().c_str());
		return false;
	}
	else
	{
		const auto jsonData = worldFileHandle->getExtraData<nox::app::resource::JsonExtraData>();

		if (jsonData == nullptr)
		{
			this->log.error().format("Could not get JSON data for world: %s", worldFileDescriptor.getPath().c_str());
			return false;
		}
		else
		{
			auto loader = nox::logic::world::Loader{logicContext};

      loader.registerControllingView(0, this->window);

			if (loader.loadWorld(jsonData->getRootValue(), worldManager) == false)
			{
				this->log.error().format("Failed loading world \"%s\".", worldFileDescriptor.getPath().c_str());
				return false;
			}
		}
	}

	this->log.verbose().format("Loaded world \"%s\"", worldFileDescriptor.getPath().c_str());

	return true;
}

nox::logic::Logic * NoxedApplication::getLogicPointer()
{
  return this->logicPointer;
}

void NoxedApplication::initializeWindow(nox::logic::Logic* logic)
{
	auto window = std::make_unique<NoxedWindowView>(this, this->getName());

	this->window = window.get();

	logic->addView(std::move(window));
}
